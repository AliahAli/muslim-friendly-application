<?php

namespace App\Models\Jakim;

use Illuminate\Database\Eloquent\Model;

class JakimCompany extends Model
{
    protected $fillable = [
        'num',
        'name',
        'address',
        'brand',
        'expired_date',
        'state',
        'phone_no',
        'fax_no',
        'email',
        'website',
        'reference_no',
        'officer',
        'company_url',
        'product_url',
    ];

    public function products() {
        return $this->hasMany(JakimProduct::class);
    }
}
