<?php

namespace App\Models\Jakim;

use Illuminate\Database\Eloquent\Model;

class JakimProduct extends Model
{
    protected $fillable = [
        'num',
        'name',
        'brand',
        'expired_date',
    ];

    public function company()
    {
        return $this->belongsTo(JakimCompany::class, 'jakim_company_id');
    }
}
