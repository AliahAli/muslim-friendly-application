<?php

namespace App\Http\Controllers;

use App\Models\Jakim\JakimProduct;
use Illuminate\Http\Request;

class IngredientController extends Controller
{
    public function index(Request $request)
    {
        $pageConfigs = ['pageClass' => 'ingredient-index'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"],
            ['link' => "/product/index", 'name' => "Menu"],
            ['link' => "/product/product-list", 'name' => "Menu List"],
            ['link' => "/product/create", 'name' => "Add New Menu"],
            ['name' => "Ingredient List"]
        ];

        $ingredients = JakimProduct::whereHas('company', function ($query) use ($request) {
            return $query->where('name', 'LIKE', '%' . $request->value . '%');
        })
            ->orwhere('name', 'LIKE', '%' . $request->value . '%')
            ->with('company')
            ->paginate(10);

        return view('/ingredient/ingredient-index', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs,
            'ingredients' => $ingredients,
        ]);
    }

    public function create()
    {
        $pageConfigs = ['pageClass' => 'ingredient-create'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"],
            ['link' => "/product/menu-restaurant", 'name' => "Restaurant List"],
            ['link' => "/product/index", 'name' => "Menu"],
            ['link' => "/product/create", 'name' => "Add Menu"],
            ['name' => "Add New Ingredient"]
        ];

        return view('/ingredient/ingredient-create', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs

        ]);
    }

    public function document()
    {
        $pageConfigs = ['pageClass' => 'ingredient-create'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"],
            ['link' => "/product/menu-restaurant", 'name' => "Restaurant List"],
            ['link' => "/product/index", 'name' => "Menu"],
            ['link' => "/product/create", 'name' => "Add Menu"],
            ['name' => "Add New Ingredient"]
        ];

        return view('/ingredient/ingredient-document', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs

        ]);
    }

    public function show()
    {
        $pageConfigs = ['pageClass' => 'ingredient-show'];

        $breadcrumbs = [
            ['link' => "/application/restaurant-list", 'name' => "Home"],
            ['link' => "javascript:void(0)", 'name' => "Ingredient List"],
        ];

        return view('/ingredient/ingredient-show', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function review()
    {
        $pageConfigs = ['pageClass' => 'ingredient-review'];

        $breadcrumbs = [
            ['name' => "Menu"],
            ['link' => "/menu/menu-index", 'name' => "Restaurant List"],
            ['link' => "/menu/menu-list", 'name' => "Menu List"],
            ['link' => "/menu/menu-review", 'name' => "Menu Review"],
            ['name' => "Ingredient Review"]
        ];

        return view('/ingredient/ingredient-review', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }
}
