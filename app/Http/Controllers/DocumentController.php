<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DocumentController extends Controller
{
    public function documentRequirement()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/document/document-requirement', ['pageConfigs' => $pageConfigs]);
    }

    public function documentPayment()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/document/document-payment', ['pageConfigs' => $pageConfigs]);
    }

    public function documentSubmit()
    {
        $pageConfigs = ['pageClass' => 'document-submit'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"], ['name' => "Restaurant List"], ['name' => "Add New Restaurant"]
        ];

        return view('/document/document-submit', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function documentReviewed()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/document/document-reviewed', ['pageConfigs' => $pageConfigs]);
    }
}