<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function index()
    {
        $pageConfigs = [
            'pageClass' => 'menu-index',
        ];

        $breadcrumbs = [
            ['name' => "Menu"],
            ['name' => "Restaurant List"]
        ];

        return view('/menu/menu-index', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function index2()
    {
        $pageConfigs = [
            'pageClass' => 'menu-index2',
        ];

        $breadcrumbs = [
            ['name' => "Menu"],
            ['link' => "/menu/menu-index2", 'name' => "Restaurant List"],
        ];

        return view('/menu/menu-index2', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function list()
    {
        $pageConfigs = [
            'pageClass' => 'menu-list',
        ];

        $breadcrumbs = [
            ['name' => "Menu"],
            ['link' => "/menu/menu-index", 'name' => "Restaurant List"],
            ['name' => "Menu List"]
        ];

        return view('/menu/menu-list', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function list2()
    {
        $pageConfigs = [
            'pageClass' => 'menu-list2',
        ];

        $breadcrumbs = [
            ['name' => "Menu"],
            ['link' => "/menu/menu-index", 'name' => "Restaurant List"],
            ['name' => "Menu List"]
        ];

        return view('/menu/menu-list2', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function review()
    {
        $pageConfigs = [
            'pageClass' => 'menu-review',
        ];

        $breadcrumbs = [
            ['name' => "Menu"],
            ['link' => "/menu/menu-index", 'name' => "Restaurant List"],
            ['link' => "/menu/menu-list", 'name' => "Menu List"],
            ['name' => "Menu Review"]
        ];

        return view('/menu/menu-review', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function review2()
    {
        $pageConfigs = [
            'pageClass' => 'menu-review2',
        ];

        $breadcrumbs = [
            ['name' => "Menu"],
            ['link' => "/menu/menu-index", 'name' => "Restaurant List"],
            ['link' => "/menu/menu-list", 'name' => "Menu List"],
            ['name' => "Menu Review"]
        ];

        return view('/menu/menu-review2', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function approve()
    {
        $pageConfigs = [
            'pageClass' => 'menu-approve',
        ];

        $breadcrumbs = [
            ['name' => "Menu"],
            ['name' => "Restaurant List"]
        ];

        return view('/menu/menu-approve', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function reject()
    {
        $pageConfigs = [
            'pageClass' => 'menu-reject',
        ];

        $breadcrumbs = [
            ['name' => "Menu"],
            ['name' => "Restaurant List"]
        ];

        return view('/menu/menu-reject', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }
}
