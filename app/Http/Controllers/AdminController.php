<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function adminDashboard()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/admin/admin-dashboard', [
            'pageConfigs' => $pageConfigs,
        ]);
    }

    public function adminIndex()
    {
        $pageConfigs = ['pageClass' => 'admin-index'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"], ['link' => "javascript:void(0)", 'name' => "Restaurant"], ['name' => "New Application"]
        ];
        return view('/admin/admin-index', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function adminApproved()
    {
        $pageConfigs = [
            'pageClass' => 'admin-approved',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"], ['link' => "javascript:void(0)", 'name' => "Restaurant"], ['name' => "Approved Application"]
        ];

        return view('/admin/admin-approved', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function adminRejected()
    {
        $pageConfigs = [
            'pageClass' => 'admin-rejected',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"], ['link' => "javascript:void(0)", 'name' => "Restaurant"], ['name' => "Rejected Application"]
        ];

        return view('/admin/admin-rejected', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function adminDocumentReviewed()
    {
        $pageConfigs = ['pageClass' => 'admin-document-reviewed'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"], ['link' => "javascript:void(0)", 'name' => "Restaurant"], ['link' => "/admin/index", 'name' => "New Application"], ['name' => "Review"]
        ];
        return view('/admin/admin-document-reviewed', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function adminReviewed()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/admin/admin-review', ['pageConfigs' => $pageConfigs]);
    }
}