<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContentMalayController extends Controller
{
    public function applicationForm()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content-malay/application-app-form', [
            'pageConfigs' => $pageConfigs,
        ]);
    }

    public function qrcode()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content-malay/qrcode-generate', [
            'pageConfigs' => $pageConfigs,
        ]);
    } 
}