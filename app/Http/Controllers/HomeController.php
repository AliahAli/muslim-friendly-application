<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function authenticationLogin(Request $request)
    {
        $pageConfigs = ['pageHeader'=> false];
        return view('/authentication/authentication-login', ['pageConfigs' => $pageConfigs
        ]);
    }

    public function authenticationLogin2(Request $request)
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/authentication/authentication-login2', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    public function authenticatedRegisteration()
    {
        $pageConfigs = ['pageClass' => 'authentication-registeration'];

        return view('/authentication/authentication-registeration', [
            'pageConfigs' => $pageConfigs,
        ]);
    }

    public function authenticatedTac()
    {
        $pageConfigs = ['pageClass' => 'authentication-tac'];

        return view('/authentication/authentication-tac', [
            'pageConfigs' => $pageConfigs,
        ]);
    }

    public function authenticatedEmail()
    {
        $pageConfigs = ['pageClass' => 'authentication-email'];

        return view('/authentication/authentication-email', [
            'pageConfigs' => $pageConfigs,
        ]);
    } 
    public function authenticationLogin3(Request $request)
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/authentication/authentication-login3', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    public function authenticationLogin4(Request $request)
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/authentication/authentication-login4', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    public function authenticationLogin5(Request $request)
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/authentication/authentication-login5', [
            'pageConfigs' => $pageConfigs
        ]);
    }

}
