<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public function applicationForm()
    {
        $pageConfigs = ['pageClass' => 'application-app-form'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"], ['name' => "Restaurant List"], ['name' => "Add New Restaurant"]
        ];

        return view('/application/application-app-form', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function applicationReview()
    {
        $pageConfigs = ['pageClass' => 'application-review'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"], 
            ['link' => "javascript:void(0)", 'name' => "Application"],
            ['link' => "/application/restaurant-list", 'name' => "Restaurant List"],
            ['name' => "Review"]
        ];

        return view('/application/application-review', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function applicationDashboard()
    {
        $pageConfigs = ['pageClass' => 'application-dashboard'];

        return view('/application/application-dashboard', [
            'pageConfigs' => $pageConfigs,
        ]);
    }

    public function applicationIndex()
    {
        $pageConfigs = ['pageClass' => 'application-index'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"], ['link' => "javascript:void(0)", 'name' => "Application"], ['name' => "New Application"]
        ];

        return view('/application/application-index', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }


    public function applicationIndexData()
    {
        $pageConfigs = ['pageClass' => 'application-index-data'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"], ['link' => "javascript:void(0)", 'name' => "Application"], ['name' => "Restaurant List"]
        ];

        return view('/application/application-index-data', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }
}
