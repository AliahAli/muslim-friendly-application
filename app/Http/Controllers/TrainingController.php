<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TrainingController extends Controller
{
    public function trainingPayment()
    {
        $pageConfigs = ['pageHeader'=> false];
        return view('/training/training-payment', ['pageConfigs' => $pageConfigs
        ]);
    }

    public function trainingVideo()
    {
        $pageConfigs = ['pageHeader'=> false];
        return view('/training/training-video', ['pageConfigs' => $pageConfigs
        ]);
    }

    public function trainingTest1()
    {
        $pageConfigs = ['pageHeader'=> false];
        return view('/training/training-test1', ['pageConfigs' => $pageConfigs
        ]);
    }
    public function trainingTest2()
    {
        $pageConfigs = ['pageHeader'=> false];
        return view('/training/training-test2', ['pageConfigs' => $pageConfigs
        ]);
    }

    public function trainingRestaurant()
    {
        $pageConfigs = ['pageHeader'=> false];
        return view('/training/training-restaurant', ['pageConfigs' => $pageConfigs
        ]);
    }

    public function trainingResult()
    {
        $pageConfigs = ['pageHeader'=> false];
        return view('/training/training-result', ['pageConfigs' => $pageConfigs
        ]);
    }
   
}
