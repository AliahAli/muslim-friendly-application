<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
  // Dashboard - Analytics
  public function dashboardAnalytics()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/dashboard/dashboard-analytics', ['pageConfigs' => $pageConfigs]);
  }

  // Dashboard - Ecommerce
  public function dashboardEcommerce()
  {
    $pageConfigs = ['pageHeader' => false];

    return view('/content/dashboard/dashboard-ecommerce', ['pageConfigs' => $pageConfigs]);
  }
 // Dashboard - Admin1
  public function dashboardAdmin1()
  {
    $pageConfigs = [
      'pageClass' => 'dashboard-admin1',
    ];

    $breadcrumbs = [
      ['link' => "/", 'name' => "Dashboard"],
    ];

    return view('/dashboard/dashboard-admin1', [
      'pageConfigs' => $pageConfigs,
      'breadcrumbs' => $breadcrumbs
    ]);
  }

  // Dashboard - Admin2
  public function dashboardAdmin2()
  {
    $pageConfigs = [
      'pageClass' => 'dashboard-admin2',
    ];

    $breadcrumbs = [
      ['link' => "/", 'name' => "Dashboard"],
    ];

    return view('/dashboard/dashboard-admin2', [
      'pageConfigs' => $pageConfigs,
      'breadcrumbs' => $breadcrumbs
    ]);
  }

   // Dashboard - User1
   public function dashboardUser1()
   {
     $pageConfigs = [
       'pageClass' => 'dashboard-user1',
     ];
 
     $breadcrumbs = [
       ['link' => "/", 'name' => "Dashboard"],
     ];
 
     return view('/dashboard/dashboard-user1', [
       'pageConfigs' => $pageConfigs,
       'breadcrumbs' => $breadcrumbs
     ]);
   }

  // Dashboard - User2
  public function dashboardUser2()
  {
    $pageConfigs = [
      'pageClass' => 'dashboard-user2',
    ];

    $breadcrumbs = [
      ['link' => "/", 'name' => "Dashboard"],
    ];

    return view('/dashboard/dashboard-user2', [
      'pageConfigs' => $pageConfigs,
      'breadcrumbs' => $breadcrumbs
    ]);
  }

  // Dashboard - User3
  public function dashboardUser3()
  {
    $pageConfigs = [
      'pageClass' => 'dashboard-user3',
    ];

    $breadcrumbs = [
      ['link' => "/", 'name' => "Dashboard"],
    ];

    return view('/dashboard/dashboard-user3', [
      'pageConfigs' => $pageConfigs,
      'breadcrumbs' => $breadcrumbs
    ]);
  }
}
