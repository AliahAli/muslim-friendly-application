<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;  
use PDF;  

class QRCodeController extends Controller
{
    public function index()
    {
        $pageConfigs = [
            'pageClass' => 'qrcode-index',
        ];

        $breadcrumbs = [
            ['link' => "/dashboard/user3", 'name' => "Dashboard"],
            ['name' => "QR Code"],
            ['name' => "Restaurant List"]
        ];

        return view('/qr-code/qrcode-index', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function payment()
    {
        $pageConfigs = [
            'pageClass' => 'qrcode-payment',
        ];

        $breadcrumbs = [
            ['link' => "/dashboard/user3", 'name' => "Dashboard"],
            ['name' => "QR Code"],
            ['link' => "/qr-code/qrcode-index", 'name' => "Restaurant List"],
            ['name' => "Payment"]
        ];

        return view('/qr-code/qrcode-payment', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function generate(Request $request)
    {
        $pageConfigs = [
            'pageClass' => 'qrcode-generate',
        ];

        $breadcrumbs = [
            ['link' => "/dashboard/user3", 'name' => "Dashboard"],
            ['name' => "QR Code"],
            ['link' => "/qr-code/qrcode-index", 'name' => "Restaurant List"],
            ['link' => "/qr-code/qrcode-payment", 'name' => "Payment"],
            ['name' => "Generate QR Code"]
        ];

        return view('/qr-code/qrcode-generate', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function menu()
    {
        $pageConfigs = [
            'pageClass' => 'qrcode-menu',
        ];

        return view('/qr-code/qrcode-menu', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    public function show(Request $request)
    {
        $pageConfigs = [
            'pageClass' => 'qrcode-show',
        ];


        return view('/qr-code/qrcode-show', [
            'pageConfigs' => $pageConfigs
        ]);

    }
    
}