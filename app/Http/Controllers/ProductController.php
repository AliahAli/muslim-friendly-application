<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index()
    {
        $pageConfigs = [
            'pageClass' => 'product-index',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"],
            ['link' => "/product/menu-restaurant", 'name' => "Restaurant List"], 
            ['name' => "Menu"]
        ];

        return view('/product/product-index', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function indexData()
    {
        $pageConfigs = ['pageClass' => 'product-indexData'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"],
            ['link' => "/product/menu-restaurant", 'name' => "Restaurant List"],  
            ['link' => "/product/index", 'name' => "Menu"], 
            ['name' => "Menu List"]
        ];

        return view('/product/product-indexData', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function create()
    {
        $pageConfigs = ['pageClass' => 'product-create'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"],
            ['link' => "/product/menu-restaurant", 'name' => "Restaurant List"],  
            ['link' => "/product/index", 'name' => "Menu"], 
            ['name' => "Add New Menu"]
        ];

        return view('/product/product-create', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function createData()
    {
        $pageConfigs = ['pageClass' => 'product-createData'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"],
            ['link' => "/product/menu-restaurant", 'name' => "Restaurant List"],
            ['link' => "/product/index", 'name' => "Menu"],
            ['name' => "Add New Menu"]
        ];

        return view('/product/product-createData', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function show()
    {
        $pageConfigs = ['pageClass' => 'product-show'];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"],
            ['link' => "/product/menu-restaurant", 'name' => "Restaurant List"],
            ['link' => "/product/index", 'name' => "Menu"],
            ['name' => "Review Menu"]
        ];

        return view('/product/product-show', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function restaurant()
    {
        $pageConfigs = [
            'pageClass' => 'product-restaurant',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"],['name' => "Restaurant List"]
        ];

        return view('/product/product-restaurant', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function payment()
    {
        $pageConfigs = [
            'pageClass' => 'product-payment',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Dashboard"],
            ['link' => "/product/menu-restaurant", 'name' => "Restaurant List"],
            ['name' => "Payment"]
        ];

        return view('/product/product-payment', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }
}
