<?php

namespace App\Console\Commands;

use App\Models\Jakim\JakimCompany;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\DomCrawler\Crawler;

class ScrapJakimDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jakim:scrap {start?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to scrap jakim data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('allow_url_fopen', 1);
        $start = $this->argument('start');
        $this->processAllListingPage($start ?? 1);
//        $this->processListingPage(1);
//        $this->processDetailPage('directory/slm_viewdetail.php?comp_code=COMP-20180319-141105&type=C&ty=CO');
        return Command::SUCCESS;
    }

    protected function processAllListingPage($startFrom = 1) {
        $page = $startFrom;
        do {
            $this->info('Page: ' . $page);
            $last = $this->processListingPage($page);
        } while($page++ < $last);
    }

    protected function processListingPage($page) {
        $companyUrl = 'index.php?' .
            'data=ZGlyZWN0b3J5L2luZGV4X2RpcmVjdG9yeTs7Ozs=' .
            '&negeri=01&category=&cari=&page=' . $page;
        $html = $this->getCacheContent($companyUrl);

        $bot = new Crawler(str_replace('&nbsp;', ' ', $html));
        $companies = [];
        $bot->filter('.search-result-data')->each(function(Crawler $element, $i) use (&$companies, $companyUrl) {
            $tds = $element->filter('div');
            if($tds->count() < 4) return;
            $url = $this->convertDetailUrl($tds->eq(3)->filter('img')->first()->attr('onclick'));
            $companyData = $this->extractCompany($tds->eq(1));
            $company = [
                'num' => $this->html2Text($tds->eq(0)),
                'name' => $companyData['name'],
                'address' => $companyData['address'],
                'company_url' => $companyUrl,
                'product_url' => $url,
            ];
            $companyModel = JakimCompany::updateOrCreate(
                collect($company)->only('num')->toArray(),
                $company
            );
            $companies[] = $company;
            $this->processDetailPage($url, $companyModel);
        });
        $total = explode(' ', $bot->filter('.corporatedesc b')->last()->text());
        $last = (int) $total[count($total) - 1];
//        dd(compact('companies'));
        return $last;
    }

    protected function convertDate($dates) {
        if($dates == '') return [];
        $dates = explode("\n", $dates);
        $newDates = [];
        foreach($dates as $date) {
            $newDates[] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
        }
        return $newDates;
    }

    protected function convertDetailUrl($url) {
        preg_match('/^([^\']+\')([^\']*)\'.*$/', $url, $matches);

        return $matches[2];
    }

    protected function extractCompany($node) {
        $details = $node->filter('span');
        return [
            'name' => $this->html2Text($details->eq(0)),
            'address' => $this->html2Text($details->eq(1)),
            'brand' => $this->html2Text($details->eq(2)),
        ];
    }

    protected function processDetailPage($url, $companyModel) {
        $html = $this->getCacheContent($url);

        $bot = new Crawler(str_replace('&nbsp;', ' ', $html));
        $data = [];
        $products = [];
        $bot->filter('body > table > td > table > tr')->each(function(Crawler $element, $i) use (&$data, &$products) {
            if($i == 0) return;
            $children = $element->filter('td');

            $table = $element->filter('table');
            if($table->count()) {
                $products = array_merge($products, $this->extractProduct($table));
                return;
            }
            $data[$this->convertKey($children->first())] = $this->html2Text($children->last());
        });

        if(!is_null($companyModel)) {
            $companyModel->update($data);
            foreach($products as $product) {
                $companyModel->products()
                    ->updateOrCreate(
                        collect($product)->only('num')->toArray(),
                        $product
                    );
            }
        }
    }

    protected function convertKey($node) {
        $key = $node->text();
        $key = trim(strtolower($key), ' :.');
        $key = str_replace([' ', '-'], ['_', ''], $key);
        return $key;
    }

    protected function extractProduct($node) {
        $products = [];
        $node->each(function(Crawler $element) use (&$products) {
            $element->filter('tr')->each(function(Crawler $element, $i) use (&$products) {
                if($i == 0) return;
                $tds = $element->filter('td');
                if($tds->count() < 4) return;
                $products[] = [
                    'num' => $this->html2Text($tds->eq(0)),
                    'name' => $this->html2Text($tds->eq(1)),
                    'brand' => $this->html2Text($tds->eq(2)),
                    'expired_date' => implode(' ', $this->convertDate($this->html2Text($tds->eq(3)))),
                ];
            });
        });

        return $products;
    }

    protected function html2Text($node) {
        $html = $node->html();
        $html = preg_replace('/<br[ \/]*>/', "\n", $html);
        $html = str_replace('&nbsp;', ' ', $html);
        $html = preg_replace('/\r\n/', "\n", $html);
        $html = preg_replace('/[ \t]+/', ' ', $html);
        $html = preg_replace('/[ ]*\n[ ]*/', "\n", $html);
        $html = trim($html, " \n.");
        return html_entity_decode(strip_tags($html));
    }

    protected function getUrl($url) {
        return 'https://www.halal.gov.my/v4/' . $url;
    }

    protected function getCacheContent($url, $post = false) {
        return Cache::rememberForever('curl-url-' . $url, function() use ($url, $post) {
            return $this->getCurlContent($url, $post);
        });
    }

    protected function getCurlContent($url,  $post = false) {
        $url = $this->getUrl($url);
        $userAgent = $_ENV['USER_AGENT'] ?? @$_SERVER['HTTP_USER_AGENT'];
        $ssl = $_ENV['APP_SSL'] ?? true;
        $arr_url = parse_url( $url );
        $origin = $arr_url['scheme'].'://'.$arr_url['host'];
        // Initialize a CURL session.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, !!$post);
        if(!!$post) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
        $headers   = [
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/;q=0.8',
            'Accept-Language: en-US,en;q=0.5',
            'Connection: keep-alive',
            'Upgrade-Insecure-Requests: 1',
            'Pragma: no-cache',
            'Cache-Control: no-cache',
            "Origin: {$origin}",
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // Return Page contents.
        $html = curl_exec($ch);
        $info = curl_getinfo($ch);
        $retry = 0;
        while(!$html && $retry < 3){
            $html = curl_exec($ch);
            $info = curl_getinfo($ch);
            $retry++;
        }
        curl_close($ch);
        return $html;
    }
}
