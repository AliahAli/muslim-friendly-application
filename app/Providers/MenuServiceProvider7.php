<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MenuServiceProvider7 extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // get all data from menu.json file
        $verticalMenuJson = file_get_contents(base_path('resources/data/menu-data/verticalMenuMalay.json'));
        $verticalMenuData2 = json_decode($verticalMenuJson);
        $horizontalMenuJson = file_get_contents(base_path('resources/data/menu-data/horizontalMenu.json'));
        $horizontalMenuData = json_decode($horizontalMenuJson);

         // Share all menuData to all the views
        \View::share('menuData7',[$verticalMenuData2, $horizontalMenuData]);
    }
}
