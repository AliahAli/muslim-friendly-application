// Payment before take test
function confirmFirstPayment() {
  Swal.fire({
    title: 'Payment Successful',
    text: "Your payment is successful. You are being able to take the test now",
    icon: 'success',
    showCancelButton: false,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'OK'
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "/training/video";
    }
  })
}

function confirmPaymentMenu() {
  Swal.fire({
    title: 'Payment Successful',
    text: "Your payment is successful. You are being able to register the menu now",
    icon: 'success',
    showCancelButton: false,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'OK'
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "index";
    }
  })
}

function confirmPayment() {
  Swal.fire({
    title: 'Payment Successful',
    text: "You have made 100% of payment required, the QR Code will be generate.",
    icon: 'success',
    showCancelButton: false,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'OK'
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "generate";
    }
  })
}

function confirmApplication() {
  Swal.fire({
    title: 'Are you sure to submit application?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "/application/index-data";
    }
  })
}

function verifiedApp() {
  Swal.fire({
    title: 'Are you sure to verify this application?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "/admin/approved";
    }
  })
}

function rejectApp() {
  Swal.fire({
    title: 'Are you sure want to reject this application?',
    text: "Please insert the reason",
    input: 'text',
    icon: 'warning',
    inputValue: "Incomplete document",
    inputAttributes: {
      autocapitalize: 'off'
    },
    showCancelButton: true,
    confirmButtonText: 'Submit',
    showLoaderOnConfirm: true,
    allowOutsideClick: () => !Swal.isLoading()
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "/admin/rejected";
    }
  })
}

function submitMenu() {
  Swal.fire({
    title: 'Are you sure to submit Menu and Ingredients?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "/authentication/login4";
    }
  })
}

function verifiedIngredient() {
  Swal.fire({
    title: 'Are you sure to approve this ingredient?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "/menu/review2";
    }
  })
}

function rejectIngredient() {
  Swal.fire({
    title: 'Are you sure want to reject?',
    text: "Please insert the reason",
    input: 'text',
    icon: 'warning',
    inputValue: "Expired halal certificate",
    inputAttributes: {
      autocapitalize: 'off'
    },
    showCancelButton: true,
    confirmButtonText: 'Submit',
    showLoaderOnConfirm: true,
    allowOutsideClick: () => !Swal.isLoading()
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "/menu/review2";
    }
  })
}

function verifiedMenu() {
  Swal.fire({
    title: 'Verified',
    text: "Menu has been verified!",
    icon: 'success',
    showCancelButton: false,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'OK'
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "/menu/list2";
    }
  })
}

function rejectMenu() {
  Swal.fire({
    title: 'Are you sure want to reject?',
    text: "Please insert the reason",
    input: 'text',
    icon: 'warning',
    inputValue: "Few of ingredient halal certificate is expired",
    inputAttributes: {
      autocapitalize: 'off'
    },
    showCancelButton: true,
    confirmButtonText: 'Submit',
    showLoaderOnConfirm: true,
    allowOutsideClick: () => !Swal.isLoading()
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "/menu/list2";
    }
  })
}

function verifyMenuRestaurant() {
  Swal.fire({
    title: 'Are you sure to submit the verification of menu?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "/menu/index2";
    }
  })
}

function confirmTest() {
  Swal.fire({
    title: 'Are you sure you want to start the test ?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "/training/test1";
    }
  })
}

function submitTest() {
  Swal.fire({
    title: 'Are you sure to submit the test?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "/training/result";
    }
  })
}