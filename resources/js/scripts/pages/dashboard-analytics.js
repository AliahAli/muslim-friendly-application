/*=========================================================================================
    File Name: dashboard-analytics.js
    Description: dashboard analytics page content with Apexchart Examples
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(window).on('load', function () {
  'use strict';

  var $avgSessionStrokeColor2 = '#ebf0f7';
  var $textHeadingColor = '#5e5873';
  var $white = '#fff';
  var $strokeColor = '#ebe9f1';

  var $gainedChart = document.querySelector('#gained-chart');
  var $restaurantChart = document.querySelector('#restaurant-chart');
  var $menuChart = document.querySelector('#menu-chart');
  var $ingredientChart = document.querySelector('#ingredient-chart');
  var $avgSessionsChart = document.querySelector('#avg-sessions-chart');
  var $restaurantTrackerChart = document.querySelector('#restaurant-trackers-chart');
  var $menuTrackerChart = document.querySelector('#menu-trackers-chart');
  var $restaurant2TrackerChart = document.querySelector('#restaurant2-trackers-chart');
  var $menu2TrackerChart = document.querySelector('#menu2-trackers-chart');
  var $salesVisitChart = document.querySelector('#sales-visit-chart');
  var $restaurantUserChart = document.querySelector('#restaurantUser-chart');
  var $menuUserChart = document.querySelector('#menuUser-chart');
  var $ingredientUserChart = document.querySelector('#ingredientUser-chart');
  var $restaurantUser2Chart = document.querySelector('#restaurantUser2-chart');
  var $menuUser2Chart = document.querySelector('#menuUser2-chart');
  var $ingredientUser2Chart = document.querySelector('#ingredientUser2-chart');

  var gainedChartOptions;
  var restaurantChartOptions;
  var menuChartOptions;
  var ingredientChartOptions;
  var avgSessionsChartOptions;
  var restaurantTrackerChartOptions;
  var menuTrackerChartOptions;
  var restaurant2TrackerChartOptions;
  var menu2TrackerChartOptions;
  var salesVisitChartOptions;
  var restaurantUserChartOptions;
  var menuUserChartOptions;
  var ingredientUserChartOptions;
  var restaurantUser2ChartOptions;
  var menuUser2ChartOptions;
  var ingredientUser2ChartOptions;

  var gainedChart;
  var restaurantChart;
  var menuChart;
  var ingredientChart;
  var avgSessionsChart;
  var restaurantTrackerChart;
  var menuTrackerChart;
  var restaurant2TrackerChart;
  var menu2TrackerChart;
  var salesVisitChart;
  var restaurantUserChart;
  var menuUserChart;
  var ingredientUserChart;
  var restaurantUser2Chart;
  var menuUser2Chart;
  var ingredientUser2Chart;
  var isRtl = $('html').attr('data-textdirection') === 'rtl';

  // Subscribed Gained Chart
  // ----------------------------------

  gainedChartOptions = {
    chart: {
      height: 100,
      type: 'area',
      toolbar: {
        show: false
      },
      sparkline: {
        enabled: true
      },
      grid: {
        show: false,
        padding: {
          left: 0,
          right: 0
        }
      }
    },
    colors: [window.colors.solid.primary],
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'smooth',
      width: 2.5
    },
    fill: {
      type: 'gradient',
      gradient: {
        shadeIntensity: 0.9,
        opacityFrom: 0.7,
        opacityTo: 0.5,
        stops: [0, 80, 100]
      }
    },
    series: [
      {
        name: 'Subscribers',
        data: [28, 40, 36, 52, 38, 60, 55]
      }
    ],
    xaxis: {
      labels: {
        show: false
      },
      axisBmenu: {
        show: false
      }
    },
    yaxis: [
      {
        y: 0,
        offsetX: 0,
        offsetY: 0,
        padding: { left: 0, right: 0 }
      }
    ],
    tooltip: {
      x: { show: false }
    }
  };
  gainedChart = new ApexCharts($gainedChart, gainedChartOptions);
  gainedChart.render();

  // Restaurant Registered Chart
  // ----------------------------------

  restaurantChartOptions = {
    chart: {
      height: 100,
      type: 'area',
      toolbar: {
        show: false
      },
      sparkline: {
        enabled: true
      },
      grid: {
        show: false,
        padding: {
          left: 0,
          right: 0
        }
      }
    },
    colors: [window.colors.solid.success],
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'smooth',
      width: 2.5
    },
    fill: {
      type: 'gradient',
      gradient: {
        shadeIntensity: 0.9,
        opacityFrom: 0.7,
        opacityTo: 0.5,
        stops: [0, 80, 100]
      }
    },
    series: [
      {
        name: 'Subscribers',
        data: [28, 40, 36, 52, 38, 60, 55]
      }
    ],
    xaxis: {
      labels: {
        show: false
      },
      axisBmenu: {
        show: false
      }
    },
    yaxis: [
      {
        y: 0,
        offsetX: 0,
        offsetY: 0,
        padding: { left: 0, right: 0 }
      }
    ],
    tooltip: {
      x: { show: false }
    }
  };
  restaurantChart = new ApexCharts($restaurantChart, restaurantChartOptions);
  restaurantChart.render();


  // Menu Received Chart
  // ----------------------------------

  menuChartOptions = {
    chart: {
      height: 100,
      type: 'area',
      toolbar: {
        show: false
      },
      sparkline: {
        enabled: true
      },
      grid: {
        show: false,
        padding: {
          left: 0,
          right: 0
        }
      }
    },
    colors: [window.colors.solid.info],
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'smooth',
      width: 2.5
    },
    fill: {
      type: 'gradient',
      gradient: {
        shadeIntensity: 0.9,
        opacityFrom: 0.7,
        opacityTo: 0.5,
        stops: [0, 80, 100]
      }
    },
    series: [
      {
        name: 'Orders',
        data: [10, 15, 8, 15, 7, 12, 8]
      }
    ],
    xaxis: {
      labels: {
        show: false
      },
      axisBmenu: {
        show: false
      }
    },
    yaxis: [
      {
        y: 0,
        offsetX: 0,
        offsetY: 0,
        padding: { left: 0, right: 0 }
      }
    ],
    tooltip: {
      x: { show: false }
    }
  };
  menuChart = new ApexCharts($menuChart, menuChartOptions);
  menuChart.render();

  // Ingredient Gained Chart
  // ----------------------------------

  ingredientChartOptions = {
    chart: {
      height: 100,
      type: 'area',
      toolbar: {
        show: false
      },
      sparkline: {
        enabled: true
      },
      grid: {
        show: false,
        padding: {
          left: 0,
          right: 0
        }
      }
    },
    colors: [window.colors.solid.warning],
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'smooth',
      width: 2.5
    },
    fill: {
      type: 'gradient',
      gradient: {
        shadeIntensity: 0.9,
        opacityFrom: 0.7,
        opacityTo: 0.5,
        stops: [0, 80, 100]
      }
    },
    series: [
      {
        name: 'Subscribers',
        data: [10, 20, 60, 70, 38, 80, 55]
      }
    ],
    xaxis: {
      labels: {
        show: false
      },
      axisBmenu: {
        show: false
      }
    },
    yaxis: [
      {
        y: 0,
        offsetX: 0,
        offsetY: 0,
        padding: { left: 0, right: 0 }
      }
    ],
    tooltip: {
      x: { show: false }
    }
  };
  ingredientChart = new ApexCharts($ingredientChart, ingredientChartOptions);
  ingredientChart.render();

  // Average Session Chart
  // ----------------------------------
  avgSessionsChartOptions = {
    chart: {
      type: 'bar',
      height: 200,
      sparkline: { enabled: true },
      toolbar: { show: false }
    },
    states: {
      hover: {
        filter: 'none'
      }
    },
    colors: [
      $avgSessionStrokeColor2,
      $avgSessionStrokeColor2,
      window.colors.solid.primary,
      $avgSessionStrokeColor2,
      $avgSessionStrokeColor2,
      $avgSessionStrokeColor2
    ],
    series: [
      {
        name: 'Sessions',
        data: [75, 125, 225, 175, 125, 75, 25]
      }
    ],
    grid: {
      show: false,
      padding: {
        left: 0,
        right: 0
      }
    },
    plotOptions: {
      bar: {
        columnWidth: '45%',
        distributed: true,
        endingShape: 'rounded'
      }
    },
    tooltip: {
      x: { show: false }
    },
    xaxis: {
      type: 'numeric'
    }
  };
  avgSessionsChart = new ApexCharts($avgSessionsChart, avgSessionsChartOptions);
  avgSessionsChart.render();

  // restaurant Tracker Chart
  // -----------------------------
  restaurantTrackerChartOptions = {
    chart: {
      height: 270,
      type: 'radialBar'
    },
    plotOptions: {
      radialBar: {
        size: 150,
        offsetY: 20,
        startAngle: -150,
        endAngle: 150,
        hollow: {
          size: '65%'
        },
        track: {
          background: $white,
          strokeWidth: '100%'
        },
        dataLabels: {
          name: {
            offsetY: -5,
            color: $textHeadingColor,
            fontSize: '1rem'
          },
          value: {
            offsetY: 15,
            color: $textHeadingColor,
            fontSize: '1.714rem'
          }
        }
      }
    },
    colors: [window.colors.solid.danger],
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        type: 'horizontal',
        shadeIntensity: 0.5,
        gradientToColors: [window.colors.solid.primary],
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100]
      }
    },
    stroke: {
      dashArray: 8
    },
    series: [40],
    labels: ['Completed Review']
  };
  restaurantTrackerChart = new ApexCharts($restaurantTrackerChart, restaurantTrackerChartOptions);
  restaurantTrackerChart.render();

  // Menu Tracker Chart
  // -----------------------------
  menuTrackerChartOptions = {
    chart: {
      height: 270,
      type: 'radialBar'
    },
    plotOptions: {
      radialBar: {
        size: 150,
        offsetY: 20,
        startAngle: -150,
        endAngle: 150,
        hollow: {
          size: '65%'
        },
        track: {
          background: $white,
          strokeWidth: '100%'
        },
        dataLabels: {
          name: {
            offsetY: -5,
            color: $textHeadingColor,
            fontSize: '1rem'
          },
          value: {
            offsetY: 15,
            color: $textHeadingColor,
            fontSize: '1.714rem'
          }
        }
      }
    },
    colors: [window.colors.solid.danger],
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        type: 'horizontal',
        shadeIntensity: 0.5,
        gradientToColors: [window.colors.solid.primary],
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100]
      }
    },
    stroke: {
      dashArray: 8
    },
    series: [50],
    labels: ['Completed Review']
  };
  menuTrackerChart = new ApexCharts($menuTrackerChart, menuTrackerChartOptions);
  menuTrackerChart.render();

  // restaurant-2 Tracker Chart
  // -----------------------------
  restaurant2TrackerChartOptions = {
    chart: {
      height: 270,
      type: 'radialBar'
    },
    plotOptions: {
      radialBar: {
        size: 150,
        offsetY: 20,
        startAngle: -150,
        endAngle: 150,
        hollow: {
          size: '65%'
        },
        track: {
          background: $white,
          strokeWidth: '100%'
        },
        dataLabels: {
          name: {
            offsetY: -5,
            color: $textHeadingColor,
            fontSize: '1rem'
          },
          value: {
            offsetY: 15,
            color: $textHeadingColor,
            fontSize: '1.714rem'
          }
        }
      }
    },
    colors: [window.colors.solid.danger],
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        type: 'horizontal',
        shadeIntensity: 0.5,
        gradientToColors: [window.colors.solid.primary],
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100]
      }
    },
    stroke: {
      dashArray: 8
    },
    series: [84],
    labels: ['Completed Review']
  };
  restaurant2TrackerChart = new ApexCharts($restaurant2TrackerChart, restaurant2TrackerChartOptions);
  restaurant2TrackerChart.render();

  // Menu2 Tracker Chart
  // -----------------------------
  menu2TrackerChartOptions = {
    chart: {
      height: 270,
      type: 'radialBar'
    },
    plotOptions: {
      radialBar: {
        size: 150,
        offsetY: 20,
        startAngle: -150,
        endAngle: 150,
        hollow: {
          size: '65%'
        },
        track: {
          background: $white,
          strokeWidth: '100%'
        },
        dataLabels: {
          name: {
            offsetY: -5,
            color: $textHeadingColor,
            fontSize: '1rem'
          },
          value: {
            offsetY: 15,
            color: $textHeadingColor,
            fontSize: '1.714rem'
          }
        }
      }
    },
    colors: [window.colors.solid.danger],
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        type: 'horizontal',
        shadeIntensity: 0.5,
        gradientToColors: [window.colors.solid.primary],
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100]
      }
    },
    stroke: {
      dashArray: 8
    },
    series: [70],
    labels: ['Completed Review']
  };
  menu2TrackerChart = new ApexCharts($menu2TrackerChart, menu2TrackerChartOptions);
  menu2TrackerChart.render();

  // Sales Chart
  // -----------------------------
  salesVisitChartOptions = {
    chart: {
      height: 300,
      type: 'radar',
      dropShadow: {
        enabled: true,
        blur: 8,
        left: 1,
        top: 1,
        opacity: 0.2
      },
      toolbar: {
        show: false
      },
      offsetY: 5
    },
    series: [
      {
        name: 'Sales',
        data: [90, 50, 86, 40, 100, 20]
      },
      {
        name: 'Visit',
        data: [70, 75, 70, 76, 20, 85]
      }
    ],
    stroke: {
      width: 0
    },
    colors: [window.colors.solid.primary, window.colors.solid.info],
    plotOptions: {
      radar: {
        polygons: {
          strokeColors: [$strokeColor, 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
          connectorColors: 'transparent'
        }
      }
    },
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        gradientToColors: [window.colors.solid.primary, window.colors.solid.info],
        shadeIntensity: 1,
        type: 'horizontal',
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100, 100, 100]
      }
    },
    markers: {
      size: 0
    },
    legend: {
      show: false
    },
    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
    dataLabels: {
      background: {
        foreColor: [$strokeColor, $strokeColor, $strokeColor, $strokeColor, $strokeColor, $strokeColor]
      }
    },
    yaxis: {
      show: false
    },
    grid: {
      show: false,
      padding: {
        bottom: -27
      }
    }
  };
  salesVisitChart = new ApexCharts($salesVisitChart, salesVisitChartOptions);
  salesVisitChart.render();

  // Chart Dashboard User 1 Start

  // Restaurant Tracker Chart
  // -----------------------------
  restaurantUserChartOptions = {
    chart: {
      height: 270,
      type: 'radialBar'
    },
    plotOptions: {
      radialBar: {
        size: 150,
        offsetY: 20,
        startAngle: -150,
        endAngle: 150,
        hollow: {
          size: '65%'
        },
        track: {
          background: $white,
          strokeWidth: '100%'
        },
        dataLabels: {
          name: {
            offsetY: -5,
            color: $textHeadingColor,
            fontSize: '1rem'
          },
          value: {
            offsetY: 15,
            color: $textHeadingColor,
            fontSize: '1.714rem'
          }
        }
      }
    },
    colors: [window.colors.solid.danger],
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        type: 'horizontal',
        shadeIntensity: 0.5,
        gradientToColors: [window.colors.solid.primary],
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100]
      }
    },
    stroke: {
      dashArray: 8
    },
    series: [0],
    labels: ['Completed Review']
  };
  restaurantUserChart = new ApexCharts($restaurantUserChart, restaurantUserChartOptions);
  restaurantUserChart.render();

  // Menu Tracker Chart
  // -----------------------------
  menuUserChartOptions = {
    chart: {
      height: 270,
      type: 'radialBar'
    },
    plotOptions: {
      radialBar: {
        size: 150,
        offsetY: 20,
        startAngle: -150,
        endAngle: 150,
        hollow: {
          size: '65%'
        },
        track: {
          background: $white,
          strokeWidth: '100%'
        },
        dataLabels: {
          name: {
            offsetY: -5,
            color: $textHeadingColor,
            fontSize: '1rem'
          },
          value: {
            offsetY: 15,
            color: $textHeadingColor,
            fontSize: '1.714rem'
          }
        }
      }
    },
    colors: [window.colors.solid.danger],
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        type: 'horizontal',
        shadeIntensity: 0.5,
        gradientToColors: [window.colors.solid.primary],
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100]
      }
    },
    stroke: {
      dashArray: 8
    },
    series: [0],
    labels: ['Completed Review']
  };
  menuUserChart = new ApexCharts($menuUserChart, menuUserChartOptions);
  menuUserChart.render();

  // Ingredient Tracker Chart
  // -----------------------------
  ingredientUserChartOptions = {
    chart: {
      height: 270,
      type: 'radialBar'
    },
    plotOptions: {
      radialBar: {
        size: 150,
        offsetY: 20,
        startAngle: -150,
        endAngle: 150,
        hollow: {
          size: '65%'
        },
        track: {
          background: $white,
          strokeWidth: '100%'
        },
        dataLabels: {
          name: {
            offsetY: -5,
            color: $textHeadingColor,
            fontSize: '1rem'
          },
          value: {
            offsetY: 15,
            color: $textHeadingColor,
            fontSize: '1.714rem'
          }
        }
      }
    },
    colors: [window.colors.solid.danger],
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        type: 'horizontal',
        shadeIntensity: 0.5,
        gradientToColors: [window.colors.solid.primary],
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100]
      }
    },
    stroke: {
      dashArray: 8
    },
    series: [0],
    labels: ['Completed Review']
  };
  ingredientUserChart = new ApexCharts($ingredientUserChart, ingredientUserChartOptions);
  ingredientUserChart.render();
  // Chart Dashboard User 1 End

  // Chart Dashboard User 2 Start

  // Restaurant Tracker Chart
  // -----------------------------
  restaurantUser2ChartOptions = {
    chart: {
      height: 270,
      type: 'radialBar'
    },
    plotOptions: {
      radialBar: {
        size: 150,
        offsetY: 20,
        startAngle: -150,
        endAngle: 150,
        hollow: {
          size: '65%'
        },
        track: {
          background: $white,
          strokeWidth: '100%'
        },
        dataLabels: {
          name: {
            offsetY: -5,
            color: $textHeadingColor,
            fontSize: '1rem'
          },
          value: {
            offsetY: 15,
            color: $textHeadingColor,
            fontSize: '1.714rem'
          }
        }
      }
    },
    colors: [window.colors.solid.danger],
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        type: 'horizontal',
        shadeIntensity: 0.5,
        gradientToColors: [window.colors.solid.primary],
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100]
      }
    },
    stroke: {
      dashArray: 8
    },
    series: [100],
    labels: ['Completed Review']
  };
  restaurantUser2Chart = new ApexCharts($restaurantUser2Chart, restaurantUser2ChartOptions);
  restaurantUser2Chart.render();

  // Menu Tracker Chart
  // -----------------------------
  menuUser2ChartOptions = {
    chart: {
      height: 270,
      type: 'radialBar'
    },
    plotOptions: {
      radialBar: {
        size: 150,
        offsetY: 20,
        startAngle: -150,
        endAngle: 150,
        hollow: {
          size: '65%'
        },
        track: {
          background: $white,
          strokeWidth: '100%'
        },
        dataLabels: {
          name: {
            offsetY: -5,
            color: $textHeadingColor,
            fontSize: '1rem'
          },
          value: {
            offsetY: 15,
            color: $textHeadingColor,
            fontSize: '1.714rem'
          }
        }
      }
    },
    colors: [window.colors.solid.danger],
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        type: 'horizontal',
        shadeIntensity: 0.5,
        gradientToColors: [window.colors.solid.primary],
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100]
      }
    },
    stroke: {
      dashArray: 8
    },
    series: [63],
    labels: ['Completed Review']
  };
  menuUser2Chart = new ApexCharts($menuUser2Chart, menuUser2ChartOptions);
  menuUser2Chart.render();

  // Ingredient Tracker Chart
  // -----------------------------
  ingredientUser2ChartOptions = {
    chart: {
      height: 270,
      type: 'radialBar'
    },
    plotOptions: {
      radialBar: {
        size: 150,
        offsetY: 20,
        startAngle: -150,
        endAngle: 150,
        hollow: {
          size: '65%'
        },
        track: {
          background: $white,
          strokeWidth: '100%'
        },
        dataLabels: {
          name: {
            offsetY: -5,
            color: $textHeadingColor,
            fontSize: '1rem'
          },
          value: {
            offsetY: 15,
            color: $textHeadingColor,
            fontSize: '1.714rem'
          }
        }
      }
    },
    colors: [window.colors.solid.danger],
    fill: {
      type: 'gradient',
      gradient: {
        shade: 'dark',
        type: 'horizontal',
        shadeIntensity: 0.5,
        gradientToColors: [window.colors.solid.primary],
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 100]
      }
    },
    stroke: {
      dashArray: 8
    },
    series: [55],
    labels: ['Completed Review']
  };
  ingredientUser2Chart = new ApexCharts($ingredientUser2Chart, ingredientUser2ChartOptions);
  ingredientUser2Chart.render();
  // Chart Dashboard User 2 End
});
