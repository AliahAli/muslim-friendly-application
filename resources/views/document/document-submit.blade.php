@extends('layouts/contentLayoutMaster')

@section('title', 'Add Restaurant')

@section('vendor-style')
{{-- Vendor Css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
<section>
  <!-- warnings and primary alerts starts -->
  <div class="row">
    <div class="col-12">
      <div class="alert alert-primary" role="alert">
        <div class="alert-body">
          <strong>Info:</strong> Please upload a supporting document required for application to be reviewed.
        </div>
      </div>
    </div>
  </div>
  <!-- warnings and primary alerts ends -->
  <div class="card">
    <div class="card-header">
      <h4 class="card-title">Document Submission</h4>
    </div>
    <div class="card-body">
      <form>
        <div class="mb-1">
          <label for="customFile1" class="form-label">Company Registration Certificate</label>
          <input class="form-control" type="file" id="customFile1" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Business license (PBT)</label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Annual Financial Statements</label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">IC and Letter of appointment of 2 Muslim Employees (Malaysian Citizens)</label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Halal certificate of raw materials</label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Previous Halal Certificate (renewal application only)</label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Product manufacturing process flow chart</label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Location map to the factory</label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Packaging Labels
          </label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Complete Halal application file          </label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Complete Halal Assurance System
          </label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Factory/premise layout plan (Premises Layout)</label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Pest control records
          </label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Employee information records
          </label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Product production records</label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Purchase/invoice records</label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Letter of permission from the MOH Drug Control Authority
          </label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Import Permit from the Department of Veterinary Services Malaysia for meat/animal based products
          </label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Anti-thypoid injection records
          </label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Food handler course certificate
          </label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Product manufacturing records
          </label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div class="mb-1">
          <label for="customFile2" class="form-label">Veterinary Health Certificates from JPV/JOV certificate</label>
          <input class="form-control" type="file" id="customFile2" required />
        </div>
        <div>
          <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
            <div class="me-1">
              <a class="btn btn-outline-secondary" href="{{ route('application-app-form') }}">
                <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                <span>Back</span>
              </a>
            </div>
            <form class="chat-app-form" action="javascript:void(0);" onsubmit="confirmApplication();">
              <button type="button" class="btn btn-primary me-1" onclick="confirmApplication();">Submit</button>
            </form>
          </div>
        </div>
      </form>
    </div>
</section>
@endsection

@section('vendor-script')
<!-- vendor files -->
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection

@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/pages/alert-payment.js')) }}"></script>
@endsection