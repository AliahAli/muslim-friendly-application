@extends('layouts/contentLayoutMaster')

@section('title', 'Payment of 90%')

@section('vendor-style')
<!-- vendor css files -->
<link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap5.min.css')) }}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap5.min.css')) }}">
@endsection
@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <ul class="nav nav-pills mb-2">
      <!-- Payment -->
      <li class="nav-item">
        <a class="nav-link active" href="{{asset('page/account-settings-billing')}}">
          <i data-feather="bookmark" class="font-medium-3 me-50"></i>
          <span class="fw-bold">Payment</span>
        </a>
      </li>
    </ul>
    <!-- warnings and primary alerts starts -->
    <div class="row">
      <div class="col-12">
        <div class="alert alert-warning" role="alert">
          <div class="alert-body">
            <h5>Please make a <strong>90%</strong> of total payment first for the process of generating the QR Code and Training
              certificate.</h5>
          </div>
        </div>
      </div>
    </div>
    <!-- warnings and primary alerts ends -->
    <!-- payment methods -->
    <div class="card">
      <div class="card-header border-bottom">
        <h4 class="card-title">Payment Methods</h4>
      </div>
      <div class="card-body my-1 py-25">
        <div class="row gx-4">
          <div class="col-lg-6">
            <form id="creditCardForm" class="row gx-2 gy-1 validate-form" onsubmit="return false">
              <div class="col-12">
                <div class="form-check form-check-inline mb-1">
                  <input name="collapsible-payment" class="form-check-input" type="radio" value=""
                    id="collapsible-payment-cc" checked="" />
                  <label class="form-check-label" for="collapsible-payment-cc">Credit/Debit/ATM Card</label>
                </div>
                <div class="form-check form-check-inline mb-1">
                  <input name="collapsible-payment" class="form-check-input" type="radio" value=""
                    id="collapsible-payment-cash" />
                  <label class="form-check-label" for="collapsible-payment-cash">PayPal account</label>
                </div>
              </div>
              <div class="col-12 mt-0">
                <label class="form-label" for="addCardNumber">Card Number</label>
                <div class="input-group input-group-merge">
                  <input id="addCardNumber" name="addCard" class="form-control add-credit-card-mask" type="text"
                    placeholder="5637 8172 1290 7898" aria-describedby="addCard2"
                    data-msg="Please enter your credit card number" />
                  <span class="input-group-text cursor-pointer p-25" id="addCard2">
                    <span class="add-card-type"></span>
                  </span>
                </div>
              </div>

              <div class="col-md-6">
                <label class="form-label" for="addCardName">Name On Card</label>
                <input type="text" id="addCardName" class="form-control" placeholder="John Doe" />
              </div>

              <div class="col-6 col-md-3">
                <label class="form-label" for="addCardExpiryDate">Exp. Date</label>
                <input type="text" id="addCardExpiryDate" class="form-control add-expiry-date-mask"
                  placeholder="MM/YY" />
              </div>

              <div class="col-6 col-md-3">
                <label class="form-label" for="addCardCvv">CVV</label>
                <input type="text" id="addCardCvv" class="form-control add-cvv-code-mask" maxlength="3"
                  placeholder="cvv" />
              </div>

              <div class="col-12">
                <div class="d-flex align-items-center">
                  <div class="form-check form-switch form-check-primary me-25">
                    <input type="checkbox" class="form-check-input" id="addSaveCard" checked />
                    <label class="form-check-label" for="addSaveCard">
                      <span class="switch-icon-left"><i data-feather="check"></i></span>
                      <span class="switch-icon-right"><i data-feather="x"></i></span>
                    </label>
                  </div>
                  <label class="form-check-label fw-bolder" for="addSaveCard"> Save Card for future billing? </label>
                </div>
              </div>
              <div class="d-flex justify-content-between mt-3">
                <a class="btn btn-outline-secondary" href="{{ route('document-requirement') }}">
                  <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                  <span class="align-middle d-sm-inline-block d-none">Back</span>
                </a>
                <form class="chat-app-form" action="javascript:void(0);" onsubmit="confirmFirstPayment();">
                  <button type="button" class="btn btn-primary me-1" onclick="confirmFirstPayment();">Confirm Payment</button>
                </form>
              </div>
              <input type="hidden" />
            </form>
          </div>
          <div class="col-lg-6 mt-2 mt-lg-0">
            <h6 class="fw-bolder mb-2">My Cards</h6>
            <div class="added-cards">
              <div class="cardMaster rounded border p-2 mb-1">
                <div class="d-flex justify-content-between flex-sm-row flex-column">
                  <div class="card-information">
                    <img class="mb-1 img-fluid" src="{{asset('images/icons/payments/mastercard.png')}}"
                      alt="Master Card" />
                    <div class="d-flex align-items-center mb-50">
                      <h6 class="mb-0">Tom McBride</h6>
                      <span class="badge badge-light-primary ms-50">Primary</span>
                    </div>
                    <span class="card-number">∗∗∗∗ ∗∗∗∗ 9856</span>
                  </div>
                  <div class="d-flex flex-column text-start text-lg-end">
                    <div class="d-flex order-sm-0 order-1 mt-1 mt-sm-0">
                      <button class="btn btn-outline-primary me-75" data-bs-toggle="modal" data-bs-target="#editCard">
                        Edit
                      </button>
                      <button class="btn btn-outline-secondary">Delete</button>
                    </div>
                    <span class="mt-2">Card expires at 12/24</span>
                  </div>
                </div>
              </div>
              <div class="cardMaster border rounded p-2">
                <div class="d-flex justify-content-between flex-sm-row flex-column">
                  <div class="card-information">
                    <img class="mb-1 img-fluid" src="{{asset('images/icons/payments/visa.png')}}" alt="Visa Card" />
                    <h6>Mildred Wagner</h6>
                    <span class="card-number">∗∗∗∗ ∗∗∗∗ 5896</span>
                  </div>
                  <div class="d-flex flex-column text-start text-lg-end">
                    <div class="d-flex order-sm-0 order-1 mt-1 mt-sm-0">
                      <button class="btn btn-outline-primary me-75" data-bs-toggle="modal" data-bs-target="#editCard">
                        Edit
                      </button>
                      <button class="btn btn-outline-secondary">Delete</button>
                    </div>
                    <span class="mt-2">Card expires at 02/24</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / payment methods -->
  </div>
</div>

@include('content/_partials/_modals/modal-pricing')
@include('content/_partials/_modals/modal-edit-cc')
@endsection

@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/pages/alert-payment.js')) }}"></script>
@endsection

@section('vendor-script')
<!-- vendor files -->
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
@endsection