@extends('layouts/contentLayoutMaster')

@section('title', 'Document Submission')

@section('content')
<section>
  <div class="card">
    <div class="card-header">
      <h4 class="card-title">Requirements by PIJ</h4>
    </div>
    <div class="card-body">
      <p class="card-text mb-2 pb-1">
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam consequuntur error odit doloribus, corrupti magnam, cum maiores earum impedit accusamus excepturi veritatis non dignissimos molestiae quaerat ipsam? Sit, itaque ipsa!
      </p>

      <!-- single license -->
      <h5>Single License</h5>
      <ul class="ps-25 ms-1">
        <li>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Blanditiis aliquid provident minima odio voluptates quidem consequatur omnis, repellat quam voluptas nihil iusto? Quos expedita veniam sed mollitia. Reiciendis, ducimus numquam?</li>
        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum doloremque nostrum tempore deserunt animi dolor! Facilis eaque aspernatur sapiente necessitatibus nobis adipisci quaerat quo soluta explicabo ipsum sunt, earum dolor?</li>
      </ul>
      <p class="card-text mb-2 pb-75">
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Alias est voluptatibus, modi, impedit sed repellat illum soluta expedita eius ipsa, ad inventore ab hic. Quaerat incidunt tempora ullam ipsam facere!
      </p>

      <!-- multiple license -->
      <h5>Multiple License</h5>
      <ul class="ps-25 ms-1">
        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci, quo enim esse animi ut culpa incidunt voluptatem ratione. Autem consectetur labore rem necessitatibus nihil magni obcaecati explicabo ut quaerat tempore.</li>
        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas sunt repudiandae, soluta totam illum doloribus obcaecati reiciendis magnam, assumenda laudantium adipisci a ea nemo. Iusto dignissimos quia quaerat eum eos.</li>
      </ul>
      <p class="card-text mb-2 pb-75">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias expedita ad laudantium dicta aspernatur ut reprehenderit? Inventore, quae quo nesciunt reprehenderit sapiente temporibus deserunt nostrum? Dicta, quam! Delectus, iste cupiditate?
      </p>

      <!-- extended license -->
      <h5>Extended License</h5>
      <ul class="ps-25 ms-1">
        <li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illum earum quisquam quo blanditiis asperiores soluta aperiam, dolorum ipsam fugit sequi. Temporibus cumque sit libero voluptate dolores eos blanditiis, pariatur ducimus!</li>
        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure adipisci at doloremque dolorum doloribus deleniti quaerat, veniam molestiae inventore fugit fugiat cupiditate explicabo tempore placeat maiores esse sit. Maxime, animi?</li>
      </ul>
      <p class="card-text mb-2 pb-1">
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quia numquam, odit quaerat adipisci dignissimos cumque! Consectetur illo rem reiciendis officiis culpa nesciunt porro. Assumenda perferendis sunt molestiae maxime reprehenderit non.
      </p>
      <div class="form-check pb-1">
        <input type="checkbox" class="form-check-input" id="validationCheckBootstrap" required />
        <label class="form-check-label" for="validationCheckBootstrap">Agree to our terms and conditions</label>
        <div class="invalid-feedback">You must agree before submitting.</div>
      </div>
      <div>
        <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
          <div class="me-1">
            <a class="btn btn-outline-secondary" href="{{ route('training-test1') }}">
              <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
              <span>Back</span>
            </a>
          </div>
          <a class="btn btn-primary" href="{{ route('document-payment') }}">Submit</a>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection