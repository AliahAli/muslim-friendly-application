@extends('layouts/contentLayoutMaster2')

@section('title', 'Restaurant List')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
@endsection

@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
@endsection

@section('content')
<section class="invoice-list-wrapper">
  <div class="card">
    <div class="border-bottom">
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <h4 class="card-title">List of Rejected Restaurant</h4>
        </div>
      </div>
    </div>
    <div class="card-datatable table-responsive px-2 py-2">
      <table class="invoice-list-table table">
        <thead>
          <tr>
            <th>No</th>
            <th>Restaurant Name</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr class="align-top">
            <td><span class="fw-bold">1</span></td>
            <td>LKT Food Sdn Bhd</td>
            <td><span class="badge bg-light-danger">Rejected</span></td>
            <td>
                <div class="text-center d-inline-flex">
                  <a class="me-1" href="{{ route('admin-review') }}">
                    <i data-feather="eye" class="font-medium-3"></i>
                  </a>
                  <div class="modal-size-xl ">
                    <a class="me-1" data-bs-toggle="modal" data-bs-target="#xlarge">
                      <i data-feather="file-text" class="font-medium-3"></i>
                    </a>
                    <div class="modal fade text-start" id="xlarge" tabindex="-1" aria-labelledby="myModalLabel16" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered ">
                        <div class="modal-content">
                          <div class="card-datatable px-2 py-2">
                            <div class="card">
                              <div class="">
                                <label for="login-email" class="form-label">Remarks</label>
                                <input type="text" class="form-control" id="login-email" name="email"
                                  value="Incomplete document" aria-describedby="login-email" tabindex="1" autofocus/>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <a href="#">
                    <i data-feather="trash-2" class="font-medium-3"></i>
                  </a>
                </div>
              </td>
          </tr>
          <tr class="align-top">
            <td><span class="fw-bold">2</span></td>
            <td>Bidfood Malaysia Sdn Bhd</td>
            <td><span class="badge bg-light-danger">Rejected</span></td>
            <td>
                <div class="text-center d-inline-flex">
                  <a class="me-1" href="{{ route('admin-review') }}">
                    <i data-feather="eye" class="font-medium-3"></i>
                  </a>
                  <div class="modal-size-xl ">
                    <a class="me-1" data-bs-toggle="modal" data-bs-target="#xlarge">
                      <i data-feather="file-text" class="font-medium-3"></i>
                    </a>
                    <div class="modal fade text-start" id="xlarge" tabindex="-1" aria-labelledby="myModalLabel16" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered ">
                        <div class="modal-content">
                          <div class="card-datatable px-2 py-2">
                            <div class="card">
                              <div class="">
                                <label for="login-email" class="form-label">Remarks</label>
                                <input type="text" class="form-control" id="login-email" name="email"
                                  value="Incomplete document" aria-describedby="login-email" tabindex="1" autofocus/>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <a href="#">
                    <i data-feather="trash-2" class="font-medium-3"></i>
                  </a>
                </div>
              </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div>
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <a class="btn btn-outline-secondary" href="{{ route('dashboard-admin1') }}">
            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
            <span>Back</span>
          </a>
        </div>
        <a class="btn btn-primary" href="{{ route('authentication-login3') }}">
          <span>Next</span>
        </a>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/responsive.bootstrap5.js')}}"></script>
@endsection

{{-- @section('page-script')
<script src="{{asset('js/scripts/pages/app-invoice-list.js')}}"></script>
@endsection --}}