@extends('layouts/contentLayoutMaster2')

@section('title', 'Document Submission')

@section('vendor-style')
{{-- Vendor Css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
<section>
  <div class="card">
    <div class="card-header border-bottom">
      <h4 class="card-title">K Fry Urban Korean Holiday Villa Johor Bahru Document List</h4>
    </div>
    <div class="card-body my-2 py-25">
      <div class="row gx-4">
        <div class="col-lg-6">
          <h6 class="fw-bolder mb-2">Restaurant Details</h6>
        <ul class="list-unstyled align-items-center">
          <li class="mb-75">
            <span class="fw-bolder me-25">Name:</span>
            <span>K Fry Urban Korean Holiday Villa Johor Bahru</span>
          </li>
          <li class="mb-75">
            <span class="fw-bolder me-25">Address:</span>
            <span>No. 260, Jalan Dato Sulaiman, Taman Abad Johor Bahru 80250, Johor, Malaysia.</span>
          </li>
          <li class="mb-75">
            <span class="fw-bolder me-25">Contact:</span>
            <span>+60 7-266 0902</span>
          </li>
          <li class="mb-75">
            <span class="fw-bolder me-25">Email:</span>
            <span>kfry@malaysia.org</span>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<div class="card">
  <div class="border-bottom">
    <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
      <div class="me-1">
        <h4 class="card-title">Supporting Document</h4>
      </div>
    </div>
  </div>
  <div class="card-datatable table-responsive px-2 py-2">
    <table class="invoice-list-table table">
      <thead>
        <tr>
          <th>No</th>
          <th>Document</th>
          <th class="text-center">View</th>
        </tr>
        <tr class="align-top">
          <td>1</td>
          <td>Sijil Pendaftaran Syarikat</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Sijil Pendaftaran Syarikat</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>2</td>
          <td>Lesen perniagaan (PBT)</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Lesen perniagaan (PBT)</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>3</td>
          <td>Penyata Kewangan Tahunan</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Penyata Kewangan Tahunan</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>3</td>
          <td>IC dan Surat pelantikan 2 orang Pekerja Muslim (Warganegara Malaysia)</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >IC dan Surat pelantikan 2 orang Pekerja Muslim (Warganegara Malaysia)</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>4</td>
          <td>Sijil Halal bahan mentah</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Sijil Halal bahan mentah</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>5</td>
          <td>Sijil Halal terdahulu</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Sijil Halal terdahulu</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>6</td>
          <td>Carta alir proses pengilangan produk</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Carta alir proses pengilangan produk</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>7</td>
          <td>Peta lokasi ke kilang</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Peta lokasi ke kilang</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>8</td>
          <td>Label Pembungkusan</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Label Pembungkusan</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>9</td>
          <td>Fail permohonan Halal lengkap</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Fail permohonan Halal lengkap</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>10</td>
          <td>Sistem Jaminan Halal lengkap</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Sistem Jaminan Halal lengkap</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>11</td>
          <td>Pelan susur atur kilang/premis(Premis Layout)</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Pelan susur atur kilang/premis(Premis Layout)</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>12</td>
          <td>Rekod kawalan makhluk perosak</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Rekod kawalan makhluk perosak</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>13</td>
          <td>Rekod maklumat pekerja</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Rekod maklumat pekerja</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>14</td>
          <td>Rekod pengeluaran produk</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Rekod pengeluaran produk</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>15</td>
          <td>Rekod pembelian/invois</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Rekod pembelian/invois</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>16</td>
          <td>Permit Import dari Jabatan Perkhidmatan Haiwan Malaysia bagi daging/produk berasaskan Haiwan</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Permit Import dari Jabatan Perkhidmatan Haiwan Malaysia bagi daging/produk berasaskan Haiwan</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>17</td>
          <td>Rekod Suntkan anti-thypoid</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Rekod Suntkan anti-thypoid</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>18</td>
          <td>Sijil kursus pengendali makanan</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Sijil kursus pengendali makanan</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>19</td>
          <td>Rekod pengilangan produk</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Rekod pengilangan produk</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr class="align-top">
          <td>20</td>
          <td>Veterinary Health Certificates daripada JPV/Surat perakuan JOV</td>
          <td class="text-center">
            <div class="modal-size-xl d-inline-block">
              <!-- Button trigger modal -->
              <a data-bs-toggle="modal" data-bs-target="#xlarge">
                <i data-feather="eye" class="font-medium-3"></i>
              </a>
              <!-- Modal -->
              <div
                class="modal fade text-start"
                id="xlarge"
                tabindex="-1"
                aria-labelledby="myModalLabel16"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered ">
                  <div class="modal-content">
                    <div class="card-datatable px-2 py-2">
                      <div class="card">
                        <a href="{{ asset('page/blog/detail') }}">
                          <img class="card-img-top img-fluid" src="{{asset('images/slider/document-example.jpeg')}}" alt="Blog Post pic" />
                        </a>
                        <div class="card-body">
                          <h4 class="card-title">
                            <a href="{{ asset('page/blog/detail') }}" class="blog-title-truncate text-body-heading"
                              >Veterinary Health Certificates daripada JPV/Surat perakuan JOV</a>
                          </h4>
                          Click to download
                          <i data-feather="download" class="font-medium-3 me-50"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
      </thead>
    </table>
  </div>
  <div class="alert-body d-flex align-items-center flex-wrap p-2 justify-content-between">
    <div>
      <a class="btn btn-outline-secondary" href="{{ route('admin-rejected') }}">
        <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
        <span>Back</span>
      </a>
    </div>
    
</div>
@endsection

@section('vendor-script')
<!-- vendor files -->
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection

@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/pages/alert-payment.js')) }}"></script>
@endsection