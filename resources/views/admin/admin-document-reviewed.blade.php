@extends('layouts/contentLayoutMaster2')

@section('title', 'Review Application')

@section('vendor-style')
{{-- Vendor Css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
<section>
  <div class="card">
    <div class="card-header border-bottom">
      <h4 class="card-title">K Fry Urban Korean Holiday Villa Johor Bahru</h4>
    </div>
    <div class="card-body my-2 py-25">
      <div class="row gx-4">
        <div class="col-lg-12">
          <h6 class="fw-bolder mb-2">Restaurant Details</h6>
        <ul class="list-unstyled align-items-center">
          <li class="mb-75">
            <span class="fw-bolder me-25">Name:</span>
            <span>K Fry Urban Korean Holiday Villa Johor Bahru</span>
          </li>
          <li class="mb-75">
            <span class="fw-bolder me-25">Address:</span>
            <span>No. 260, Jalan Dato Sulaiman, Taman Abad Johor Bahru 80250, Johor, Malaysia.</span>
          </li>
          <li class="mb-75">
            <span class="fw-bolder me-25">Contact:</span>
            <span>+60 7-266 0902</span>
          </li>
          <li class="mb-75">
            <span class="fw-bolder me-25">Email:</span>
            <span>kfry@malaysia.org</span>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<div class="card">
  <div class="border-bottom">
    <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
      <div class="me-1">
        <h4 class="card-title">Supporting Document</h4>
      </div>
    </div>
  </div>
  <div class="card-datatable table-responsive px-2 py-2">
    <table class="invoice-list-table table">
      <thead>
        <tr>
          <th>No</th>
          <th>Document</th>
          <th class="text-center">View</th>
        </tr>
        <tr class="align-top">
          <td>1</td>
          <td>Sijil Pendaftaran Syarikat</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>2</td>
          <td>Lesen perniagaan (PBT)</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>3</td>
          <td>Penyata Kewangan Tahunan</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>3</td>
          <td>IC dan Surat pelantikan 2 orang Pekerja Muslim (Warganegara Malaysia)</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>4</td>
          <td>Sijil Halal bahan mentah</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>5</td>
          <td>Sijil Halal terdahulu</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>6</td>
          <td>Carta alir proses pengilangan produk</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>7</td>
          <td>Peta lokasi ke kilang</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>8</td>
          <td>Label Pembungkusan</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>9</td>
          <td>Fail permohonan Halal lengkap</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>10</td>
          <td>Sistem Jaminan Halal lengkap</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>11</td>
          <td>Pelan susur atur kilang/premis(Premis Layout)</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>12</td>
          <td>Rekod kawalan makhluk perosak</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>13</td>
          <td>Rekod maklumat pekerja</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>14</td>
          <td>Rekod pengeluaran produk</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>15</td>
          <td>Rekod pembelian/invois</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>16</td>
          <td>Permit Import dari Jabatan Perkhidmatan Haiwan Malaysia bagi daging/produk berasaskan Haiwan</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>17</td>
          <td>Rekod Suntkan anti-thypoid</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>18</td>
          <td>Sijil kursus pengendali makanan</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>19</td>
          <td>Rekod pengilangan produk</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
        <tr class="align-top">
          <td>20</td>
          <td>Veterinary Health Certificates daripada JPV/Surat perakuan JOV</td>
          <td class="text-center">
            <a href="{{asset('images/document/ssm-certificate.pdf')}}" target="_blank">
              <i data-feather="eye" class="font-medium-3"></i>
            </a>
          </td>
        </tr>
      </thead>
    </table>
  </div>
  <div class="alert-body d-flex align-items-center flex-wrap p-2 justify-content-between">
    <div>
      <a class="btn btn-outline-secondary" href="{{ route('admin-index') }}">
        <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
        <span>Back</span>
      </a>
    </div>
    <div class="ms-3 d-inline-block">
      <form class="d-inline-block" action="javascript:void(0);" onsubmit="verifiedApp();">
        <a type="button" class="btn btn-success m-1" onclick="verifiedApp();">Verify</a>
      </form>
      <form class="d-inline-block" action="javascript:void(0);" onsubmit="rejectApp();">
        <a type="button" class="d-inline-block btn btn-danger m-1" onclick="rejectApp();">Reject</a>
      </form>
    </div>
</div>
@endsection

@section('vendor-script')
<!-- vendor files -->
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection

@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/pages/alert-payment.js')) }}"></script>
@endsection