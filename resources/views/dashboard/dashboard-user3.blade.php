@extends('layouts/contentLayoutMaster6')

@section('title', 'Dashboard')

@section('vendor-style')
<!-- vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
@endsection
@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-invoice-list.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/dashboard-ecommerce.css')) }}">
@endsection

@section('content')
<!-- Dashboard Analytics Start -->
<section id="dashboard-analytics">
  <div class="row match-height">
    <!-- Greetings Card starts -->
    <div class="col-lg-6 col-md-12 col-sm-12">
      <div class="card card-congratulations">
        <div class="card-body text-center">
          <img src="{{asset('images/elements/decore-left.png')}}" class="congratulations-img-left"
            alt="card-img-left" />
          <img src="{{asset('images/elements/decore-right.png')}}" class="congratulations-img-right"
            alt="card-img-right" />
          <div class="avatar avatar-xl bg-primary shadow">
            <div class="avatar-content">
              <i data-feather="award" class="font-large-1"></i>
            </div>
          </div>
          <div class="text-center">
            <h1 class="mb-1 text-white">Congratulations Olivia,</h1>
            <p class="card-text m-auto w-75">
              Your restaurant menus has been approved.
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- Greetings Card ends -->

    <!-- Statistics Card -->
    <div class="col-xl-6 col-md-6 col-12">
      <div class="card card-statistics">
        <div class="card-header">
          <h4 class="card-title">Statistics</h4>
          <div class="d-flex align-items-center">
            <p class="card-text font-small-2 me-25 mb-0">Updated 1 day ago</p>
          </div>
        </div>
        <div class="card-body statistics-body">
          <div class="row">
            <div class="col-xl-4 col-sm-4 col-12 mb-2 mb-xl-0">
              <div class="d-flex flex-row">
                <div class="avatar bg-light-primary me-2">
                  <div class="avatar-content">
                    <i data-feather="clipboard" class="avatar-icon"></i>
                  </div>
                </div>
                <div class="my-auto">
                  <h4 class="fw-bolder mb-0">3</h4>
                  <p class="card-text font-small-3 mb-0">Restaurants</p>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-sm-4 col-12 mb-2 mb-xl-0">
              <div class="d-flex flex-row">
                <div class="avatar bg-light-info me-2">
                  <div class="avatar-content">
                    <i data-feather="book-open" class="avatar-icon"></i>
                  </div>
                </div>
                <div class="my-auto">
                  <h4 class="fw-bolder mb-0">4</h4>
                  <p class="card-text font-small-3 mb-0">Menus</p>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-sm-4 col-12 mb-2 mb-sm-0">
              <div class="d-flex flex-row">
                <div class="avatar bg-light-danger me-2">
                  <div class="avatar-content">
                    <i data-feather="package" class="avatar-icon"></i>
                  </div>
                </div>
                <div class="my-auto">
                  <h4 class="fw-bolder mb-0">12</h4>
                  <p class="card-text font-small-3 mb-0">Ingredients</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/ Statistics Card -->
  </div>

  <div class="row match-height">
    <!-- Restaurant Tracker Chart Card starts -->
    <div class="col-lg-6 col-12">
      <div class="card">
        <div class="card-header d-flex justify-content-between pb-0">
          <h4 class="card-title">Restaurants Tracker</h4>
          <div class="dropdown chart-dropdown">
            <button class="btn btn-sm border-0 dropdown-toggle p-50" type="button" id="dropdownItem4"
              data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Last 7 Days
            </button>
            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownItem4">
              <a class="dropdown-item" href="#">Last 28 Days</a>
              <a class="dropdown-item" href="#">Last Month</a>
              <a class="dropdown-item" href="#">Last Year</a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-sm-2 col-12 d-flex flex-column flex-wrap text-center">
              <h1 class="font-large-2 fw-bolder mt-2 mb-0">3</h1>
              <p class="card-text">Restaurants</p>
            </div>
            <div class="col-sm-10 col-12 d-flex justify-content-center">
              <div id="restaurantUser2-chart"></div>
            </div>
          </div>
          <div class="d-flex justify-content-between mt-1">
            <div class="text-center">
              <p class="card-text mb-50">Pending Review</p>
              <span class="font-large-1 fw-bold">0</span>
            </div>
            <div class="text-center">
              <p class="card-text mb-50">Approved Restaurants</p>
              <span class="font-large-1 fw-bold">3</span>
            </div>
            <div class="text-center">
              <p class="card-text mb-50">Rejected Restaurants</p>
              <span class="font-large-1 fw-bold">0</span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Restaurant Tracker Chart Card ends -->

    <!-- Menu Tracker Chart Card starts -->
    <div class="col-lg-6 col-12">
      <div class="card">
        <div class="card-header d-flex justify-content-between pb-0">
          <h4 class="card-title">Menus Tracker</h4>
          <div class="dropdown chart-dropdown">
            <button class="btn btn-sm border-0 dropdown-toggle p-50" type="button" id="dropdownItem4"
              data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Last 7 Days
            </button>
            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownItem4">
              <a class="dropdown-item" href="#">Last 28 Days</a>
              <a class="dropdown-item" href="#">Last Month</a>
              <a class="dropdown-item" href="#">Last Year</a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-sm-2 col-12 d-flex flex-column flex-wrap text-center">
              <h1 class="font-large-2 fw-bolder mt-2 mb-0">4</h1>
              <p class="card-text">Menus</p>
            </div>
            <div class="col-sm-10 col-12 d-flex justify-content-center">
              <div id="menuUser2-chart"></div>
            </div>
          </div>
          <div class="d-flex justify-content-between mt-1">
            <div class="text-center">
              <a class="text-black" href="#">
                <p class="card-text mb-50">Pending Review</p>
                <span class="font-large-1 fw-bold">2</span>
              </a>
            </div>
            <div class="text-center">
              <a class="text-black" href="#">
                <p class="card-text mb-50">Approved Menus</p>
                <span class="font-large-1 fw-bold">1</span>
              </a>
            </div>
            <div class="text-center">
              <a class="text-black" href="#">
                <p class="card-text mb-50">Rejected Menus</p>
                <span class="font-large-1 fw-bold">1</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Menu Tracker Chart Card ends -->

    <!-- Browser States Card -->
    <div class="col-lg-6 col-md-6 col-12">
      <div class="card card-browser-states">
        <div class="card-header">
          <div>
            <h4 class="card-title">Training Test Results</h4>
          </div>
          <div class="dropdown chart-dropdown">
            <i data-feather="more-vertical" class="font-medium-3 cursor-pointer" data-bs-toggle="dropdown"></i>
            <div class="dropdown-menu dropdown-menu-end">
              <a class="dropdown-item" href="#">Last 28 Days</a>
              <a class="dropdown-item" href="#">Last Month</a>
              <a class="dropdown-item" href="#">Last Year</a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="browser-states">
            <div class="d-flex">
              <h6 class="align-self-center mb-0">K Fry Urban Korean</h6>
            </div>
            <div class="d-flex align-items-center">
              <div class="fw-bold text-body-heading me-1">54.4%</div>
              <div id="browser-state-chart-primary"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/ Browser States Card -->

    <!-- Ingredient Tracker Chart Card starts -->
    <div class="col-lg-6 col-12">
      <div class="card">
        <div class="card-header d-flex justify-content-between pb-0">
          <h4 class="card-title">Ingredients Tracker</h4>
          <div class="dropdown chart-dropdown">
            <button class="btn btn-sm border-0 dropdown-toggle p-50" type="button" id="dropdownItem4"
              data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Last 7 Days
            </button>
            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownItem4">
              <a class="dropdown-item" href="#">Last 28 Days</a>
              <a class="dropdown-item" href="#">Last Month</a>
              <a class="dropdown-item" href="#">Last Year</a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-sm-2 col-12 d-flex flex-column flex-wrap text-center">
              <h1 class="font-large-2 fw-bolder mt-2 mb-0">12</h1>
              <p class="card-text">Ingredients</p>
            </div>
            <div class="col-sm-10 col-12 d-flex justify-content-center">
              <div id="ingredientUser2-chart"></div>
            </div>
          </div>
          <div class="d-flex justify-content-between mt-1">
            <div class="text-center">
              <a class="text-black" href="#">
                <p class="card-text mb-50">Pending Review</p>
                <span class="font-large-1 fw-bold">5</span>
              </a>
            </div>
            <div class="text-center">
              <a class="text-black" href="#">
                <p class="card-text mb-50">Approved Ingredients</p>
                <span class="font-large-1 fw-bold">4</span>
              </a>
            </div>
            <div class="text-center">
              <a class="text-black" href="#">
                <p class="card-text mb-50">Rejected Ingredients</p>
                <span class="font-large-1 fw-bold">3</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Ingredient Tracker Chart Card ends -->
  </div>

  <!-- List Menu Payment DataTable -->
  <div class="row">
    <div class="col-12">
      <div class="card invoice-list-wrapper">
        <div class="border-bottom p-2">
          <h4 class="card-title">Payment Report for Menu Registration</h4>
        </div>
        <div class="card-datatable table-responsive">
          <table class="invoice-list-table table">
            <thead>
              <tr>
                <th></th>
                <th>#</th>
                <th><i data-feather="trending-up"></i></th>
                <th>Restaurant</th>
                <th>Total</th>
                <th class="text-truncate">Issued Date</th>
                <th>Balance</th>
                <th>Invoice Status</th>
                <th class="cell-fit">Actions</th>
              </tr>
              <tr>
                <td colspan="9" class="text-center">No data found</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!--/ List Menu Payment DataTable -->
</section>
<!-- Dashboard Analytics end -->
@endsection

@section('vendor-script')
<!-- vendor files -->
<script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
@endsection
@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/pages/dashboard-analytics.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/pages/app-invoice-list.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/pages/dashboard-ecommerce.js')) }}"></script>
@endsection