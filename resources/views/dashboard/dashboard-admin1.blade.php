@extends('layouts/contentLayoutMaster2')

@section('title', 'Dashboard')

@section('vendor-style')
<!-- vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
@endsection
@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-invoice-list.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/dashboard-ecommerce.css')) }}">
@endsection

@section('content')
<!-- Dashboard Analytics Start -->
<section id="dashboard-analytics">
  <div class="row match-height">
    <!-- Subscribers Chart Card starts -->
    <div class="col-lg-3 col-sm-6 col-12">
      <div class="card">
        <div class="card-header flex-column align-items-start pb-0">
          <div class="avatar bg-light-primary p-50 m-0">
            <div class="avatar-content">
              <i data-feather="users" class="font-medium-5"></i>
            </div>
          </div>
          <h2 class="fw-bolder mt-1">20</h2>
          <p class="card-text">Users Gained</p>
        </div>
        <div id="gained-chart"></div>
      </div>
    </div>
    <!-- Subscribers Chart Card ends -->

    <!-- Subscribers Chart Card starts -->
    <div class="col-lg-3 col-sm-6 col-12">
      <div class="card">
        <div class="card-header flex-column align-items-start pb-0">
          <div class="avatar bg-light-success p-50 m-0">
            <div class="avatar-content">
              <i data-feather="clipboard" class="font-medium-5"></i>
            </div>
          </div>
          <h2 class="fw-bolder mt-1">20</h2>
          <p class="card-text">Restaurants Registered</p>
        </div>
        <div id="restaurant-chart"></div>
      </div>
    </div>
    <!-- Subscribers Chart Card ends -->

    <!-- Subscribers Chart Card starts -->
    <div class="col-lg-3 col-sm-6 col-12">
      <div class="card">
        <div class="card-header flex-column align-items-start pb-0">
          <div class="avatar bg-info p-50 m-0">
            <div class="avatar-content">
              <i data-feather="book-open" class="font-medium-5"></i>
            </div>
          </div>
          <h2 class="fw-bolder mt-1">70</h2>
          <p class="card-text">Menus Registered</p>
        </div>
        <div id="menu-chart"></div>
      </div>
    </div>
    <!-- Subscribers Chart Card ends -->

    <!-- Orders Chart Card starts -->
    <div class="col-lg-3 col-sm-6 col-12">
      <div class="card">
        <div class="card-header flex-column align-items-start pb-0">
          <div class="avatar bg-light-warning p-50 m-0">
            <div class="avatar-content">
              <i data-feather="package" class="font-medium-5"></i>
            </div>
          </div>
          <h2 class="fw-bolder mt-1">90</h2>
          <p class="card-text">Ingredients Registered</p>
        </div>
        <div id="ingredient-chart"></div>
      </div>
    </div>
    <!-- Orders Chart Card ends -->
  </div>

  <div class="row match-height">
    <!-- Restaurant Tracker Chart Card starts -->
    <div class="col-lg-6 col-12">
      <div class="card">
        <div class="card-header d-flex justify-content-between pb-0">
          <h4 class="card-title">Restaurants Tracker</h4>
          <div class="dropdown chart-dropdown">
            <button class="btn btn-sm border-0 dropdown-toggle p-50" type="button" id="dropdownItem4"
              data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Last 7 Days
            </button>
            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownItem4">
              <a class="dropdown-item" href="#">Last 28 Days</a>
              <a class="dropdown-item" href="#">Last Month</a>
              <a class="dropdown-item" href="#">Last Year</a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-sm-2 col-12 d-flex flex-column flex-wrap text-center">
              <h1 class="font-large-2 fw-bolder mt-2 mb-0">20</h1>
              <p class="card-text">Restaurants</p>
            </div>
            <div class="col-sm-10 col-12 d-flex justify-content-center">
              <div id="restaurant-trackers-chart"></div>
            </div>
          </div>
          <div class="d-flex justify-content-between mt-1">
            <div class="text-center">
              <a class="text-black" href="{{ route('admin-index') }}">
                <p class="card-text mb-50">New Restaurants</p>
                <span class="font-large-1 fw-bold">4</span>
              </a>
            </div>
            <div class="text-center">
              <a class="text-black" href="{{ route('admin-approved') }}">
                <p class="card-text mb-50">Approved Restaurants</p>
                <span class="font-large-1 fw-bold">18</span>
              </a>
            </div>
            <div class="text-center">
              <a class="text-black" href="{{ route('admin-rejected') }}">
                <p class="card-text mb-50">Rejected Restaurants</p>
                <span class="font-large-1 fw-bold">3</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Restaurant Tracker Chart Card ends -->

    <!-- Menu Tracker Chart Card starts -->
    <div class="col-lg-6 col-12">
      <div class="card">
        <div class="card-header d-flex justify-content-between pb-0">
          <h4 class="card-title">Menus Tracker</h4>
          <div class="dropdown chart-dropdown">
            <button class="btn btn-sm border-0 dropdown-toggle p-50" type="button" id="dropdownItem4"
              data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Last 7 Days
            </button>
            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownItem4">
              <a class="dropdown-item" href="#">Last 28 Days</a>
              <a class="dropdown-item" href="#">Last Month</a>
              <a class="dropdown-item" href="#">Last Year</a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-sm-2 col-12 d-flex flex-column flex-wrap text-center">
              <h1 class="font-large-2 fw-bolder mt-2 mb-0">70</h1>
              <p class="card-text">Menus</p>
            </div>
            <div class="col-sm-10 col-12 d-flex justify-content-center">
              <div id="menu-trackers-chart"></div>
            </div>
          </div>
          <div class="d-flex justify-content-between mt-1">
            <div class="text-center">
              <a class="text-black" href="{{ route('menu-index') }}">
                <p class="card-text mb-50">New Menus</p>
                <span class="font-large-1 fw-bold">15</span>
              </a>
            </div>
            <div class="text-center">
              <a class="text-black" href="{{ route('menu-approve') }}">
                <p class="card-text mb-50">Approved Menus</p>
                <span class="font-large-1 fw-bold">45</span>
              </a>
            </div>
            <div class="text-center">
              <a class="text-black" href="{{ route('menu-reject') }}">
                <p class="card-text mb-50">Rejected Menus</p>
                <span class="font-large-1 fw-bold">20</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Menu Tracker Chart Card ends -->

    <!-- Training Test Report Card -->
    <div class="col-lg-12 col-12">
      <div class="card p-2">
        <div class="row mx-0">
          <div class="col-md-12 col-12 revenue-report-wrapper">
            <div class="d-sm-flex justify-content-between align-items-center mb-3">
              <h4 class="card-title mb-50 mb-sm-0">Training Test Report</h4>
              <div class="d-flex align-items-center">
                <div class="d-flex align-items-center me-2">
                  <span class="bullet bullet-primary font-small-3 me-50 cursor-pointer"></span>
                  <span>Passed</span>
                </div>
                <div class="d-flex align-items-center ms-75">
                  <span class="bullet bullet-warning font-small-3 me-50 cursor-pointer"></span>
                  <span>Failed</span>
                </div>
              </div>
            </div>
            <div id="revenue-report-chart"></div>
          </div>
        </div>
      </div>
    </div>
    <!--/ Training Test Report Card -->
  </div>

  <!-- List Menu Payment DataTable -->
  <div class="row">
    <div class="col-12">
      <div class="card invoice-list-wrapper">
        <div class="border-bottom p-2">
          <h4 class="card-title">Payment Report for Menu Registration</h4>
        </div>
        <div class="card-datatable table-responsive">
          <table class="invoice-list-table table">
            <thead>
              <tr>
                <th></th>
                <th>#</th>
                <th><i data-feather="trending-up"></i></th>
                <th>Restaurant</th>
                <th>Total</th>
                <th class="text-truncate">Issued Date</th>
                <th>Balance</th>
                <th>Invoice Status</th>
                <th class="cell-fit">Actions</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!--/ List Menu Payment DataTable -->
</section>
<!-- Dashboard Analytics end -->
@endsection

@section('vendor-script')
<!-- vendor files -->
<script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
@endsection
@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/pages/dashboard-analytics.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/pages/app-invoice-list.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/pages/dashboard-ecommerce.js')) }}"></script>
@endsection