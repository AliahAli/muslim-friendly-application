@extends('layouts/contentLayoutMaster5')

@section('title', 'Restaurant List')

@section('vendor-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap5.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap5.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
<div class="card">
  <div class="border-bottom">
    <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
      <div class="me-1">
        <h4 class="card-title">List of Restaurant</h4>
        <p class="card-text">
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam consequuntur error odit doloribus, corrupti
          magnam, cum maiores earum impedit accusamus excepturi veritatis non dignissimos molestiae quaerat ipsam? Sit, itaque
          ipsa!
        </p>
      </div>
    </div>
  </div>
      <div class="card-datatable table-responsive px-2 py-2">
        <table class="invoice-list-table table">
          <thead>
            <tr>
              <th>No</th>
              <th>Restaurant Name</th>
              <th>Status</th>
              <th class="text-center">Menu</th>
            </tr>
          </thead>
          <tbody>
            <tr class="align-top">
              <td><span class="fw-bold">1</span></td>
              <td>LKT Food Sdn Bhd</td>
              <td><span class="badge bg-light-danger">Rejected</span></td>
              <td>
                <div class="text-center">
                  <a href="{{ route('menu-list') }}">
                    <i data-feather="eye" class="font-medium-3"></i>
                  </a>
                </div>
              </td>
            </tr>
            <tr class="align-top">
              <td><span class="fw-bold">2</span></td>
              <td>Bidfood Malaysia Sdn Bhd</td>
              <td><span class="badge bg-light-danger">Rejected</span></td>
              <td>
                <div class="text-center">
                  <a href="{{ route('menu-list') }}">
                    <i data-feather="eye" class="font-medium-3"></i>
                  </a>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <a class="btn btn-outline-secondary" href="{{ route('dashboard-admin2') }}">
            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
            <span>Back</span>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Basic Tables end -->
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/app-user-list.js')) }}"></script>
@endsection
