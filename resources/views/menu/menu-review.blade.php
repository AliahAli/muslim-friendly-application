@extends('layouts/contentLayoutMaster5')

@section('title', 'Review Menu')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
@endsection

@section('content')
<section>
  <!-- warnings and primary alerts starts -->
  <div class="card bg-light-primary">
    <ul class="list-unstyled px-2 py-2">
      <li>
        <span class="fw-bolder">Restaurant Name:</span>
        <span>K Fry Urban Korean Holiday Villa Johor Bahru</span>
      </li>
    </ul>
  </div>
  <!-- warnings and primary alerts ends -->

  {{--  Menu Details start --}}
  <div class="card">
    <div class="card-header border-bottom">
      <h4 class="card-title">Review Menu</h4>
    </div>
    <div class="card-body px-2 py-2 align-items-center">
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Menu Name :</label>
        <div class="col-sm-4">
          <span>FRIED CHICKEN SAMBAL</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Brand :</label>
        <div class="col-sm-4">
          <span>AROMA</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Does the product has specific information :</label>
        <div class="col-sm-4">
          <span>Yes</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Specific Information of Menu Name :</label>
        <div class="col-sm-4">
          <span>CAMPURAN AYAM GORENG SAMBAL</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Menu Category :</label>
        <div class="col-sm-4">
          <span>FOOD</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Menu Market :</label>
        <div class="col-sm-4">
          <span>International Market</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Ingredients Filled By :</label>
        <div class="col-sm-4">
          <span>Applicant [KESOM45]</span>
        </div>
      </div>
    </div>
  </div>
  </div>
  {{-- Menu Details ends --}}

  {{-- Ingredients --}}
  <div class="card">
    <div class="border-bottom">
      <div class="p-2">
        <h4 class="card-title">Ingredient List</h4>
        <h5>List all ingredients including ready made ingredients/products that used as component materials</h5>
      </div>
    </div>
    <div class="card-datatable table-responsive px-2 py-2">
      <table class="invoice-list-table table">
        <thead>
          <tr>
            <th>No</th>
            <th>Ingredient</th>
            <th class="cell-fit">Name & Address of Manufacturer</th>
            <th>Halal Status</th>
            <th>Status</th>
            <th class="text-center">Review</th>
          </tr>
          <tr class="align-top">
            <td>1</td>
            <td>Chilli Powder</td>
            <td>
              <div>Tusidhis Spice Marketing (M) Sdn Bhd</div>
              <div>No 5, Jalan Industri Ringan Permatang Tinggi 1, 14000 Bukit Mertajam, Pulau Pinang</div>
            </td>
            <td class="bg-light-success">
              <div>JAKIM</div>
              <div>Tarikh Tamat : 30/06/2022</div>
            </td>
            <td><span class="badge bg-light-success">Approved</span></td>
            <td>
              <div class="text-center">
                
              </div>
            </td>
          </tr>
          <tr class="align-top">
            <td>2</td>
            <td>Wheat Flour</td>
            <td>
              <div>SEBERANG FLOUR MILL SDN BHD</div>
              <div>Plot 112, Jalan Mawar, Pelabuhan Barat, 42920 Pulau Indah, Selangor</div>
            </td>
            <td class="bg-light-danger">
              <div>JAKIM</div>
              <div>Tarikh Tamat : 1/12/2021</div>
            </td>
            <td><span class="badge bg-light-secondary">New</span></td>
            <td>
              <div class="text-center">
                <a href="{{ route('ingredient-review') }}">
                  <i data-feather="external-link" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
          <tr class="align-top">
            <td>3</td>
            <td>Fine Salt (Garam Putih / Garam Laut)</td>
            <td>
              <div>SENG HIN BROTHERS ENTERPRISES SDN BHD</div>
              <div>Lot 156, Taman Perindustrian Integrasi Rawang, 48000 Rawang, Selangor</div>
            </td>
            <td>
              <div>JAKIM</div>
              <div>Tarikh Tamat : 31/10/2022</div>
            </td>
            <td><span class="badge bg-light-secondary">New</span></td>
            <td>
              <div class="text-center">
                <a href="{{ route('ingredient-review') }}">
                  <i data-feather="external-link" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
        </thead>
      </table>
    </div>
    <div>
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div>
          <a class="btn btn-outline-secondary" href="{{ route('menu-list') }}">
            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
            <span>Back</span>
          </a>
        </div>
        <div class="ms-3 d-inline-block">
          <form class="d-inline-block" action="javascript:void(0);" onsubmit="verifiedApp();">
            <a type="button" class="btn btn-success m-1" onclick="verifiedApp();">Finish</a>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection

@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/pages/alert-payment.js')) }}"></script>
@endsection