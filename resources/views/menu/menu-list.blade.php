@extends('layouts/contentLayoutMaster5')

@section('title', 'Menu List')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
@endsection

@section('content')
<section class="invoice-list-wrapper">
  <!-- warnings and primary alerts starts -->
  <div class="card bg-light-primary">
    <ul class="list-unstyled px-2 py-2">
      <li>
        <span class="fw-bolder">Restaurant Name:</span>
        <span>K Fry Urban Korean Holiday Villa Johor Bahru</span>
      </li>
    </ul>
  </div>
  <!-- warnings and primary alerts ends -->
  
  {{-- menu list start --}}
  <div class="card">
    <div class="border-bottom">
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <h4 class="card-title">Menu List</h4>
      </div>
    </div>
    <div class="card-datatable table-responsive px-2 py-2">
      <table class="invoice-list-table table">
        <thead>
          <tr>
            <th>No</th>
            <th>Menu Name</th>
            <th>Brand</th>
            <th>Category</th>
            <th>Market</th>
            <th>Menu</th>
            <th class="text-center cell-fit">Ingredients</th>
          </tr>
          <tr class="align-top">
            <td>1</td>
            <td>Fried Chicken Coating - Spicy</td>
            <td>Aroma</td>
            <td>Food</td>
            <td>International Market</td>
            <td>Applicant</td>
            <td>
              <div class="text-center">
                <a href="{{ route('menu-review') }}">
                  <i data-feather="eye" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
          <tr class="align-top">
            <td>2</td>
            <td>Fried Chicken Premix - Black Pepper</td>
            <td>Aroma</td>
            <td>Food</td>
            <td>Local Market</td>
            <td>Applicant</td>
            <td>
              <div class="text-center">
                <a href="{{ route('menu-review') }}">
                  <i data-feather="eye" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
          <tr class="align-top">
            <td>3</td>
            <td>Fried Chicken Sambal</td>
            <td>Aroma</td>
            <td>Food</td>
            <td>Local Market</td>
            <td>Applicant</td>
            <td>
              <div class="text-center">
                <a href="{{ route('menu-review') }}">
                  <i data-feather="eye" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
        </thead>
      </table>
    </div>
    <div>
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <a class="btn btn-outline-secondary" href="{{ route('menu-index') }}">
            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
            <span>Back</span>
          </a>
        </div>
      </div>
    </div>
  </div>
  {{-- menu list ends --}}
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/responsive.bootstrap5.js')}}"></script>
<script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection

@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/pages/alert-payment.js')) }}"></script>
@endsection