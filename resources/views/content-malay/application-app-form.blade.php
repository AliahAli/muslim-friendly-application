@extends('layouts/contentLayoutMaster7')

@section('title', 'Pendaftaran')

@section('vendor-style')
<!-- vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
@endsection

@section('content')
<section>
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">PENDAFTARAN</h4>
      </div>
      <div class="card-body">
        <h5 class="card-text">Sila daftarkan restoran anda</h5>
      </div>
    </div>
    <div class="card">
      <div class="card-body px-2 py-2 align-items-center">
        <form>
          <div class="mb-1 row">
            <label class="col-sm-2 col-form-label" for="first-name">Nama :</label>
            <div class="col-sm-5">
              <input type="text" class="form-control" id="colFormLabel" value="K Fry Urban Korean Holiday Villa Johor Bahru" />
            </div>
          </div>
          <div class="mb-1 row">
            <label class="col-sm-2 col-form-label" for="first-name">No. Pendaftaran Syarikat :</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" id="colFormLabel" value="JM0490503-V" />
            </div>
          </div>
          <div class="mb-1 row">
            <label class="col-sm-2 col-form-label" for="first-name">Status Syarikat :</label>
            <div class="col-sm-3">
              <select class="select2 form-select" id="default-select">
                <option>Bumiputera</option>
                <option>Bukan Bumiputera</option>
              </select>
            </div>
          </div>
          <div class="mb-1 row">
            <label class="col-sm-2 col-form-label" for="first-name">Alamat Syarikat :</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="colFormLabel"
                value="No. 260, Jalan Dato Sulaiman, Taman Abad Johor Bahru 80250, Johor, Malaysia." />
            </div>
          </div>
          <div class="mb-1 row">
            <label class="col-sm-2 col-form-label" for="first-name">Poskod :</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" id="colFormLabel" value="80250" />
            </div>
          </div>
          <div class="mb-1 row">
            <label class="col-sm-2 col-form-label" for="first-name">Bandar :</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" id="colFormLabel" value="Johor Bahru" />
            </div>
          </div>
          <div class="mb-1 row">
            <label class="col-sm-2 col-form-label" for="first-name">Daerah :</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" id="colFormLabel" value="Johor Bahru" />
            </div>
          </div>
          <div class="mb-1 row">
            <label class="col-sm-2 col-form-label" for="first-name">Negeri :</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" id="colFormLabel" value="Johor" />
            </div>
          </div>
          <div class="mb-1 row">
            <label for="colFormLabel" class="col-sm-2 col-form-label">No. Telefon :</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" id="colFormLabel" value="011-1103 2035" />
            </div>
          </div>
          <div class="mb-1 row">
            <label for="colFormLabel" class="col-sm-2 col-form-label">Faks :</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" id="colFormLabel" value="034569890" />
            </div>
          </div>

          <!-- Basic Radio Button start -->
          <section id="basic-radio">
            <div class="mb-1 row">
              <label class="col-sm-2 col-form-label" for="last-name">Jenis Perniagaan :</label>
              <div class="demo-inline-spacing col-sm-6">
                <div class=" col-sm-6">
                  <input class="form-check-input" type="radio" name="inlineRadioOptions1" id="inlineRadio1"
                    value="option1" checked />
                  <label class="form-check-label" for="inlineRadio1">Pengeluaran</label>
                </div>
                <div class="form-check form-check-inline  col-sm-6">
                  <input class="form-check-input" type="radio" name="inlineRadioOptions1" id="inlineRadio2"
                    value="option2" />
                  <label class="form-check-label" for="inlineRadio2">Kontrak Pengeluaran</label>
                </div>
                <div class="form-check form-check-inline  col-sm-6">
                  <input class="form-check-input" type="radio" name="inlineRadioOptions1" id="inlineRadio2"
                    value="option2" />
                  <label class="form-check-label" for="inlineRadio2">Pengedar / Dagangan</label>
                </div>
              </div>
            </div>
          </section>

          <section id="basic-radio">
            <div class="mb-1 row">
              <label class="col-sm-2 col-form-label" for="last-name">Jenis Industri :</label>
              <div class="demo-inline-spacing col-sm-6">
                <div class=" col-sm-6">
                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1"
                    value="option1" checked />
                  <label class="form-check-label" for="inlineRadio1">Industri Mikro</label>
                </div>
                <div class="form-check form-check-inline col-sm-6">
                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2"
                    value="option2" />
                  <label class="form-check-label" for="inlineRadio2">Industri Kecil</label>
                </div>
                <div class="form-check form-check-inline  col-sm-6">
                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2"
                    value="option2" />
                  <label class="form-check-label" for="inlineRadio2">Industri Sederhana
                  </label>
                </div>
                <div class="form-check form-check-inline  col-sm-6">
                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2"
                    value="option2" />
                  <label class="form-check-label" for="inlineRadio2">Multinational</label>
                </div>
              </div>
            </div>
          </section>
          <!-- Basic Radio Button end -->

          <div class="mb-1 row">
            <label for="colFormLabel" class="col-sm-2 col-form-label">Hasil Jualan Setahun RM:
            </label>
            <div class="col-sm-6">
              <select class="select form-select" id="default-select">
                <option>Nilai jualan tahunan dari RM15 juta hingga RM50 juta</option>
                <option>Nilai jualan tahunan dari RM60 juta hingga RM100 juta</option>
                <option>Nilai jualan tahunan dari RM200 juta hingga RM500 juta</option>
              </select>
            </div>
          </div>

          <!-- Basic Checkbox start -->
          <section id="basic-checkbox">
            <div class="mb-1 row">
              <label class="col-sm-2 col-form-label" for="last-name">Pasaran Produk :</label>
              <div class="demo-inline-spacing col-sm-6">
                <div class="col-sm-6">
                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" />
                  <label class="form-check-label" for="inlineCheckbox1">
                    Dalam Negara</label>
                </div>
                <div class="col-sm-6">
                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" checked/>
                  <label class="form-check-label" for="inlineCheckbox1">Luar Negara</label>
                </div>
              </div>
            </div>
          </section>
          <!-- Basic Checkbox end -->

          <section id="basic-checkbox">
            <div class="mb-1 row">
              <label class="col-sm-2 col-form-label" for="last-name">Waktu Operasi :</label>
              <div class="col-sm-12">
                <div class="card-body">
                  <div class="row">
                    <div class="demo-inline-spacing col-sm-4">
                      <div class=" form-check form-check-inline col-sm-6">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" />
                        <label class="form-check-label" for="inlineCheckbox1">Monday</label>
                      </div>
                      <div class="form-check form-check-inline col-sm-6">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" />
                        <label class="form-check-label" for="inlineCheckbox1">Tuesday</label>
                      </div>
                      <div class="form-check form-check-inline  col-sm-6">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" />
                        <label class="form-check-label" for="inlineCheckbox1">Wednesday</label>
                      </div>
                      <div class="form-check form-check-inline  col-sm-6">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" />
                        <label class="form-check-label" for="inlineCheckbox1">Thursday</label>
                      </div>
                      <div class="form-check form-check-inline  col-sm-6">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" />
                        <label class="form-check-label" for="inlineCheckbox1">Friday</label>
                      </div>
                      <div class="form-check form-check-inline  col-sm-6">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" />
                        <label class="form-check-label" for="inlineCheckbox1">Saturday</label>
                      </div>
                      <div class="form-check form-check-inline  col-sm-6">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" />
                        <label class="form-check-label" for="inlineCheckbox1">Sunday</label>
                      </div>
                    </div>
              
                    <div class="demo-inline-spacing col-sm-4">
                      <div class="col-md-7 mb-0">
                        <label class="form-label" for="fp-time">From</label>
                        <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM" />
                      </div>
                      <div class="col-md-7 mb-0">
                        <label class="form-label" for="fp-time">From</label>
                        <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM" />
                      </div>
                      <div class="col-md-7 mb-0">
                        <label class="form-label" for="fp-time">From</label>
                        <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM" />
                      </div>
                      <div class="col-md-7 mb-0">
                        <label class="form-label" for="fp-time">From</label>
                        <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM" />
                      </div>
                      <div class="col-md-7 mb-0">
                        <label class="form-label" for="fp-time">From</label>
                        <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM" />
                      </div>
                      <div class="col-md-7 mb-0">
                        <label class="form-label" for="fp-time">From</label>
                        <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM" />
                      </div>
                      <div class="col-md-7 mb-0">
                        <label class="form-label" for="fp-time">From</label>
                        <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM" />
                      </div>
                    </div>
                    <div class="demo-inline-spacing col-sm-4">
                      <div class="col-md-7 mb-0">
                        <label class="form-label" for="fp-time">To</label>
                        <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM" />
                      </div>
                      <div class="col-md-7 mb-0">
                        <label class="form-label" for="fp-time">To</label>
                        <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM" />
                      </div>
                      <div class="col-md-7 mb-0">
                        <label class="form-label" for="fp-time">To</label>
                        <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM" />
                      </div>
                      <div class="col-md-7 mb-0">
                        <label class="form-label" for="fp-time">To</label>
                        <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM" />
                      </div>
                      <div class="col-md-7 mb-0">
                        <label class="form-label" for="fp-time">To</label>
                        <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM" />
                      </div>
                      <div class="col-md-7 mb-0">
                        <label class="form-label" for="fp-time">To</label>
                        <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM" />
                      </div>
                      <div class="col-md-7 mb-0">
                        <label class="form-label" for="fp-time">From</label>
                        <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <div class="mb-1 row">
            <label for="colFormLabel" class="col-sm-2 col-form-label">Bilangan Syif :</label>
            <div class="col-sm-2">
              <select class="select form-select" id="default-select">
                <option>1</option>
                <option>2</option>
                <option>3</option>
              </select>
            </div>
          </div>
      </div>
      </form>
    </div>
  </div>

    <div class="card">
      <div class="border-bottom">
        <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
          <div class="me-1">
            <h4 class="card-title">PEGAWAI YANG BOLEH DIHUBUNGI</h4>
          </div>
          <a class="btn btn-primary" href="#">
            <i data-feather="plus" class="font-medium-3 me-50"></i>
            <span class="fw-bold">Tambah Baru</span>
          </a>
        </div>
      </div>
      <div class="card-datatable table-responsive px-2 py-2">
        <table class="invoice-list-table table">
          <thead>
            <tr>
              <th>Bil</th>
              <th>Nama</th>
              <th>Jawatan</th>
              <th>E-Mel</th>
              <th>No. Telefon</th>
              <th>Waktu Bertugas</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
            <tr>
              <td>1</td>
              <td>NURAZREENA BT MOHD RAMDZAN</td>
              <td>HALAL EXECUTIVE</td>
              <td>azzeryne@yahoo.com</td>
              <td>0123040686</td>
              <td>09:00 AM-18:00 PM</td>
              <td class="text-center">
                <div class="d-inline-flex">
                  <a class="me-1" href="#">
                    <i data-feather="edit" class="font-medium-3"></i>
                  </a>
                  <a href="#">
                    <i data-feather="trash-2" class="font-medium-3"></i>
                  </a>
                </div>
              </td>
            </tr>
            </tr>
            <tr>
            <tr>
              <td>2</td>
              <td>SITI ZULAIKHA BINTI ABDULLAH</td>
              <td>HALAL EXECUTIVE</td>
              <td>zulaikhaabdullah@gmail.com</td>
              <td>0189765467</td>
              <td>09:00 AM-18:00 PM</td>
              <td class="text-center">
                <div class="d-inline-flex">
                  <a class="me-1" href="#">
                    <i data-feather="edit" class="font-medium-3"></i>
                  </a>
                  <a href="#">
                    <i data-feather="trash-2" class="font-medium-3"></i>
                  </a>
                </div>
              </td>
            </tr>
            </tr>
          </tbody>
        </table>
      </div>
      </form>
    </div>

    <div class="card">
      <div class="border-bottom">
        <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
          <div class="me-1">
            <h4 class="card-title">
              EKSEKUTIF HALAL (MUSLIM SAHAJA)</h4>
          </div>
          <a class="btn btn-primary" href="#">
            <i data-feather="plus" class="font-medium-3 me-50"></i>
            <span class="fw-bold">Tambah Baru</span>
          </a>
        </div>
      </div>
      <div class="card-datatable table-responsive px-2 py-2">
        <table class="invoice-list-table table">
          <thead>
            <tr>
              <th>Bil</th>
              <th>Nama</th>
              <th>Jawatan</th>
              <th>No. K/P</th>
              <th>Telefon Bimbit</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>NURUL ZAFIRAH IYLIANA BINTI MOHD FADIL</td>
              <td>QA EXECUTIVE</td>
              <td>970521-01-6858</td>
              <td>0331023272</td>
              <td class="text-center">
                <div class="d-inline-flex">
                  <a class="me-1" href="#">
                    <i data-feather="edit" class="font-medium-3"></i>
                  </a>
                  <a href="#">
                    <i data-feather="trash-2" class="font-medium-3"></i>
                  </a>
                </div>
              </td>
            </tr>
            <tr>
              <td>2</td>
              <td>NURAZIAH JASMIN BINTI JAMALUDIN</td>
              <td>QC EXECUTIVE</td>
              <td>941022-10-5422</td>
              <td>0331023272</td>
              <td class="text-center">
                <div class="d-inline-flex">
                  <a class="me-1" href="#">
                    <i data-feather="edit" class="font-medium-3"></i>
                  </a>
                  <a href="#">
                    <i data-feather="trash-2" class="font-medium-3"></i>
                  </a>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      </form>
    </div>

    <div class="card">
      <div class="border-bottom">
        <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
          <div class="me-1">
            <h4 class="card-title">JAWATANKUASA HAL DALAMAN</h4>
          </div>
          <a class="btn btn-primary" href="#">
            <i data-feather="plus" class="font-medium-3 me-50"></i>
            <span class="fw-bold">Tambah Baru</span>
          </a>
        </div>
      </div>
      <div class="card-datatable table-responsive px-2 py-2">
        <table class="invoice-list-table table">
          <thead>
            <tr>
              <th> </th>
              <th>Nama</th>
              <th>Jawatan Dalam Syarikat</th>
              <th>Keanggotaan Dalam Jawatankuasa</th>
              <th>Email</th>
              <th>Telefon Bimbit</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>LUQMAN</td>
              <td>PURCHASING MANAGER</td>
              <td>AHLI JKHD</td>
              <td>luqman@gmail.com</td>
              <td>010200987</td>
              <td class="text-center">
                <div class="d-inline-flex">
                  <a class="me-1" href="#">
                    <i data-feather="edit" class="font-medium-3"></i>
                  </a>
                  <a href="#">
                    <i data-feather="trash-2" class="font-medium-3"></i>
                  </a>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      </form>
    </div>

    <div class="card">
      <div class="border-bottom">
        <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
          <div class="me-1">
            <h4 class="card-title">BILANGAN PEKERJA</h4>
          </div>
        </div>
      </div>
      <div class="card-datatable table-responsive px-2 py-2">
        <table class="invoice-list-table table">
          <thead>
            <tr>
              <th> </th>
              <th>Pengurusan</th>
              <th>Bahagian Pengeluaran
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Islam</td>
              <td>
                <div class="col-sm-3">
                  <input type="text" class="form-control" id="colFormLabel" value="5" /> orang
                </div>
              </td>
              <td>
                <div class="col-sm-3">
                  <input type="text" class="form-control" id="colFormLabel" value="45" /> orang
                </div>
              </td>
            </tr>
            <tr>
              <td>Bukan Islam</td>
              <td>
                <div class="col-sm-3">
                  <input type="text" class="form-control" id="colFormLabel" value="" /> orang
                </div>
              </td>
              <td>
                <div class="col-sm-3">
                  <input type="text" class="form-control" id="colFormLabel" value="" /> orang
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      </form>
      <div>
        <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
          <div class="me-1">
            <a class="btn btn-outline-secondary" href="{{ route('application-index-data') }}">
              <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
              <span>Kembali</span>
            </a>
          </div>
          <a class="btn btn-primary" href="{{ route('document-submit') }}">
            <i class="align-middle me-sm-25 me-0"></i>
            <span>Seterusnya</span>
          </a>
        </div>
      </div>
    </div>
</section>
<!-- /Horizontal Wizard -->
@endsection

@section('vendor-script')
<!-- vendor files -->
<script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
@endsection
@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/forms/form-repeater.js')) }}"></script>
@endsection