@extends('layouts/contentLayoutMaster7')

@section('title', 'QR Kod')

@section('content')
<section>
  <!-- warnings and primary alerts starts -->
  <div class="card bg-light-primary">
    <ul class="list-unstyled px-2 py-2">
      <li>
        <span class="fw-bolder">Nama Restoran:</span>
        <span>K Fry Urban Korean Holiday Villa Johor Bahru</span>
      </li>
    </ul>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="alert alert-warning" role="alert">
        <div class="alert-body">
          <h5><strong>Info:</strong> Kod QR untuk restoran anda telah berjaya dijana.</h5>
        </div>
      </div>
    </div>
  </div>
  <!-- warnings and primary alerts ends -->
  <div class="card">
    <div class="card-header border-bottom">
      <h4 class="card-title">Penjana Kod QR</h4>
    </div>
    <div class="card-body my-2 py-25">
      <div class="mx-auto px-2 pb-2">
        <span>Sila klik butang di bawah untuk memuat turun Kod QR anda.</span>
      </div>
      <div class="mx-auto px-2 pb-2">
        <a class="btn btn-info" href="{{asset('images/document/muslim-qrcode.pdf')}}" download>
          <i data-feather="download" class="align-middle me-sm-25 me-0"></i>
          <span>Muat Turun</span>
        </a>
      </div>
      <div>
        <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
          <div class="me-1">
            <a class="btn btn-outline-secondary" href="{{ route('qrcode-index') }}">
              <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
              <span>Kembali</span>
            </a>
          </div>
          <a class="btn btn-primary" href="{{ route('qrcode-menu') }}" target="_blank">
            <span>Seterusnya</span>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection