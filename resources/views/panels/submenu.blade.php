{{-- For submenu --}}
<ul class="menu-content">
  @if(isset($menu))
  @foreach($menu as $submenu)
  <li @if($submenu->slug === Route::currentRouteName()) class="active" @endif>
    @php
        if(isset($submenu->route)) {
            $submenu->url = route($submenu->route);
        } else if(isset($submenu->url)) {
            $submenu->url = url($submenu->url);
        } else {
            $submenu->url = 'javascript:void(0)';
        }
    @endphp
    <a href="{{$submenu->url}}" class="d-flex align-items-center" target="{{isset($submenu->newTab) && $submenu->newTab === true  ? '_blank':'_self'}}">
      @if(isset($submenu->icon))
      <i data-feather="{{$submenu->icon}}"></i>
      @endif
      <span class="menu-item text-truncate">{{ __('locale.'.$submenu->name) }}</span>
    </a>
    @if (isset($submenu->submenu))
    @include('panels/submenu', ['menu' => $submenu->submenu])
    @endif
  </li>
  @endforeach
  @endif
</ul>
