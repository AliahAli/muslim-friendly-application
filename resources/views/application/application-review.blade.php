@extends('layouts/contentLayoutMaster2')

@section('title', 'Review Application')

@section('vendor-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap5.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap5.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
@endsection

@section('content')

<section class="app-user-view-account">
  <div class="row">
    <!-- User Sidebar -->
    <div class="col-xl-4 col-lg-5 col-md-5 order-1 order-md-0">
      <!-- User Card -->
      <div class="card">
        <div class="card-body">
          <h4 class="fw-bolder border-bottom pb-50 mb-1">DETAILS</h4>
          <div class="info-container">
            <ul class="list-unstyled">
              <li class="mb-75">
                <span class="fw-bolder me-25">Company Name:</span>
                <span>KESOM45</span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25">Company registration number :</span>
                <span>KESOM45</span>
              </li>
              
              <li class="mb-75">
                <span class="fw-bolder me-25">Company Status:</span>
                <span >Bumiputera</span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25">Company Address:</span>
                <span>No.1, Jln Saudagar U1/16 Zon Perindustrian Hicom Glenmarie Seksyen U1, Shah Alam Selangor</span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25">Poscode:</span>
                <span>40150</span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25">City:</span>
                <span>Shah Alam</span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25">Area:</span>
                <span>Klang</span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25">State:</span>
                <span>Selangor</span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25">Phone Number:</span>
                <span>031234567</span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25">Fax:</span>
                <span>034569890</span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25">Company e-mel:</span>
                <span>warisannogori@gmail.com</span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25">Restaurant name:</span>
                <span >Asam Pedas Selera Johor</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- /User Card -->

      
    </div>
    <!--/ User Sidebar -->

    <!-- User Content -->
    <div class="col-xl-8 col-lg-7 col-md-7 order-0 order-md-1">

<!-- Plan Card -->
<div class="card border-primary">
  <div class="card-body">
    <h4 class="fw-bolder border-bottom pb-50 mb-1">BUSINESS INFORMATION</h4>
    <div class="info-container">
      <ul class="list-unstyled">
        <li class="mb-75">
          <span class="fw-bolder me-25">Business Type :</span>
          <span>Production</span>
        </li>
        <li class="mb-75">
          <span class="fw-bolder me-25">Industry Type :</span>
          <span>Small Industry</span>
        </li>
        <li class="mb-75">
          <span class="fw-bolder me-25">Annual Sales Revenue RM</span>
          <span >Annual sales value from RM15 Million to RM50 Million</span>
        </li>
        <li class="mb-75">
          <span class="fw-bolder me-25">Product Market:</span>
          <span>Local</span>
        </li>
        <li class="mb-75">
          <span class="fw-bolder me-25">Operation Hour :</span>
          <span>09:00 AM-18:00 PM</span>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- /Plan Card -->
<div class="row" id="basic-table">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">NUMBER OF EMPLOYEES</h4>
      </div>
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>   </th>
              <th>Management</th>
              <th>Production Division
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Muslim</td>
              <td>5 person</td>
              <td>45 person</td>
            </tr>
            <tr>
              <td>Non-muslim</td>
              <td>0 person</td>
              <td>0 person</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

      
    </div>
    <!--/ User Content -->
  </div>

  <div class="row" id="basic-table">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">OFFICERS WHO CAN BE CONTACTED</h4>
        </div>
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>  </th>
                <th>Name</th>
                <th>Position</th>
                <th>E-mel</th>
                <th>Phone Number</th>
                <th>Working Hour</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><span class="fw-bold">1</span></td>
                <td>NURAZREENA BT MOHD RAMDZAN</td>
                <td>HALAL EXECUTIVE</td>
                <td>azzeryne@yahoo.com</td>
                <td>0123040686</td>
                <td>09:00 AM-18:00 PM</td>
              </tr>
            </tbody>
          </table>
        </div>
        
      </div>
    </div>
  </div>

  
  <div class="row" id="basic-table">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">HALAL EXECUTIVE (MUSLIMS ONLY)</h4>
        </div>
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>  </th>
                <th>Name</th>
                <th>Position</th>       
                <th>ID No</th>
                <th>Phone Number</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><span class="fw-bold">1</span></td>
                <td>NURUL ZAFIRAH ILYANA BINTI MOHD FADIL</td>
                <td>QA EXECUTIVE</td>
                <td>970521-01-6858</td>
                <td>0331023272</td>
              </tr>
              <tr>
                <td><span class="fw-bold">2</span></td>
                <td>NURAZIAH JASMIN BINTI JAMALUDIN</td>
                <td>QC EXECUTIVE</td>
                <td>951022-10-5422</td>
                <td>0331023272</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="row" id="basic-table">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">INTERNAL HALAL COMMITTEE</h4>
        </div>
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>  </th>
                <th>Name</th>
                <th>Position</th>       
                <th>Membership In The Committee</th>
                <th>Email</th>
                <th>Phone Number</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><span class="fw-bold">1</span></td>
                <td>LUQMAN</td>
                <td>PURCHASING MANAGER</td>
                <td>MEMBER OF JKHD</td>
                <td>luqman@gmail.com</td>
                <td>010200987</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

 

  <div class="card">
    <div class="border-bottom">
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <h4 class="card-title">MENU LIST</h4>
        </div>
      </div>
    </div>
    <div class="card-datatable table-responsive px-2 py-2">
      <table class="invoice-list-table table">
        <thead>
          <tr>
            <th>No</th>
            <th>Product Name</th>
            <th>Brand</th>
            <th>Category</th>
            <th>Market</th>
            <th>Status</th>
            <th>Product</th>
            <th>Ingredients</th>
          </tr>
          <tr class="align-top">
            <td>1</td>
            <td>Fried Chicken Coating - Spicy</td>
            <td>Aroma</td>
            <td>Food</td>
            <td>International Market</td>
            <td><span class="badge bg-light-danger">Rejected</span></td>
            <td>Applicant</td>
            <td class="text-center">

              <div class="modal-size-xl d-inline-block">
                <!-- Button trigger modal -->
                <a data-bs-toggle="modal" data-bs-target="#xlarge">
                  <i data-feather="eye" class="font-medium-3"></i>
                </a>
                <!-- Modal -->
                <div
                  class="modal fade text-start"
                  id="xlarge"
                  tabindex="-1"
                  aria-labelledby="myModalLabel16"
                  aria-hidden="true"
                >
                  <div class="modal-dialog modal-dialog-centered modal-xl">
                    <div class="modal-content">
                      <div class="card-datatable table-responsive px-2 py-2">
                        <table class="invoice-list-table table">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Ingredient</th>
                              <th>Source of Ingredient</th>
                              <th class="cell-fit">Name & Address of Manufacturer</th>
                              <th>Halal Status</th>
                              <th>Action</th>
                            </tr>
                            <tr class="align-top">
                              <td>1</td>
                              <td>Chilli Powder</td>
                              <td>Plants</td>
                              <td>
                                <div>Tusidhis Spice Marketing (M) Sdn Bhd</div>
                                <div>No 5, Jalan Industri Ringan Permatang Tinggi 1, 14000 Bukit Mertajam, Pulau Pinang</div>
                              </td>
                              <td class="bg-light-danger">
                                <div>JAKIM</div>
                                <div>Tarikh Tamat : 1/12/2021</div>
                              </td>
                              <td>
                                <ul class="list-unstyled mt-1">
                                  <li class="mt-1"><input name="radiobutton" type="radio" class="form-check-input" id="radio" >
                                    <label class="form-check-label" for="validationCheckBootstrap">Approve</label>
                                  </li>
                                  <li class="mt-1"><input name="radiobutton" type="radio" class="form-check-input" id="radio">
                                    <label class="form-check-label" for="validationCheckBootstrap">Reject</label>
                                  </li>
                                </ul>
                              </td>
                            </tr>
                            <tr class="align-top">
                              <td>2</td>
                              <td>Wheat Flour</td>
                              <td>Plants</td>
                              <td>
                                <div>SEBERANG FLOUR MILL SDN BHD</div>
                                <div>Plot 112, Jalan Mawar, Pelabuhan Barat, 42920 Pulau Indah, Selangor</div>
                              </td>
                              <td class="bg-light-success">
                                <div>JAKIM</div>
                                <div>Tarikh Tamat : 30/06/2022</div>
                              </td>
                              <td>
                                <ul class="list-unstyled mt-1">
                                  <li class="mt-1"><input name="radiobutton2" type="radio" class="form-check-input" id="radio">
                                    <label class="form-check-label" for="validationCheckBootstrap">Approve</label>
                                  </li>
                                  <li class="mt-1"><input name="radiobutton2" type="radio" class="form-check-input" id="radio">
                                    <label class="form-check-label" for="validationCheckBootstrap">Reject</label>
                                  </li>
                                </ul>
                              </td>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </td>
            
          </tr>
          <tr class="align-top">
            <td>2</td>
            <td>Fried Chicken Premix - Black Pepper</td>
            <td>Aroma</td>
            <td>Food</td>
            <td>Local Market</td>
            <td><span class="badge bg-light-success">Approved</span></td>
            <td>Applicant</td>
            <td class="text-center">
              <div class="modal-size-xl d-inline-block">
                <!-- Button trigger modal -->
                <a data-bs-toggle="modal" data-bs-target="#xlarge">
                  <i data-feather="eye" class="font-medium-3"></i>
                </a>
                <!-- Modal -->
                <div
                  class="modal fade text-start"
                  id="xlarge"
                  tabindex="-1"
                  aria-labelledby="myModalLabel16"
                  aria-hidden="true"
                >
                  <div class="modal-dialog modal-dialog-centered modal-xl">
                    <div class="modal-content">
                      <div class="card-datatable table-responsive px-2 py-2">
                        <table class="invoice-list-table table">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Ingredient</th>
                              <th>Source of Ingredient</th>
                              <th class="cell-fit">Name & Address of Manufacturer</th>
                              <th>Halal Status</th>
                              <th>Action</th>
                            </tr>
                            <tr class="align-top">
                              <td>1</td>
                              <td>Chilli Powder</td>
                              <td>Plants</td>
                              <td>
                                <div>Tusidhis Spice Marketing (M) Sdn Bhd</div>
                                <div>No 5, Jalan Industri Ringan Permatang Tinggi 1, 14000 Bukit Mertajam, Pulau Pinang</div>
                              </td>
                              <td class="bg-light-danger">
                                <div>JAKIM</div>
                                <div>Tarikh Tamat : 1/12/2021</div>
                              </td>
                              <td>
                                <ul class="list-unstyled mt-1">
                                  <li class="mt-1"><input name="radiobutton" type="radio" class="form-check-input" id="radio">
                                    <label class="form-check-label" for="validationCheckBootstrap">Approve</label>
                                  </li>
                                  <li class="mt-1"><input name="radiobutton" type="radio" class="form-check-input" id="radio">
                                    <label class="form-check-label" for="validationCheckBootstrap">Reject</label>
                                  </li>
                                </ul>
                              </td>
                            </tr>
                            <tr class="align-top">
                              <td>2</td>
                              <td>Wheat Flour</td>
                              <td>Plants</td>
                              <td>
                                <div>SEBERANG FLOUR MILL SDN BHD</div>
                                <div>Plot 112, Jalan Mawar, Pelabuhan Barat, 42920 Pulau Indah, Selangor</div>
                              </td>
                              <td class="bg-light-success">
                                <div>JAKIM</div>
                                <div>Tarikh Tamat : 30/06/2022</div>
                              </td>
                              <td>
                                <ul class="list-unstyled mt-1">
                                  <li class="mt-1"><input name="radiobutton2" type="radio" class="form-check-input" id="radio">
                                    <label class="form-check-label" for="validationCheckBootstrap">Approve</label>
                                  </li>
                                  <li class="mt-1"><input name="radiobutton2" type="radio" class="form-check-input" id="radio">
                                    <label class="form-check-label" for="validationCheckBootstrap">Reject</label>
                                  </li>
                                </ul>
                              </td>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </td>
            
          </tr>
          <tr class="align-top">
            <td>3</td>
            <td>Fried Chicken Sambal</td>
            <td>Aroma</td>
            <td>Food</td>
            <td>Local Market</td>
            <td><span class="badge bg-light-success">Approved</span></td>
            <td>Applicant</td>
            <td class="text-center">

              <div class="modal-size-xl d-inline-block">
                <!-- Button trigger modal -->
                <a data-bs-toggle="modal" data-bs-target="#xlarge">
                  <i data-feather="eye" class="font-medium-3"></i>
                </a>
                <!-- Modal -->
                <div
                  class="modal fade text-start"
                  id="xlarge"
                  tabindex="-1"
                  aria-labelledby="myModalLabel16"
                  aria-hidden="true"
                >
                  <div class="modal-dialog modal-dialog-centered modal-xl">
                    <div class="modal-content">
                      <div class="card-datatable table-responsive px-2 py-2">
                        <table class="invoice-list-table table">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Ingredient</th>
                              <th>Source of Ingredient</th>
                              <th class="cell-fit">Name & Address of Manufacturer</th>
                              <th>Halal Status</th>
                              <th>Action</th>
                            </tr>
                            <tr class="align-top">
                              <td>1</td>
                              <td>Chilli Powder</td>
                              <td>Plants</td>
                              <td>
                                <div>Tusidhis Spice Marketing (M) Sdn Bhd</div>
                                <div>No 5, Jalan Industri Ringan Permatang Tinggi 1, 14000 Bukit Mertajam, Pulau Pinang</div>
                              </td>
                              <td class="bg-light-danger">
                                <div>JAKIM</div>
                                <div>Tarikh Tamat : 1/12/2021</div>
                              </td>
                              <td>
                                <ul class="list-unstyled mt-1">
                                  <li class="mt-1"><input name="radiobutton" type="radio" class="form-check-input" id="radio">
                                    <label class="form-check-label" for="validationCheckBootstrap">Approve</label>
                                  </li>
                                  <li class="mt-1"><input name="radiobutton" type="radio" class="form-check-input" id="radio">
                                    <label class="form-check-label" for="validationCheckBootstrap">Reject</label>
                                  </li>
                                </ul>
                              </td>
                            </tr>
                            <tr class="align-top">
                              <td>2</td>
                              <td>Wheat Flour</td>
                              <td>Plants</td>
                              <td>
                                <div>SEBERANG FLOUR MILL SDN BHD</div>
                                <div>Plot 112, Jalan Mawar, Pelabuhan Barat, 42920 Pulau Indah, Selangor</div>
                              </td>
                              <td class="bg-light-success">
                                <div>JAKIM</div>
                                <div>Tarikh Tamat : 30/06/2022</div>
                              </td>
                              <td>
                                <ul class="list-unstyled mt-1">
                                  <li class="mt-1"><input name="radiobutton2" type="radio" class="form-check-input" id="radio">
                                    <label class="form-check-label" for="validationCheckBootstrap">Approve</label>
                                  </li>
                                  <li class="mt-1"><input name="radiobutton2" type="radio" class="form-check-input" id="radio">
                                    <label class="form-check-label" for="validationCheckBootstrap">Reject</label>
                                  </li>
                                </ul>
                              </td>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </td>
            
          </tr>
        </thead>
      </table>
    </div>
    <div class="alert-body d-flex align-items-center flex-wrap p-2 justify-content-between">
      <div>
        <a class="btn btn-outline-secondary" href="{{ route('application-restaurant-list') }}">
          <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
          <span>Back</span>
        </a>
      </div>
      <div class="ms-3 d-inline-block">
        <form class="d-inline-block" action="javascript:void(0);" onsubmit="verifiedApp();">
          <a type="button" class="btn btn-success m-1" onclick="verifiedApp();">Verified</a>
        </form>
        <form class="d-inline-block" action="javascript:void(0);" onsubmit="rejectApp();">
          <a type="button" class="d-inline-block btn btn-danger m-1" onclick="rejectApp();">Reject</a>
        </form>
      </div>
  </div>
</section>

@include('content/_partials/_modals/modal-edit-user')
@include('content/_partials/_modals/modal-upgrade-plan')
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  {{-- data table --}}
  <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/modal-edit-user.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/pages/app-user-view-account.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/pages/app-user-view.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/pages/alert-payment.js')) }}"></script>
@endsection