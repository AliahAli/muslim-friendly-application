@extends('layouts/contentLayoutMaster')

@section('title', 'Restaurant List')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
@endsection

@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
@endsection

@section('content')
<section class="invoice-list-wrapper">
  <div class="card">
    <div class="border-bottom">
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <h4 class="card-title">List of Restaurant</h4>
        </div>
        <a class="btn btn-primary" href="{{route('application-app-form')}}">
          <i data-feather="plus" class="font-medium-3 me-50"></i>
          <span class="fw-bold">Add Restaurant</span>
        </a>
      </div>
    </div>
    <div class="card-datatable table-responsive px-2 py-2">
      <table class="invoice-list-table table">
        <thead>
          <tr >
            <th>No</th>
            <th>Restaurant Name</th>
            <th>Address</th>
            <th>Phone Number</th>
            <th class="cell-fit">Action</th>
          </tr>
          <tr class="align-top">
            <td>1</td>
            <td>K Fry Urban Korean Holiday Villa Johor Bahru</td>
            <td>No. 260, Jalan Dato Sulaiman, Taman Abad Johor Bahru 80250, Johor, Malaysia.</td>
            <td>011-1103 2035</td>
            <td class="text-center">
              <div class="d-inline-flex">
                <a class="me-1" href="{{ route('application-app-form') }}">
                  <i data-feather="edit" class="font-medium-3"></i>
                </a>
                <a href="#">
                  <i data-feather="trash-2" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
          <tr class="align-top">
            <td>2</td>
            <td>K Fry Urban Korean One Utama Shopping Center</td>
            <td>LG221A Bandar Utama City Centre, 1, Lebuh Bandar Utama, Bandar Utama, Petaling Jaya 47800, Selangor, Malaysia</td>
            <td>019-728 0633</td>
            <td class="text-center">
              <div class="d-inline-flex">
                <a class="me-1" href="{{ route('application-app-form') }}">
                  <i data-feather="edit" class="font-medium-3"></i>
                </a>
                <a href="#">
                  <i data-feather="trash-2" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
        </thead>
      </table>
    </div>
    <div>
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <a class="btn btn-outline-secondary" href="{{ route('document-submit') }}">
            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
            <span>Back</span>
          </a>
        </div>
        <a class="btn btn-primary" href="{{ route('authentication-login2') }}">
          <i class="align-middle me-sm-25 me-0"></i>
          <span>Next</span>
        </a>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/responsive.bootstrap5.js')}}"></script>
@endsection

{{-- @section('page-script')
<script src="{{asset('js/scripts/pages/app-invoice-list.js')}}"></script>
@endsection --}}