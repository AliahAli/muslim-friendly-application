@extends('layouts/contentLayoutMaster')

@section('title', 'Add Restaurant')

@section('vendor-style')
<!-- vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
@endsection

@section('content')
<section>
  <div class="card">
    <div class="card-header">
      <h4 class="card-title">REGISTRATION</h4>
    </div>
    <div class="card-body">
      <h5 class="card-text">Please register your restaurant</h5>
    </div>
  </div>
  <div class="card">
    <div class="card-body px-2 py-2 align-items-center">
      <form>
        <div class="mb-1 row">
          <label class="col-sm-2 col-form-label" for="first-name">Name :</label>
          <div class="col-sm-5">
            <input type="text" class="form-control" id="colFormLabel" value="K Fry Urban Korean Holiday Villa" />
          </div>
        </div>
        <div class="mb-1 row">
          <label class="col-sm-2 col-form-label" for="first-name">Company Registration No. :</label>
          <div class="col-sm-3">
            <input type="text" class="form-control" id="colFormLabel" value="JM0490503-V" />
          </div>
        </div>
        <div class="mb-1 row">
          <label for="colFormLabel" class="col-sm-2 col-form-label">Company Status :
          </label>
          <div class="col-sm-3">
            <select class="select2 form-select" id="default-select">
              <option>Bumiputera</option>
              <option>Non-Bumiputera</option>
            </select>
          </div>
        </div>
        <div class="mb-1 row">
          <label class="col-sm-2 col-form-label" for="first-name">Company Address :</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="colFormLabel"
              value="No. 260, Jalan Dato Sulaiman, Taman Abad Johor Bahru 80250, Johor, Malaysia" />
          </div>
        </div>
        <div class="mb-1 row">
          <label class="col-sm-2 col-form-label" for="first-name">Postcode :</label>
          <div class="col-sm-3">
            <input type="text" class="form-control" id="colFormLabel" value="80250" />
          </div>
        </div>
        <div class="mb-1 row">
          <label class="col-sm-2 col-form-label" for="first-name">City :</label>
          <div class="col-sm-3">
            <input type="text" class="form-control" id="colFormLabel" value="Johor Bahru" />
          </div>
        </div>
        <div class="mb-1 row">
          <label class="col-sm-2 col-form-label" for="first-name">District :</label>
          <div class="col-sm-3">
            <input type="text" class="form-control" id="colFormLabel" value="Johor Bahru" />
          </div>
        </div>
        <div class="mb-1 row">
          <label class="col-sm-2 col-form-label" for="first-name">State :</label>
          <div class="col-sm-3">
            <input type="text" class="form-control" id="colFormLabel" value="Johor" />
          </div>
        </div>
        <div class="mb-1 row">
          <label for="colFormLabel" class="col-sm-2 col-form-label">Phone Number :</label>
          <div class="col-sm-3">
            <input type="text" class="form-control" id="colFormLabel" value="6072660902" />
          </div>
        </div>
        <div class="mb-1 row">
          <label for="colFormLabel" class="col-sm-2 col-form-label">Fax :</label>
          <div class="col-sm-3">
            <input type="text" class="form-control" id="colFormLabel" value="034569890" />
          </div>
        </div>

        <!-- Basic Radio Button start -->
        <div class="mb-1 row">
          <label class="col-sm-2 col-form-label" for="last-name">Business Type :</label>
          <div class="demo-inline-spacing col-sm-6">
            <div class=" col-sm-6">
              <input class="form-check-input" type="radio" name="inlineRadioOptions1" 
                  checked />
              <label class="form-check-label" for="inlineRadio1">Production</label>
            </div>
            <div class="form-check form-check-inline col-sm-6">
              <input class="form-check-input" type="radio" name="inlineRadioOptions1" 
                  />
              <label class="form-check-label" for="inlineRadio2">Production Contract</label>
            </div>
            <div class="form-check form-check-inline  col-sm-6">
              <input class="form-check-input" type="radio" name="inlineRadioOptions1" 
                />
              <label class="form-check-label" for="inlineRadio2">Distributor/Trader</label>
            </div>
          </div>
        </div>
        
        <div class="mb-1 row">
          <label class="col-sm-2 col-form-label" for="last-name">Type of Industry :</label>
          <div class="demo-inline-spacing col-sm-6">
            <div class=" col-sm-6">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" 
                  />
              <label class="form-check-label" for="inlineRadio1">Micro Industry</label>
            </div>
            <div class="form-check form-check-inline col-sm-6">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" 
                />
              <label class="form-check-label" for="inlineRadio2">Small Industry</label>
            </div>
            <div class="form-check form-check-inline  col-sm-6">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" 
                  />
              <label class="form-check-label" for="inlineRadio2">Medium Industry
              </label>
            </div>
            <div class="form-check form-check-inline  col-sm-6">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" 
                checked />
              <label class="form-check-label" for="inlineRadio2">Multinational</label>
            </div>
          </div>
        </div>
        <!-- Basic Radio Button end -->

        <div class="mb-1 row">
          <label for="colFormLabel" class="col-sm-2 col-form-label">Annual Sales Revenue RM :
          </label>
          <div class="col-sm-6">
            <select class="select2 form-select" id="default-select">
              <option>Annual sales value from RM15 Million to RM50 Million</option>
              <option>Annual sales value from RM60 Million to RM100 Million</option>
              <option>Annual sales value from RM200 Million to RM500 Million</option>
            </select>
          </div>
        </div>

        <!-- Basic Checkbox start -->
        <section id="basic-checkbox">
          <div class="mb-1 row">
            <label class="col-sm-2 col-form-label" for="last-name">Product Market :</label>
            <div class="demo-inline-spacing col-sm-6">
              <div class="col-sm-6">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" />
                <label class="form-check-label" for="inlineCheckbox1">
                  Local Market</label>
              </div>
              <div class="col-sm-6">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" checked />
                <label class="form-check-label" for="inlineCheckbox1">International Market</label>
              </div>
            </div>
          </div>
        </section>
        <!-- Basic Checkbox end -->

        <!-- Flatpickr Starts -->
        <section >
          <div class="mb-1 row">
            <label class="col-sm-2 mb-0 col-form-label" for="last-name">Operating Hour:</label> 
          </div>
          <div class="col-sm-12">
            <div class="card-body">
              <div class="row">
                <div class="demo-inline-spacing col-sm-4">
                  <div class=" form-check form-check-inline col-sm-6">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" checked />
                    <label class="form-check-label" for="inlineCheckbox1">Monday</label>
                  </div>
                  <div class="form-check form-check-inline col-sm-6">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" checked/>
                    <label class="form-check-label" for="inlineCheckbox1">Tuesday</label>
                  </div>
                  <div class="form-check form-check-inline  col-sm-6">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" checked/>
                    <label class="form-check-label" for="inlineCheckbox1">Wednesday</label>
                  </div>
                  <div class="form-check form-check-inline  col-sm-6">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" checked />
                    <label class="form-check-label" for="inlineCheckbox1">Thursday</label>
                  </div>
                  <div class="form-check form-check-inline  col-sm-6">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" checked/>
                    <label class="form-check-label" for="inlineCheckbox1">Friday</label>
                  </div>
                  <div class="form-check form-check-inline  col-sm-6">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" />
                    <label class="form-check-label" for="inlineCheckbox1">Saturday</label>
                  </div>
                  <div class="form-check form-check-inline  col-sm-6">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" />
                    <label class="form-check-label" for="inlineCheckbox1">Sunday</label>
                  </div>
                </div>

                <div class="demo-inline-spacing col-sm-4">
                  <div class="col-md-7 mb-0">
                    <label class="form-label" for="fp-time">From</label>
                    <input type="text" id="fp-time" class="form-control flatpickr-time text-start" value="09:00" />
                  </div>
                  <div class="col-md-7 mb-0">
                    <label class="form-label" for="fp-time">From</label>
                    <input type="text" id="fp-time" class="form-control flatpickr-time text-start" value="09:00"  />
                  </div>
                  <div class="col-md-7 mb-0">
                    <label class="form-label" for="fp-time">From</label>
                    <input type="text" id="fp-time" class="form-control flatpickr-time text-start" value="09:00"  />
                  </div>
                  <div class="col-md-7 mb-0">
                    <label class="form-label" for="fp-time">From</label>
                    <input type="text" id="fp-time" class="form-control flatpickr-time text-start" value="09:00"  />
                  </div>
                  <div class="col-md-7 mb-0">
                    <label class="form-label" for="fp-time">From</label>
                    <input type="text" id="fp-time" class="form-control flatpickr-time text-start" value="09:00"  />
                  </div>
                  <div class="col-md-7 mb-0">
                    <label class="form-label" for="fp-time">From</label>
                    <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM"  disabled/>
                  </div>
                  <div class="col-md-7 mb-0">
                    <label class="form-label" for="fp-time">From</label>
                    <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM"  disabled/>
                  </div>
                </div>
                <div class="demo-inline-spacing col-sm-4">
                  <div class="col-md-7 mb-0">
                    <label class="form-label" for="fp-time">To</label>
                    <input type="text" id="fp-time" class="form-control flatpickr-time text-start" value="06:00"  />
                  </div>
                  <div class="col-md-7 mb-0">
                    <label class="form-label" for="fp-time">To</label>
                    <input type="text" id="fp-time" class="form-control flatpickr-time text-start" value="06:00"  />
                  </div>
                  <div class="col-md-7 mb-0">
                    <label class="form-label" for="fp-time">To</label>
                    <input type="text" id="fp-time" class="form-control flatpickr-time text-start" value="06:00"  />
                  </div>
                  <div class="col-md-7 mb-0">
                    <label class="form-label" for="fp-time">To</label>
                    <input type="text" id="fp-time" class="form-control flatpickr-time text-start" value="06:00"  />
                  </div>
                  <div class="col-md-7 mb-0">
                    <label class="form-label" for="fp-time">To</label>
                    <input type="text" id="fp-time" class="form-control flatpickr-time text-start" value="06:00"  />
                  </div>
                  <div class="col-md-7 mb-0">
                    <label class="form-label" for="fp-time">To</label>
                    <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM"  disabled />
                  </div>
                  <div class="col-md-7 mb-0">
                    <label class="form-label" for="fp-time">To</label>
                    <input type="text" id="fp-time" class="form-control flatpickr-time text-start" placeholder="HH:MM"  disabled />
                  </div>
                </div>
              </div>
            </div>
          </div> 
        </section>
        <!-- Flatpickr Ends-->

        <div class="mb-1 row">
          <label for="colFormLabel" class="col-sm-2 col-form-label">Number of Shifts :</label>
          <div class="col-sm-2">
            <select class="select2 form-select" id="default-select">
              <option>1</option>
              <option>2</option>
              <option>3</option>
            </select>
          </div>
        </div>
      </form>
    </div>
  </div>

  <div class="card">
    <div class="card-body px-2 py-2 align-items-center">
      <form>
        <div class="border-bottom">
          <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
            <div class="me-1">
              <h4 class="card-title">PERSON IN CHARGE</h4>
            </div>
            <a class="btn btn-primary" href="{{route('application-app-form')}}">
              <i data-feather="plus" class="font-medium-3 me-50"></i>
              <span class="fw-bold">Add New</span>
            </a>
          </div>
        </div>
        <div class="card-datatable table-responsive px-2 py-2">
          <table class="invoice-list-table table">
            <thead>
              <tr>
                <th> </th>
                <th>Name</th>
                <th>Position</th>
                <th>value</th>
                <th>Phone Number</th>
                <th>Working Hour</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
              <tr>
                <td>1</td>
                <td>NURAZREENA BT MOHD RAMDZAN</td>
                <td>HALAL EXECUTIVE</td>
                <td>azzeryne@yahoo.com</td>
                <td>0123040686</td>
                <td>09:00 AM-18:00 PM</td>
                <td class="text-center">
                  <div class="d-inline-flex">
                    <a class="me-1" href="#">
                      <i data-feather="edit" class="font-medium-3"></i>
                    </a>
                    <a href="#">
                      <i data-feather="trash-2" class="font-medium-3"></i>
                    </a>
                  </div>
                </td>
              </tr>
              </tr>
              <tr>
              <tr>
                <td>2</td>
                <td>SITI ZULAIKHA BINTI ABDULLAH</td>
                <td>HALAL EXECUTIVE</td>
                <td>zulaikhaabdullah@gmail.com</td>
                <td>0189765467</td>
                <td>09:00 AM-18:00 PM</td>
                <td class="text-center">
                  <div class="d-inline-flex">
                    <a class="me-1" href="#">
                      <i data-feather="edit" class="font-medium-3"></i>
                    </a>
                    <a href="#">
                      <i data-feather="trash-2" class="font-medium-3"></i>
                    </a>
                  </div>
                </td>
              </tr>
              </tr>
            </tbody>
          </table>
        </div>
      </form>
    </div>
  </div>

  <div class="card">
    
  </div>

  <div class="card">
    <div class="border-bottom">
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <h4 class="card-title">
            HALAL EXECUTIVE (MUSLIMS ONLY)</h4>
        </div>
        <a class="btn btn-primary" href="{{route('application-app-form')}}">
          <i data-feather="plus" class="font-medium-3 me-50"></i>
          <span class="fw-bold">Add New</span>
        </a>
      </div>
    </div>
    <div class="card-datatable table-responsive px-2 py-2">
      <table class="invoice-list-table table">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th>Position</th>
            <th>MyKad Number</th>
            <th>Phone Number</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>NURUL ZAFIRAH IYLIANA BINTI MOHD FADIL</td>
            <td>QA EXECUTIVE</td>
            <td>970521-01-6858</td>
            <td>0331023272</td>
            <td class="text-center">
              <div class="d-inline-flex">
                <a class="me-1" href="#">
                  <i data-feather="edit" class="font-medium-3"></i>
                </a>
                <a href="#">
                  <i data-feather="trash-2" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
          <tr>
            <td>2</td>
            <td>NURAZIAH JASMIN BINTI JAMALUDIN</td>
            <td>QC EXECUTIVE</td>
            <td>941022-10-5422</td>
            <td>0331023272</td>
            <td class="text-center">
              <div class="d-inline-flex">
                <a class="me-1" href="#">
                  <i data-feather="edit" class="font-medium-3"></i>
                </a>
                <a href="#">
                  <i data-feather="trash-2" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    </form>
  </div>

  <div class="card">
    <div class="border-bottom">
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <h4 class="card-title">INTERNAL HALAL COMMITTEE</h4>
        </div>
        <a class="btn btn-primary" href="{{route('application-app-form')}}">
          <i data-feather="plus" class="font-medium-3 me-50"></i>
          <span class="fw-bold">Add New</span>
        </a>
      </div>
    </div>
    <div class="card-datatable table-responsive px-2 py-2">
      <table class="invoice-list-table table">
        <thead>
          <tr>
            <th> </th>
            <th>Name</th>
            <th>Position In Company</th>
            <th>Membership In The Committee</th>
            <th>value</th>
            <th>Phone Number</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>LUQMAN</td>
            <td>PURCHASING MANAGER</td>
            <td>AHLI JKHD</td>
            <td>luqman@gmail.com</td>
            <td>010200987</td>
            <td class="text-center">
              <div class="d-inline-flex">
                <a class="me-1" href="#">
                  <i data-feather="edit" class="font-medium-3"></i>
                </a>
                <a href="#">
                  <i data-feather="trash-2" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    </form>
  </div>

  <div class="card">
    <div class="border-bottom">
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <h4 class="card-title">NUMBER OF EMPLOYEES</h4>
        </div>
      </div>
    </div>
    <div class="card-datatable table-responsive px-2 py-2">
      <table class="invoice-list-table table">
        <thead>
          <tr>
            <th> </th>
            <th>Management</th>
            <th>Production Division
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Muslim</td>
            <td>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="colFormLabel" value="5" /> person
              </div>
            </td>
            <td>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="colFormLabel" value="45" /> person
              </div>
            </td>
          </tr>
          <tr>
            <td>Non-Muslim</td>
            <td>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="colFormLabel" value="" /> person
              </div>
            </td>
            <td>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="colFormLabel" value="" /> person
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    </form>
    <div>
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <a class="btn btn-outline-secondary" href="{{ route('application-index-data') }}">
            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
            <span>Back</span>
          </a>
        </div>
        <a class="btn btn-primary" href="{{ route('document-submit') }}">
          <i class="align-middle me-sm-25 me-0"></i>
          <span>Next</span>
        </a>
      </div>
    </div>
  </div>
</section>

<!-- /Horizontal Wizard -->
@endsection

@section('vendor-script')
<!-- vendor files -->
<script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/forms/form-repeater.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>
@endsection