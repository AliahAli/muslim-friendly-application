@extends('layouts/contentLayoutMaster5')

@section('title', 'Ingredient')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
@endsection

@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
@endsection

@section('content')
<section>
  <div class="card">
    <div class="border-bottom p-2">
      <h4 class="card-title">Ingredient List</h4>
    </div>
    <div class="card-datatable table-responsive px-2 py-2">
      <table class="invoice-list-table table">
        <thead>
          <tr>
            <th>No</th>
            <th>Ingredient</th>
            <th>Source of Ingredient</th>
            <th class="cell-fit">Name & Address of Manufacturer</th>
            <th>Halal Status</th>
            <th>Status</th>
          </tr>
          <tr class="align-top">
            <td>1</td>
            <td>Chilli Powder</td>
            <td>Plants</td>
            <td>
              <div>Tusidhis Spice Marketing (M) Sdn Bhd</div>
              <div>No 5, Jalan Industri Ringan Permatang Tinggi 1, 14000 Bukit Mertajam, Pulau Pinang</div>
            </td>
            <td>
              <div>JAKIM</div>
              <div>Tarikh Tamat : 1/12/2023</div>
            </td>
            <td><span class="badge bg-light-success">Approved</span></td>
          </tr>
          <tr class="align-top">
            <td>2</td>
            <td>Wheat Flour</td>
            <td>Plants</td>
            <td>
              <div>SEBERANG FLOUR MILL SDN BHD</div>
              <div>Plot 112, Jalan Mawar, Pelabuhan Barat, 42920 Pulau Indah, Selangor</div>
            </td>
            <td>
              <div>JAKIM</div>
              <div>Tarikh Tamat : 30/06/2022</div>
            </td>
            <td><span class="badge bg-light-danger">Rejected</span></td>
          </tr>
          <tr class="align-top">
            <td>3</td>
            <td>Fine Salt (Garam Putih / Garam Laut)</td>
            <td>Natural</td>
            <td>
              <div>SENG HIN BROTHERS ENTERPRISES SDN BHD</div>
              <div>Lot 156, Taman Perindustrian Integrasi Rawang, 48000 Rawang, Selangor</div>
            </td>
            <td>
              <div>JAKIM</div>
              <div>Tarikh Tamat : 31/10/2022</div>
            </td>
            <td><span class="badge bg-light-success">Approved</span></td>
          </tr>
        </thead>
      </table>
    </div>
    <div>
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <a class="btn btn-outline-secondary" href="{{ route('dashboard-admin2') }}">
            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
            <span>Back</span>
          </a>
        </div>
        <a class="btn btn-primary" href="{{ route('authentication-login5') }}">
          <i class="align-middle me-sm-25 me-0"></i>
          <span>Next</span>
        </a>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/responsive.bootstrap5.js')}}"></script>
@endsection