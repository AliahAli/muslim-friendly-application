@extends('layouts/contentLayoutMaster5')

@section('title', 'Review Ingredient')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
@endsection

@section('content')
<section>
  <!-- warnings and primary alerts starts -->
  <div class="card bg-light-primary">
    <ul class="list-unstyled px-2 py-2">
      <li>
        <span class="fw-bolder">Menu Name:</span>
        <span>Fried Chicken Sambal</span>
      </li>
    </ul>
  </div>
  <!-- warnings and primary alerts ends -->
  
  {{-- Menu Details start --}}
  <div class="card">
    <div class="card-header border-bottom">
      <h4 class="card-title">Review Ingredient</h4>
    </div>
    <div class="card-body px-2 py-2 align-items-center">
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Ingredient (common name) :</label>
        <div class="col-sm-4">
          <span>Wheat Flour</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Ingredient (Chemical/ Scientific Name/ Trade Name) :</label>
        <div class="col-sm-4">
          <span>Triticum Aestivum</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Source of Ingredient :</label>
        <div class="col-sm-4">
          <span>Plants</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Manufacturer Name :</label>
        <div class="col-sm-4">
          <span>SEBERANG FLOUR MILL SDN BHD</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Manufacturer Address :</label>
        <div class="col-sm-4">
          <span>Plot 112, Jalan Mawar, Pelabuhan Barat, 42920 Pulau Indah, Selangor</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Country of Manufacturer :</label>
        <div class="col-sm-4">
          <span>Malaysia</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Halal Certificate :</label>
        <div class="col-sm-4">
          <span>Yes</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Certificate Expiry Date :</label>
        <div class="col-sm-4">
          <span>1/12/2021</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Other Document :</label>
        <div class="col-sm-4">
          <span>Processing Flowchart</span>
          <span>Sources of Raw Material</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Ingredient Category :</label>
        <div class="col-sm-4">
          <span>New Ingredient</span>
        </div>
      </div>
      <div class="mb-1 row">
        <label class="col-sm-5 fw-bolder" for="first-name">Halal Certificate :</label>
        <div class="col-sm-4">
          <a href="{{asset('images/document/halal-certificate.pdf')}}" target="_blank">
            <span>View file</span>
          </a>
        </div>
      </div>
    </div>
    <div>
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div>
          <a class="btn btn-outline-secondary" href="{{ route('menu-review') }}">
            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
            <span>Back</span>
          </a>
        </div>
        <div class="ms-3 d-inline-block">
          <form class="d-inline-block" action="javascript:void(0);" onsubmit="verifiedIngredient();">
            <a type="button" class="btn btn-success m-1" onclick="verifiedIngredient();">Approve</a>
          </form>
          <form class="d-inline-block" action="javascript:void(0);" onsubmit="rejectIngredient();">
            <a type="button" class="d-inline-block btn btn-danger m-1" onclick="rejectIngredient();">Reject</a>
          </form>
        </div>
      </div>
    </div>
  </div>
  {{-- Menu Details ends --}}
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/responsive.bootstrap5.js')}}"></script>
<script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection

@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/pages/alert-payment.js')) }}"></script>
@endsection