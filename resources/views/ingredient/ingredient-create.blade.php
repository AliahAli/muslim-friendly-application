@extends('layouts/contentLayoutMaster4')

@section('title', 'New Ingredient')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
@endsection

@section('content')

<section>
  {{-- Product [Menu] --}}
  <div class="card">
    <div class="card-header border-bottom">
      <h4 class="card-title">Add New Ingredient</h4>
    </div>
    <div class="card-body px-2 py-2 align-items-center">
      <form>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Ingredient (common name)</label>
          <input type="text" id="pincode1" class="form-control" value="Chilli Powder" />
        </div>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Ingredient (Chemical/ Scientific name/ Trade Name)</label>
          <input type="text" id="pincode1" class="form-control" value="Capsicum Annum" />
        </div>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Source of Ingredient</label>
          <select class="form-select" id="basicSelect">
            <option>Animal</option>
            <option>Chemical</option>
            <option selected>Plants</option>
            <option>Natural</option>
          </select>
        </div>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Manufacturer Name</label>
          <input type="text" id="pincode1" class="form-control" value="Tusidhis Spice Marketing (M) Sdn Bhd" />
        </div>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Manufacturer Address</label>
          <input type="text" id="pincode1" class="form-control" value="No 5, Jalan Industri Ringan Permatang Tinggi 1, 14000 Bukit Mertajam, Pulau Pinang" />
        </div>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Country of Manufacturer</label>
          <select class="form-select" id="basicSelect">
            <option selected>Malaysia</option>
            <option>Singapore</option>
            <option>Thailand</option>
            <option>Indonesia</option>
            <option>China</option>
          </select>
        </div>
        <div class="mb-1">
          <label class="fw-bolder" for="basicSelect">Halal Certificate</label>
          <select class="form-select" id="basicSelect">
            <option selected>Yes</option>
            <option>No</option>
          </select>
        </div>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Certificate Expiry Date</label>
          <input type="date" id="date" class="form-control" value="2021-12-01"/>
        </div>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Other Document</label>
          <ul class="list-unstyled mt-1">
            <li class="mt-1"><input type="checkbox" class="form-check-input" id="checkbox">
              <label class="form-check-label" for="checkbox">MSDS</label>
            </li>
            <li class="mt-1"><input type="checkbox" class="form-check-input" id="checkbox" checked>
              <label class="form-check-label" for="checkbox">Processing Flowchart</label>
            </li>
            <li class="mt-1"><input type="checkbox" class="form-check-input" id="checkbox" checked>
              <label class="form-check-label" for="checkbox">Sources of Raw Materials</label>
            </li>
            <li class="mt-1"><input type="checkbox" class="form-check-input" id="checkbox">
              <label class="form-check-label" for="checkbox">Others</label>
            </li>
          </ul>
        </div>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Ingredient Category</label>
          <ul class="list-unstyled mt-1">
            <li class="mt-1"><input name="category" type="radio" class="form-check-input" id="radio" checked>
              <label class="form-check-label" for="radio">New Ingredient</label>
            </li>
            <li class="mt-1"><input name="category" type="radio" class="form-check-input" id="radio">
              <label class="form-check-label" for="radio">Existing Ingredient</label>
            </li>
            <li class="mt-1"><input name="category" type="radio" class="form-check-input" id="radio">
              <label class="form-check-label" for="radio">New Supplier</label>
            </li>
          </ul>
        </div>
      </form>
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <a class="btn btn-outline-secondary" href="{{ route('product-create') }}">
            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
            <span>Back</span>
          </a>
        </div>
        <a class="btn btn-primary" href="{{ route('ingredient-document') }}">Next</a>
      </div>
    </div>
  </div>
  </div>
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/responsive.bootstrap5.js')}}"></script>
@endsection