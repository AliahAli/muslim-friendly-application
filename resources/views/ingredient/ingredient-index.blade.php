@extends('layouts/contentLayoutMaster4')

@section('title', 'Ingredient')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
@endsection

@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
@endsection

@section('content')

<section>
    <div class="card">
        <div class="border-bottom">
            <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
                <div class="me-1">
                    <h4 class="card-title">Ingredient List</h4>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <form class="flex flex-col md:flex-row flex-no wrap items-left">
                      <input :value="$page.props.value" type="text" name="value" class="form-control" placeholder="Search..." value="{{ request()->value }}">
                    </form>
                  </div>
                  <div class="col-lg-6">
                    <a class="btn btn-primary" href="{{route('ingredient-create')}}">
                      <i data-feather="plus" class="font-medium me-50"></i>
                      <span class="fw-bold">Add Ingredient</span>
                  </a>
                  </div>
              </div>
                
                
            </div>
        </div>
        <div class="card-datatable table-responsive px-2 py-2">
            <table class="invoice-list-table table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Ingredient Name</th>
                        <th>Brand</th>
                        <th class="cell-fit">Name & Address of Manufacturer</th>
                        <th>Halal Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                    @foreach($ingredients as $i => $ingredient)
                    <tr class="align-top">
                        <td>{{ $ingredients->firstItem() + $i }}</td>
                        <td>{{ $ingredient->name }}</td>
                        <td>{{ $ingredient->brand }}</td>
                        <td>
                            <div><b>{{ $ingredient->company->name }}</b></div>
                            <div>{{ $ingredient->company->address }}</div>
                        </td>
                        <td>
                            <div>JAKIM</div>
                            <div>Tarikh Tamat : {{ Carbon\Carbon::createFromFormat('Y-m-d', $ingredient->expired_date)->format('d/m/Y') }}</div>
                        </td>
                        <td>
                            <div class="d-inline-flex align-items-center">
                                <a class="me-1" href="#">
                                    <i data-feather="check-square" class="font-medium-3"></i>
                                </a>
                                <a class="me-1" href="#">
                                    <i data-feather="edit" class="font-medium-3"></i>
                                </a>
                                <a href="#">
                                    <i data-feather="trash-2" class="font-medium-3"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    @endforeach

                    @if ($ingredients->count() == 0)
                    <tr>
                        <td class="border-t px-6 py-4 text-center" colspan="6">No data found</td>
                    </tr>
                    @endif
                </thead>
            </table>
            {{ $ingredients->appends(['value' => request()->value])->links('pagination::template') }}
        </div>
        <div>
            <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
                <div class="me-1">
                    <a class="btn btn-outline-secondary" href="{{ route('product-create') }}">
                        <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                        <span>Back</span>
                    </a>
                </div>
                <a class="btn btn-primary" href="{{ route('product-createData') }}">Next</a>
            </div>
        </div>
    </div>
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/responsive.bootstrap5.js')}}"></script>
@endsection