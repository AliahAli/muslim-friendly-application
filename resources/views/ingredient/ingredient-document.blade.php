@extends('layouts/contentLayoutMaster4')

@section('title', 'New Ingredient')

@section('vendor-style')
{{-- Vendor Css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
<section>
  <!-- warnings and primary alerts starts -->
  <div class="row">
    <div class="col-12">
      <div class="alert alert-primary" role="alert">
        <div class="alert-body">
          <strong>Info:</strong> Please upload a documents required for ingredients.
        </div>
      </div>
    </div>
  </div>
  <!-- warnings and primary alerts ends -->
  <div class="card">
    <div class="card-header">
      <h4 class="card-title">Add New Ingredient</h4>
    </div>
    <div class="card-body">
      <form>
        <div class="mb-1">
          <label for="customFile1" class="form-label">Halal Certificate</label>
          <input class="form-control" type="file" id="customFile1" required />
        </div>
        <div>
          <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
            <div class="me-1">
              <a class="btn btn-outline-secondary" href="{{ route('ingredient-create') }}">
                <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                <span>Back</span>
              </a>
            </div>
            <a class="btn btn-primary" href="{{ route('product-createData') }}">Submit</a>
          </div>
        </div>
      </form>
    </div>
</section>
@endsection

@section('vendor-script')
<!-- vendor files -->
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
@endsection