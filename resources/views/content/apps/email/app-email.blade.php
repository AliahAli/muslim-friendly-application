
@extends('layouts/contentEmailLayout')

@section('title', 'Email Application')

@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/katex.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/monokai-sublime.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Inconsolata&family=Roboto+Slab&family=Slabo+27px&family=Sofia&family=Ubuntu+Mono&display=swap" rel="stylesheet">
@endsection

@section('page-style')
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-quill-editor.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-email.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/authentication.css')) }}">
@endsection


@section('content')
<section>
  <div class="card">
    <!-- Email search starts -->
    <div class="app-fixed-search d-flex ">
      <div class="sidebar-toggle d-block d-lg-none ms-1">
        <i data-feather="menu" class="font-medium-5"></i>
      </div>
      <div class="d-flex align-content-center justify-content-between w-100">
        <div class="input-group input-group-merge">
          <span class="input-group-text"><i data-feather="search" class="text-muted"></i></span>
          <input
            type="text"
            class="form-control"
            id="email-search"
            placeholder="Search email"
            aria-label="Search..."
            aria-describedby="email-search"
          />
        </div>
      </div>
    </div>
    <!-- Email search ends -->
  </div>
  
  <div class="column-card-wrapper">
    <!-- Email actions starts -->
    <div class="app-action">
      <div class="action-left">
        <div class="form-check selectAll">
          <input type="checkbox" class="form-check-input" id="selectAllCheck" />
          <label class="form-check-label fw-bolder ps-25" for="selectAllCheck">Select All</label>
        </div>
      </div>
      <div class="action-right">
        <ul class="list-inline m-0">
          <li class="list-inline-item">
            <div class="dropdown">
              <a
                href="#"
                class="dropdown-toggle"
                id="folder"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                <i data-feather="folder" class="font-medium-2"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-end" aria-labelledby="folder">
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <i data-feather="edit-2" class="font-small-4 me-50"></i>
                  <span>Draft</span>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <i data-feather="info" class="font-small-4 me-50"></i>
                  <span>Spam</span>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <i data-feather="trash" class="font-small-4 me-50"></i>
                  <span>Trash</span>
                </a>
              </div>
            </div>
          </li>
          <li class="list-inline-item">
            <div class="dropdown">
              <a
                href="#"
                class="dropdown-toggle"
                id="tag"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                <i data-feather="tag" class="font-medium-2"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-end" aria-labelledby="tag">
                <a href="#" class="dropdown-item"><span class="me-50 bullet bullet-success bullet-sm"></span>Personal</a>
                <a href="#" class="dropdown-item"><span class="me-50 bullet bullet-primary bullet-sm"></span>Company</a>
                <a href="#" class="dropdown-item"><span class="me-50 bullet bullet-warning bullet-sm"></span>Important</a>
                <a href="#" class="dropdown-item"><span class="me-50 bullet bullet-danger bullet-sm"></span>Private</a>
            </div>
          </div>
        </li>
        <li class="list-inline-item mail-unread">
          <span class="action-icon"><i data-feather="mail" class="font-medium-2"></i></span>
        </li>
        <li class="list-inline-item mail-delete">
          <span class="action-icon"><i data-feather="trash-2" class="font-medium-2"></i></span>
        </li>
      </ul>
    </div>
  </div>
  <!-- Email actions ends -->
    
    
  <!-- Email list starts -->
  <div class="email-user-list">
    <ul class="email-media-list">
      <li class="d-flex user-mail ">
        <div class="mail-left pe-50">
          
          <div class="user-action">
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="customCheck1" />
              <label class="form-check-label" for="customCheck1"></label>
            </div>
            <span class="email-favorite"><i data-feather="star"></i></span>
          </div>
        </div>
        <div class="mail-body">
          <div class="mail-details">
            <div class="mail-items">
              <h5 class="mb-25">Muslim Friendly App</h5>
              <span class="text-truncate">Welcome to Muslim Friendly App 👋 </span>
            </div>
            <div class="mail-meta-item">
              <span class="me-50 bullet bullet-success bullet-sm"></span>
              <span class="mail-date">4:14 AM</span>
            </div>
          </div>
          <div class="mail-message">
            <p class="text-truncate mb-0">
              Thank you for registering to Muslim Friendly App. You've entered <strong>user@yopmail.com</strong> as the email address for your Muslim Friendly App account. Please verify this email address by clicking the button below:
            </p>
          </div>
        </div>
      </li>
      <li class="d-flex user-mail ">
        <div class="mail-left pe-50">
          
          <div class="user-action">
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="customCheck1" />
              <label class="form-check-label" for="customCheck1"></label>
            </div>
            <span class="email-favorite"><i data-feather="star"></i></span>
          </div>
        </div>
        <div class="mail-body">
          <div class="mail-details">
            <div class="mail-items">
              <h5 class="mb-25">Ardis Balderson</h5>
              <span class="text-truncate">Focused impactful open system 👋 </span>
            </div>
            <div class="mail-meta-item">
              <span class="me-50 bullet bullet-success bullet-sm"></span>
              <span class="mail-date">4:14 AM</span>
            </div>
          </div>
          <div class="mail-message">
            <p class="text-truncate mb-0">
              Hey John,

              bah kivu decrete epanorthotic unnotched Argyroneta nonius veratrine preimaginary saunders demidolmen Chaldaic allusiveness lorriker unworshipping ribaldish tableman hendiadys outwrest unendeavored fulfillment scientifical Pianokoto Chelonia

              Freudian sperate unchary hyperneurotic phlogiston duodecahedron unflown Paguridea catena disrelishable Stygian paleopsychology cantoris phosphoritic disconcord fruited inblow somewhatly ilioperoneal forrard palfrey Satyrinae outfreeman melebiose
            </p>
          </div>
        </div>
      </li>
      <li class="d-flex user-mail ">
        <div class="mail-left pe-50">
          
          <div class="user-action">
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="customCheck1" />
              <label class="form-check-label" for="customCheck1"></label>
            </div>
            <span class="email-favorite"><i data-feather="star"></i></span>
          </div>
        </div>
        <div class="mail-body">
          <div class="mail-details">
            <div class="mail-items">
              <h5 class="mb-25">Modestine Spat</h5>
              <span class="text-truncate">Profound systemic alliance 🎉 🎊</span>
            </div>
            <div class="mail-meta-item">
              <span class="me-50 bullet bullet-success bullet-sm"></span>
              <span class="mail-date">4:14 AM</span>
            </div>
          </div>
          <div class="mail-message">
            <p class="text-truncate mb-0">
              Parthenopean logeion chipwood tonsilitic cockleshell substance Stilbum dismayed tape Alderamin Phororhacos bridewain zoonomia lujaurite printline extraction weanedness charterless splitmouth bindoree unfit philological Pythonissa scintillescent

              cinchonism sabbaton thyrocricoid dissuasively schematograph immerse pristane stimulability unreligion uncomplemental uteritis nef bavenite Hachiman teleutosorus anterolateral infirmate Nahani Hyla barile farthing crea venesector Cirrostomi            </p>
          </div>
        </div>
      </li>
    </ul>
  </div>
  <!-- Email list ends -->
  
  <!--/ Email list Area -->
  <!-- Detailed Email View -->
  <div class="email-app-details">
    <!-- Detailed Email Header starts -->
    <div class="email-detail-header">
      <div class="email-header-left d-flex align-items-center">
        <span class="go-back me-1"><i data-feather="chevron-left" class="font-medium-4"></i></span>
        <h4 class="email-subject mb-0">Welcome to Muslim Friendly App 👋</h4>
      </div>
      <div class="email-header-right ms-2 ps-1">
        <ul class="list-inline m-0">
          <li class="list-inline-item">
            <span class="action-icon favorite"><i data-feather="star" class="font-medium-2"></i></span>
          </li>
          <li class="list-inline-item">
            <div class="dropdown no-arrow">
              <a href="#" class="dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i data-feather="folder" class="font-medium-2"></i>
              </a>
              <div class="dropdown-menu" aria-labelledby="folder">
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <i data-feather="edit-2" class="font-medium-3 me-50"></i>
                  <span>Draft</span>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <i data-feather="info" class="font-medium-3 me-50"></i>
                  <span>Spam</span>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <i data-feather="trash" class="font-medium-3 me-50"></i>
                  <span>Trash</span>
                </a>
              </div>
            </div>
          </li>
          <li class="list-inline-item">
            <div class="dropdown no-arrow">
              <a href="#" class="dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i data-feather="tag" class="font-medium-2"></i>
              </a>
              <div class="dropdown-menu" aria-labelledby="tag">
                <a href="#" class="dropdown-item"><span class="me-50 bullet bullet-success bullet-sm"></span>Personal</a>
                <a href="#" class="dropdown-item"><span class="me-50 bullet bullet-primary bullet-sm"></span>Company</a>
                <a href="#" class="dropdown-item"><span class="me-50 bullet bullet-warning bullet-sm"></span>Important</a>
                <a href="#" class="dropdown-item"><span class="me-50 bullet bullet-danger bullet-sm"></span>Private</a>
              </div>
            </div>
          </li>
          <li class="list-inline-item">
            <span class="action-icon"><i data-feather="mail" class="font-medium-2"></i></span>
          </li>
          <li class="list-inline-item">
            <span class="action-icon"><i data-feather="trash" class="font-medium-2"></i></span>
          </li>
          <li class="list-inline-item email-prev">
            <span class="action-icon"><i data-feather="chevron-left" class="font-medium-2"></i></span>
          </li>
          <li class="list-inline-item email-next">
            <span class="action-icon"><i data-feather="chevron-right" class="font-medium-2"></i></span>
          </li>
        </ul>
      </div>
    </div>
    <!-- Detailed Email Header ends -->
  
    <!-- Detailed Email Content starts -->
    <div class="email-scroll-area">
      <div class="row">
        <div class="col-12">
          <div class="email-label">
            <span class="mail-label badge rounded-pill badge-light-primary">Company</span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header email-detail-head">
              <div class="user-details d-flex justify-content-between align-items-center flex-wrap">
                <div class="avatar me-75">
                  <img
                    src="{{asset('images/portrait/small/avatar-s-20.jpg')}}"
                    alt="avatar img holder"
                    width="48"
                    height="48"
                  />
                </div>
                <div class="mail-items">
                  <h5 class="mb-0">User</h5>
                  <div class="email-info-dropup dropdown">
                    <span
                      role="button"
                      class="dropdown-toggle font-small-3 text-muted"
                      id="card_top01"
                      data-bs-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                    noreply@muslimfriendlyapp.com
                    </span>
                    <div class="dropdown-menu" aria-labelledby="card_top01">
                      <table class="table table-sm table-borderless">
                        <tbody>
                          <tr>
                            <td class="text-end">From:</td>
                            <td>noreply@muslimfriendlyapp.com</td>
                          </tr>
                          <tr>
                            <td class="text-end">To:</td>
                            <td>kesom45@ow.ly</td>
                          </tr>
                          <tr>
                            <td class="text-end">Date:</td>
                            <td>14:58, 1 Jan 2022</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="mail-meta-item d-flex align-items-center">
                <small class="mail-date-time text-muted">1 Jan 2022, 14:58</small>
                <div class="dropdown ms-50">
                  <div
                    role="button"
                    class="dropdown-toggle hide-arrow"
                    id="email_more"
                    data-bs-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <i data-feather="more-vertical" class="font-medium-2"></i>
                  </div>
                  <div class="dropdown-menu dropdown-menu-end" aria-labelledby="email_more">
                    <div class="dropdown-item"><i data-feather="corner-up-left" class="me-50"></i>Reply</div>
                    <div class="dropdown-item"><i data-feather="corner-up-right" class="me-50"></i>Forward</div>
                    <div class="dropdown-item"><i data-feather="trash-2" class="me-50"></i>Delete</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-body mail-message-wrapper pt-2">
              <div class="mail-message">
                <p class="card-text">Hai User,</p>
                <p class="card-text">
                  Thank you for registering to Muslim Friendly App. You've entered <strong>user@yopmail.com</strong> as the email address for your Muslim Friendly App account. Please verify this email address by clicking the button below:
                </p>
                <div class="text-center">
                  <a class="btn btn-primary" href="{{ route('authentication-login') }}">
                    <i class="align-middle me-sm-25 me-0"></i>
                    <span>Verify Now</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="d-flex justify-content-between">
                <h5 class="mb-0">
                  Click here to
                  <a href="#">Reply</a>
                  or
                  <a href="#">Forward</a>
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Detailed Email Content ends -->
  </div>
  <!--/ Detailed Email View -->
  
</section>

@endsection

@section('vendor-script')
<!-- vendor js files -->
  <script src="{{ asset(mix('vendors/js/editors/quill/katex.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/editors/quill/highlight.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/editors/quill/quill.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/pages/app-email.js')) }}"></script>
@endsection
