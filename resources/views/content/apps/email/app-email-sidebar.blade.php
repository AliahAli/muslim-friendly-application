  <div class="sidebar-content email-app-sidebar">
    <div class="email-app-menu">
      <div class="sidebar-menu-list">
        <div class="list-group list-group-messages">
          <a href="#" class="list-group-item list-group-item-action active">
            <i data-feather="mail" class="font-medium-3 me-50"></i>
            <span class="align-middle">Inbox</span>
            <span class="badge badge-light-primary rounded-pill float-end">3</span>
          </a>
          <a href="#" class="list-group-item list-group-item-action">
            <i data-feather="send" class="font-medium-3 me-50"></i>
            <span class="align-middle">Sent</span>
          </a>
          <a href="#" class="list-group-item list-group-item-action">
            <i data-feather="edit-2" class="font-medium-3 me-50"></i>
            <span class="align-middle">Draft</span>
            <span class="badge badge-light-warning rounded-pill float-end">2</span>
          </a>
          <a href="#" class="list-group-item list-group-item-action">
            <i data-feather="star" class="font-medium-3 me-50"></i>
            <span class="align-middle">Starred</span>
          </a>
          <a href="#" class="list-group-item list-group-item-action">
            <i data-feather="info" class="font-medium-3 me-50"></i>
            <span class="align-middle">Spam</span>
            <span class="badge badge-light-danger rounded-pill float-end">5</span>
          </a>
          <a href="#" class="list-group-item list-group-item-action">
            <i data-feather="trash" class="font-medium-3 me-50"></i>
            <span class="align-middle">Trash</span>
          </a>
        </div>
        <!-- <hr /> -->
      </div>
    </div>
  </div>
  
