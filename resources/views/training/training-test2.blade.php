@extends('layouts/contentLayoutMaster3')

@section('title', 'Test')

@section('vendor-style')
{{-- Vendor Css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')

<!-- Basic Vertical form layout section start -->
<section id="basic-vertical-layouts">
  
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Section B</h4>
        </div>
        <div class="card-body">
          <form class="form form-vertical">
            <div class="row">
              <div class="col-12">
                <div class="mb-1">
                    <section id="basic-checkbox">
                      <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header">
                              <h4 class="card-title">6. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h4>
                            </div>
                            <div class="card-body">
                              <div class="demo-inline-spacing">
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" checked />
                                  <label class="form-check-label" for="inlineCheckbox1">A. Lorem ipsum dolor sit amet.</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">B. Lorem ipsum dolor sit amet.</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">C. Lorem ipsum dolor sit amet.</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">D. Lorem ipsum dolor sit amet.</label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                    <section id="basic-checkbox">
                      <div class="row">
                        <div class="col-12">
                          <div class="card-body">
                            <div class="demo-inline-spacing">
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="unchecked" checked />
                                <label class="form-check-label" for="inlineCheckbox1">A. Lorem ipsum dolor sit amet</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                <label class="form-check-label" for="inlineCheckbox2">B. Lorem ipsum dolor sit amet</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="unchecked" checked />
                                <label class="form-check-label" for="inlineCheckbox1">C. Lorem ipsum dolor sit amet</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                <label class="form-check-label" for="inlineCheckbox2">D. Lorem ipsum dolor sit amet</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                    <section id="basic-checkbox">
                      <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header">
                              <h4 class="card-title">8. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h4>
                            </div>
                            <div class="card-body">
                              <div class="demo-inline-spacing">
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" checked />
                                  <label class="form-check-label" for="inlineCheckbox1">A. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">B. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="checked" />
                                  <label class="form-check-label" for="inlineCheckbox2">C. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">D. Lorem ipsum dolor sit amet</label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                    <section id="basic-checkbox">
                      <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header">
                              <h4 class="card-title">9. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h4>
                            </div>
                            <div class="card-body">
                              <div class="demo-inline-spacing">
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="unchecked" checked />
                                  <label class="form-check-label" for="inlineCheckbox1">A. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">B. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="checked" />
                                  <label class="form-check-label" for="inlineCheckbox2">C. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">D. Lorem ipsum dolor sit amet</label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                    <section id="basic-checkbox">
                      <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header">
                              <h4 class="card-title">10. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h4>
                            </div>
                            <div class="card-body">
                              <div class="demo-inline-spacing">
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">A. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="unchecked" checked />
                                  <label class="form-check-label" for="inlineCheckbox1">B. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">C. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">D. Lorem ipsum dolor sit amet</label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                </div>
              </div>
          </form>
        </div>
      </div>
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <a class="btn btn-outline-secondary" href="{{ route('training-test1') }}">
            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
            <span>Back</span>
          </a>
        </div>
        <form class="chat-app-form" action="javascript:void(0);" onsubmit="submitTest();">
          <button type="button" class="btn btn-primary me-1" onclick="submitTest();">Submit</button>
        </form>
      </div>
    </div>
  </div>

</section>
<!-- Basic Vertical form layout section end -->

@endsection

@section('vendor-script')
<!-- vendor files -->
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection

@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/pages/alert-payment.js')) }}"></script>
@endsection
