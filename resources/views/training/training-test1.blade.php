@extends('layouts/contentLayoutMaster3')

@section('title', 'Test')

@section('content')

<!-- Basic Vertical form layout section start -->
<section id="basic-vertical-layouts">
  
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Section A</h4>
        </div>
        <div class="card-body">
          <form class="form form-vertical">
            <div class="row">
              <div class="col-12">
                <div class="mb-1">
                    <section id="basic-checkbox">
                      <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header">
                              <h4 class="card-title">1. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h4>
                            </div>
                            <div class="card-body">
                              <div class="demo-inline-spacing">
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" checked />
                                  <label class="form-check-label" for="inlineCheckbox1">A. Lorem ipsum dolor sit amet.</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">B. Lorem ipsum dolor sit amet.</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" checked />
                                  <label class="form-check-label" for="inlineCheckbox1">C. Lorem ipsum dolor sit amet.</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" checked />
                                  <label class="form-check-label" for="inlineCheckbox1">D. Lorem ipsum dolor sit amet.</label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                    <section id="basic-checkbox">
                      <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header">
                              <h4 class="card-title">2. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h4>
                            </div>
                            <div class="card-body">
                              <div class="demo-inline-spacing">
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" checked />
                                  <label class="form-check-label" for="inlineCheckbox1">A. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="checked" />
                                  <label class="form-check-label" for="inlineCheckbox2">B. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">C. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">D. Lorem ipsum dolor sit amet</label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                    <section id="basic-checkbox">
                      <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header">
                              <h4 class="card-title">3. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h4>
                            </div>
                            <div class="card-body">
                              <div class="demo-inline-spacing">
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked" checked />
                                  <label class="form-check-label" for="inlineCheckbox1">A. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">B. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="checked" />
                                  <label class="form-check-label" for="inlineCheckbox2">C. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">D. Lorem ipsum dolor sit amet</label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                    <section id="basic-checkbox">
                      <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header">
                              <h4 class="card-title">4. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h4>
                            </div>
                            <div class="card-body">
                              <div class="demo-inline-spacing">
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="unchecked" checked />
                                  <label class="form-check-label" for="inlineCheckbox1">A. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">B. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="unchecked" checked />
                                  <label class="form-check-label" for="inlineCheckbox1">C. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">D. Lorem ipsum dolor sit amet</label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                    <section id="basic-checkbox">
                      <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header">
                              <h4 class="card-title">5. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h4>
                            </div>
                            <div class="card-body">
                              <div class="demo-inline-spacing">
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">A. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="unchecked" checked />
                                  <label class="form-check-label" for="inlineCheckbox1">B. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">C. Lorem ipsum dolor sit amet</label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="unchecked" />
                                  <label class="form-check-label" for="inlineCheckbox2">D. Lorem ipsum dolor sit amet</label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                </div>
              </div>
          </form>
        </div>
      </div>
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
        </div>
        <a class="btn btn-primary m-1" href="{{ route('training-test2') }}">
          <span class="align-middle d-sm-inline-block d-none ">Next</span>
          <i data-feather="arrow-right" class="align-middle me-sm-25 me-0"></i>
        </a>
      </div>
    </div>
  </div>

</section>
<!-- Basic Vertical form layout section end -->

@endsection
