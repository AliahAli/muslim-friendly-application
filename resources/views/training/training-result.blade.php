
@extends('layouts/contentLayoutMaster4')

@section('title', 'Result')

@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection
@section('page-style')
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-invoice-list.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/dashboard-ecommerce.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
  @endsection

@section('content')
<!-- Dashboard Analytics Start -->
<section id="dashboard-analytics">
  <div class="row match-height">
    <!-- Greetings Card starts -->
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="card card-congratulations">
        <div class="card-body text-center">
          <img
            src="{{asset('images/elements/decore-left.png')}}"
            class="congratulations-img-left"
            alt="card-img-left"
          />
          <img
            src="{{asset('images/elements/decore-right.png')}}"
            class="congratulations-img-right"
            alt="card-img-right"
          />
          <div class="avatar avatar-xl bg-primary shadow">
            <div class="avatar-content">
              <i data-feather="award" class="font-large-1"></i>
            </div>
          </div>
          <div class="text-center">
            <h1 class="mb-1 text-white">Congratulations Olivia,</h1>
            <p class="card-text m-auto w-75">
              You have pass the test for <strong>K Fry Urban Korean Holiday Villa</strong> restaurant.
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- Greetings Card ends -->

    <!-- Goal Overview Card -->
    <div class="col-lg-8 col-md-6 col-12">
      <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
          <h4 class="card-title">Application Overview</h4>
        </div>
        <div class="card-body p-0">
          <div id="goal-overview-radial-bar-chart" class="my-2"></div>
          <div class="row border-top text-center mx-0">
            <div class="border-end py-1">
              <p class="card-text text-muted mb-0">Completed</p>
              <h3 class="fw-bolder mb-0">K Fry Urban Korean Holiday Villa</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  <!--/ Goal Overview Card -->

    <!-- Subscribers Chart Card starts -->
    <div class="col-lg-4 col-sm-6 col-12">
      <div class="card">
        <div class="card-header flex-column align-items-start pb-0">
          <div class="avatar bg-light-primary p-50 m-0">
            <div class="avatar-content">
              <i data-feather="users" class="font-medium-5"></i>
            </div>
          </div>
          <p class="card-text">Section A</p>
          <h2 class="fw-bolder mt-1">50%</h2>
        </div>
      </div>
      <div class="card">
        <div class="card-header flex-column align-items-start pb-0">
            <div class="avatar bg-light-primary p-50 m-0">
                <div class="avatar-content">
                <i data-feather="users" class="font-medium-5"></i>
                </div>
            </div>
            <p class="card-text">Section B</p>
            <h2 class="fw-bolder mt-1">45%</h2>
        </div>
    </div>
    </div>
    <!-- Subscribers Chart Card ends -->

    

    

    <!-- Browser States Card -->
    <div class="col-lg-12 col-md-6 col-12">
        <div class="card card-browser-states">
          <div class="card-header">
            <div>
              <h4 class="card-title">Training Test Results</h4>
            </div>
            <div class="dropdown chart-dropdown">
              <i data-feather="more-vertical" class="font-medium-3 cursor-pointer" data-bs-toggle="dropdown"></i>
              <div class="dropdown-menu dropdown-menu-end">
                <a class="dropdown-item" href="#">Last 28 Days</a>
                <a class="dropdown-item" href="#">Last Month</a>
                <a class="dropdown-item" href="#">Last Year</a>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="browser-states">
              <div class="d-flex">
                <h6 class="align-self-center mb-0">Haji Stick HQ</h6>
              </div>
              <div class="d-flex align-items-center">
                <div class="fw-bold text-body-heading me-1">54.4%</div>
                <div id="browser-state-chart-primary"></div>
              </div>
            </div>
            <div class="browser-states">
              <div class="d-flex">
                <h6 class="align-self-center mb-0">Frontier Food</h6>
              </div>
              <div class="d-flex align-items-center">
                <div class="fw-bold text-body-heading me-1">6.1%</div>
                <div id="browser-state-chart-warning"></div>
              </div>
            </div>
            <div class="browser-states">
              <div class="d-flex">
                <h6 class="align-self-center mb-0">Asam Pedas Johor</h6>
              </div>
              <div class="d-flex align-items-center">
                <div class="fw-bold text-body-heading me-1">14.6%</div>
                <div id="browser-state-chart-secondary"></div>
              </div>
            </div>
            <div class="browser-states">
                <div class="d-flex">
                  <h6 class="align-self-center mb-0">Asam Pedas House</h6>
                </div>
                <div class="d-flex align-items-center">
                  <div class="fw-bold text-body-heading me-1">0</div>
                </div>
            </div>
            <div class="browser-states">
                <div class="d-flex">
                  <h6 class="align-self-center mb-0">Bidfood Malaysia Sdn Bhd</h6>
                </div>
                <div class="d-flex align-items-center">
                  <div class="fw-bold text-body-heading me-1">0</div>
                </div>
              </div>
          </div>
        </div>
      </div>
      <!--/ Browser States Card -->

      {{-- Next button --}}
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <a class="btn btn-outline-secondary" href="{{ route('training-restaurant') }}">
            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
            <span>Back</span>
          </a>
        </div>
        <a class="btn btn-primary m-1" href="{{ route('product-restaurant') }}">
          <span class="align-middle d-sm-inline-block d-none ">Next</span>
        </a>
      </div>
</div>
</section>
@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/pages/dashboard-analytics.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/pages/app-invoice-list.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/pages/dashboard-ecommerce.js')) }}"></script>

@endsection
