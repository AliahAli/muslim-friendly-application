@extends('layouts/contentLayoutMaster3')

@section('title', '2 hours of video')

@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/plyr.min.css')) }}">
@endsection
@section('page-style')
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-media-player.css')) }}">
@endsection

@section('content')
<!-- Media Player -->
<section id="media-player-wrapper">
  <div class="row">
    <!-- VIDEO -->
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Video</h4>
          <div class="video-player" id="plyr-video-player">
            <iframe src="https://www.youtube.com/watch?v=56oEeDgJLvs" allowfullscreen allow="autoplay"></iframe>
          </div>
          
        </div>
        <div class="d-flex justify-content-between">
          <a class="btn btn-outline-secondary m-1" href="{{ route('training-payment') }}">
            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
            <span class="align-middle d-sm-inline-block d-none ">Back</span>
          </a>
          <form class="d-inline-block" action="javascript:void(0);" onsubmit="confirmTest();">
            <a type="button" class="btn btn-success m-1" onclick="confirmTest();">Take Test</a>
          </form>
        </div>
      </div>
      
    </div>
    <!--/ VIDEO -->
  </div>
  
</section>
<!--/ Media Player -->
@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/extensions/plyr.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/plyr.polyfilled.min.js')) }}"></script>
@endsection

@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/pages/alert-payment.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/extensions/ext-component-media-player.js')) }}"></script>
@endsection
