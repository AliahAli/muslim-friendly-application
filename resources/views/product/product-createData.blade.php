@extends('layouts/contentLayoutMaster4')

@section('title', 'New Menu')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
@endsection

@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
@endsection

@section('content')
<section>
  {{-- Product [Menu] --}}
  <div class="card">
    <div class="card-header border-bottom">
      <h4 class="card-title">Add New Menu</h4>
    </div>
    <div class="card-body px-2 py-2 align-items-center">
      <form>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Menu</label>
          <input type="text" id="pincode1" class="form-control" value="FRIED CHICKEN SAMBAL" />
        </div>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Brand</label>
          <input type="text" id="pincode1" class="form-control" value="AROMA" />
        </div>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Does the product has specific information</label>
          <input type="text" id="pincode1" class="form-control" value="Yes" />
        </div>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Specific Information of Menu Name</label>
          <input type="text" id="pincode1" class="form-control" value="CAMPURAN AYAM GORENG SAMBAL" />
        </div>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Menu Category</label>
          <input type="text" id="pincode1" class="form-control" value="FOOD" />
        </div>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Menu Market</label>
          <ul class="list-unstyled mt-1">
            <li class="mt-1"><input type="checkbox" class="form-check-input" id="checkbox">
              <label class="form-check-label" for="validationCheckBootstrap">Local Market</label>
            </li>
            <li class="mt-1"><input type="checkbox" class="form-check-input" id="checkbox" checked>
              <label class="form-check-label" for="validationCheckBootstrap">International Market</label>
            </li>
          </ul>
        <div class="mb-1">
          <label class="fw-bolder" for="pincode1">Ingredients Filled By</label>
          <ul class="list-unstyled mt-1">
            <li class="mt-1"><input name="radio" type="radio" class="form-check-input" id="checkbox" checked>
              <label class="form-check-label" for="validationCheckBootstrap">Applicant [KESOM45]</label>
            </li>
            <li class="mt-1"><input name="radio" type="radio" class="form-check-input" id="checkbox">
              <label class="form-check-label" for="validationCheckBootstrap">OEM Factory (AROMA FOOD INDUSTRIES SDN BHD)</label>
            </li>
          </ul>
        </div>
      </form>
    </div>
  </div>
  </div>
  {{-- Ingredients --}}
  <div class="card">
    <div class="border-bottom">
      <div class="p-2">
        <h4 class="card-title">Ingredient List</h4>
        <h5>List all ingredients including ready made ingredients/products that used as component materials</h5>
      </div>
    </div>
    <div class="card-datatable table-responsive px-2 py-2">
      <div class="alert-body d-flex align-items-center flex-wrap pb-2 justify-content-end">
        <div class="ms-3 d-inline-block">
          <a class="btn btn-primary d-inline-block" href="{{route('ingredient-create')}}">
            <i data-feather="plus" class="font-medium-3 me-50"></i>
            <span class="fw-bold">Add New Ingredients</span>
          </a>
          <a class="btn btn-primary d-inline-block" href="{{route('ingredient-index')}}">
            <i data-feather="plus" class="font-medium-3 me-50"></i>
            <span class="fw-bold">Choose Ingredients</span>
          </a>
        </div>
      </div>
      <table class="invoice-list-table table">
        <thead>
          <tr>
            <th>No</th>
            <th>Ingredient</th>
            <th>Source of Ingredient</th>
            <th class="cell-fit">Name & Address of Manufacturer</th>
            <th>Halal Status</th>
            <th class="text-center cell-fit">Action</th>
          </tr>
          <tr class="align-top">
            <td>1</td>
            <td>Chilli Powder</td>
            <td>Plants</td>
            <td>
              <div>Tusidhis Spice Marketing (M) Sdn Bhd</div>
              <div>No 5, Jalan Industri Ringan Permatang Tinggi 1, 14000 Bukit Mertajam, Pulau Pinang</div>
            </td>
            <td>
              <div>JAKIM</div>
              <div>Tarikh Tamat : 1/12/2021</div>
            </td>
            <td class="text-center">
             <div class="d-inline-flex">
              <a class="me-1" href="#">
                <i data-feather="edit" class="font-medium-3"></i>
              </a>
              <a href="#">
                <i data-feather="trash-2" class="font-medium-3"></i>
              </a>
            </div>
            </td>
          </tr>
        </thead>
      </table>
    </div>
    <div>
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <a class="btn btn-outline-secondary" href="{{ route('product-create') }}">
            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
            <span>Back</span>
          </a>
        </div>
        <a class="btn btn-primary" href="{{ route('product-show') }}">Save</a>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/responsive.bootstrap5.js')}}"></script>
@endsection