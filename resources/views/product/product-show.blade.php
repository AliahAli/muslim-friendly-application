@extends('layouts/contentLayoutMaster4')

@section('title', 'Review Menu')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
@endsection

@section('content')
<section class="invoice-list-wrapper">
  <div class="card bg-light-primary">
    <ul class="list-unstyled px-2 py-2">
      <li>
        <span class="fw-bolder">Restaurant Name:</span>
        <span>K Fry Urban Korean Holiday Villa Johor Bahru</span>
      </li>
    </ul>
  </div>
  <!-- warnings and primary alerts starts -->
  <div class="row">
    <div class="col-12">
      <div class="alert alert-warning" role="alert">
        <div class="alert-body">
          <h5><strong>Info:</strong> Please review your menu before submit the application</h5>
        </div>
      </div>
    </div>
  </div>
  <!-- warnings and primary alerts ends -->
  <div class="card">
    <div class="border-bottom">
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <h4 class="card-title">Menu List</h4>
        </div>
        <a class="btn btn-primary" href="{{route('product-create')}}">
          <i data-feather="plus" class="font-medium-3 me-50"></i>
          <span class="fw-bold">Add Menu</span>
        </a>
      </div>
    </div>
    <div class="card-datatable table-responsive px-2 py-2">
      <table class="invoice-list-table table">
        <thead>
          <tr>
            <th>No</th>
            <th>Menu Name</th>
            <th>Brand</th>
            <th>Category</th>
            <th>Market</th>
            <th>Menu</th>
            <th class="text-center cell-fit">Ingredients</th>
          </tr>
          <tr class="align-top">
            <td>1</td>
            <td>Fried Chicken Coating - Spicy</td>
            <td>Aroma</td>
            <td>Food</td>
            <td>International Market</td>
            <td>Applicant</td>
            <td>
              <div class="text-center d-inline-flex">
                {{-- View ingredient start --}}
                <div class="modal-size-xl ">
                  <a class="me-1" data-bs-toggle="modal" data-bs-target="#xlarge">
                    <i data-feather="eye" class="font-medium-3"></i>
                  </a>
                  <div class="modal fade text-start" id="xlarge" tabindex="-1" aria-labelledby="myModalLabel16" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-xl">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel16">List of Ingredient</h4>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                          <div class="card-datatable">
                            <div class="card">
                              <div class="card-datatable table-responsive">
                                <table class="invoice-list-table table">
                                  <thead>
                                    <tr>
                                      <th>No</th>
                                      <th>Ingredient</th>
                                      <th>Source of Ingredient</th>
                                      <th class="cell-fit">Name & Address of Manufacturer</th>
                                      <th>Halal Status</th>
                                    </tr>
                                    <tr class="align-top">
                                      <td>1</td>
                                      <td>Chilli Powder</td>
                                      <td>Plants</td>
                                      <td>
                                        <div>Tusidhis Spice Marketing (M) Sdn Bhd</div>
                                        <div>No 5, Jalan Industri Ringan Permatang Tinggi 1, 14000 Bukit Mertajam, Pulau Pinang</div>
                                      </td>
                                      <td>
                                        <div>JAKIM</div>
                                        <div>Tarikh Tamat : 1/12/2023</div>
                                      </td>
                                     </tr>
                                    <tr class="align-top">
                                      <td>2</td>
                                      <td>Wheat Flour</td>
                                      <td>Plants</td>
                                      <td>
                                        <div>SEBERANG FLOUR MILL SDN BHD</div>
                                        <div>Plot 112, Jalan Mawar, Pelabuhan Barat, 42920 Pulau Indah, Selangor</div>
                                      </td>
                                      <td>
                                        <div>JAKIM</div>
                                        <div>Tarikh Tamat : 30/06/2022</div>
                                      </td>
                                    </tr>
                                    <tr class="align-top">
                                      <td>3</td>
                                      <td>Fine Salt (Garam Putih / Garam Laut)</td>
                                      <td>Natural</td>
                                      <td>
                                        <div>SENG HIN BROTHERS ENTERPRISES SDN BHD</div>
                                        <div>Lot 156, Taman Perindustrian Integrasi Rawang, 48000 Rawang, Selangor</div>
                                      </td>
                                      <td>
                                        <div>JAKIM</div>
                                        <div>Tarikh Tamat : 31/10/2022</div>
                                      </td>
                                    </tr>
                                  </thead>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {{-- View ingredient end --}}
                <a class="me-1" href="#">
                  <i data-feather="edit" class="font-medium-3"></i>
                </a>
                <a href="#">
                  <i data-feather="trash-2" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
          <tr class="align-top">
            <td>2</td>
            <td>Fried Chicken Premix - Black Pepper</td>
            <td>Aroma</td>
            <td>Food</td>
            <td>Local Market</td>
            <td>Applicant</td>
            <td>
              <div class="text-center d-inline-flex">
                {{-- View ingredient start --}}
                <div class="modal-size-xl ">
                  <a class="me-1" data-bs-toggle="modal" data-bs-target="#xlarge">
                    <i data-feather="eye" class="font-medium-3"></i>
                  </a>
                  <div class="modal fade text-start" id="xlarge" tabindex="-1" aria-labelledby="myModalLabel16" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-xl">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel16">List of Ingredient</h4>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                          <div class="card-datatable">
                            <div class="card">
                              <div class="card-datatable table-responsive">
                                <table class="invoice-list-table table">
                                  <thead>
                                    <tr>
                                      <th>No</th>
                                      <th>Ingredient</th>
                                      <th>Source of Ingredient</th>
                                      <th class="cell-fit">Name & Address of Manufacturer</th>
                                      <th>Halal Status</th>
                                    </tr>
                                    <tr class="align-top">
                                      <td>1</td>
                                      <td>Chilli Powder</td>
                                      <td>Plants</td>
                                      <td>
                                        <div>Tusidhis Spice Marketing (M) Sdn Bhd</div>
                                        <div>No 5, Jalan Industri Ringan Permatang Tinggi 1, 14000 Bukit Mertajam, Pulau Pinang</div>
                                      </td>
                                      <td>
                                        <div>JAKIM</div>
                                        <div>Tarikh Tamat : 1/12/2023</div>
                                      </td>
                                     </tr>
                                    <tr class="align-top">
                                      <td>2</td>
                                      <td>Wheat Flour</td>
                                      <td>Plants</td>
                                      <td>
                                        <div>SEBERANG FLOUR MILL SDN BHD</div>
                                        <div>Plot 112, Jalan Mawar, Pelabuhan Barat, 42920 Pulau Indah, Selangor</div>
                                      </td>
                                      <td>
                                        <div>JAKIM</div>
                                        <div>Tarikh Tamat : 30/06/2022</div>
                                      </td>
                                      </tr>
                                    <tr class="align-top">
                                      <td>3</td>
                                      <td>Fine Salt (Garam Putih / Garam Laut)</td>
                                      <td>Natural</td>
                                      <td>
                                        <div>SENG HIN BROTHERS ENTERPRISES SDN BHD</div>
                                        <div>Lot 156, Taman Perindustrian Integrasi Rawang, 48000 Rawang, Selangor</div>
                                      </td>
                                      <td>
                                        <div>JAKIM</div>
                                        <div>Tarikh Tamat : 31/10/2022</div>
                                      </td>
                                     </tr>
                                  </thead>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {{-- View ingredient end --}}
                <a class="me-1" href="#">
                  <i data-feather="edit" class="font-medium-3"></i>
                </a>
                <a href="#">
                  <i data-feather="trash-2" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
          <tr class="align-top">
            <td>3</td>
            <td>Fried Chicken Sambal</td>
            <td>Aroma</td>
            <td>Food</td>
            <td>Local Market</td>
            <td>Applicant</td>
            <td>
              <div class="text-center d-inline-flex">
                {{-- View ingredient start --}}
                <div class="modal-size-xl ">
                  <a class="me-1" data-bs-toggle="modal" data-bs-target="#xlarge">
                    <i data-feather="eye" class="font-medium-3"></i>
                  </a>
                  <div class="modal fade text-start" id="xlarge" tabindex="-1" aria-labelledby="myModalLabel16" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-xl">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel16">List of Ingredient</h4>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                          <div class="card-datatable">
                            <div class="card">
                              <div class="card-datatable table-responsive">
                                <table class="invoice-list-table table">
                                  <thead>
                                    <tr>
                                      <th>No</th>
                                      <th>Ingredient</th>
                                      <th>Source of Ingredient</th>
                                      <th class="cell-fit">Name & Address of Manufacturer</th>
                                      <th>Halal Status</th>
                                      <th>Status</th>
                                    </tr>
                                    <tr class="align-top">
                                      <td>1</td>
                                      <td>Chilli Powder</td>
                                      <td>Plants</td>
                                      <td>
                                        <div>Tusidhis Spice Marketing (M) Sdn Bhd</div>
                                        <div>No 5, Jalan Industri Ringan Permatang Tinggi 1, 14000 Bukit Mertajam, Pulau Pinang</div>
                                      </td>
                                      <td>
                                        <div>JAKIM</div>
                                        <div>Tarikh Tamat : 1/12/2023</div>
                                      </td>
                                     </tr>
                                    <tr class="align-top">
                                      <td>2</td>
                                      <td>Wheat Flour</td>
                                      <td>Plants</td>
                                      <td>
                                        <div>SEBERANG FLOUR MILL SDN BHD</div>
                                        <div>Plot 112, Jalan Mawar, Pelabuhan Barat, 42920 Pulau Indah, Selangor</div>
                                      </td>
                                      <td>
                                        <div>JAKIM</div>
                                        <div>Tarikh Tamat : 30/06/2022</div>
                                      </td>
                                      </tr>
                                    <tr class="align-top">
                                      <td>3</td>
                                      <td>Fine Salt (Garam Putih / Garam Laut)</td>
                                      <td>Natural</td>
                                      <td>
                                        <div>SENG HIN BROTHERS ENTERPRISES SDN BHD</div>
                                        <div>Lot 156, Taman Perindustrian Integrasi Rawang, 48000 Rawang, Selangor</div>
                                      </td>
                                      <td>
                                        <div>JAKIM</div>
                                        <div>Tarikh Tamat : 31/10/2022</div>
                                      </td>
                                     </tr>
                                  </thead>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {{-- View ingredient end --}}
                <a class="me-1" href="#">
                  <i data-feather="edit" class="font-medium-3"></i>
                </a>
                <a href="#">
                  <i data-feather="trash-2" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
        </thead>
      </table>
    </div>
    <div>
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <a class="btn btn-outline-secondary" href="{{ route('product-createData') }}">
            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
            <span>Back</span>
          </a>
        </div>
        <form class="chat-app-form" action="javascript:void(0);" onsubmit="submitMenu();">
          <button type="button" class="btn btn-primary me-1" onclick="submitMenu();">Submit</button>
        </form>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/responsive.bootstrap5.js')}}"></script>
<script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection

@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/pages/alert-payment.js')) }}"></script>
@endsection