@extends('layouts/contentLayoutMaster4')

@section('title', 'Payment')

@section('vendor-style')
<!-- vendor css files -->
<link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap5.min.css')) }}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap5.min.css')) }}">
@endsection
@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card bg-light-primary">
      <ul class="list-unstyled px-2 py-2">
        <li>
          <span class="fw-bolder">Restaurant Name:</span>
          <span>K Fry Urban Korean Holiday Villa Johor Bahru</span>
        </li>
      </ul>
    </div>

    <!-- payment summary -->
    <div class="card">
      <div class="card-header border-bottom">
        <h4 class="card-title">Payment Summary</h4>
      </div>
      <div class="card-body my-2 py-25">
        <!-- Address and Contact starts -->
        <div class="card-body invoice-padding pt-0">
          <div class="row invoice-spacing">
            <div class="col-xl-8 p-0">
              <h6 class="mb-2">Payment To:</h6>
              <h6 class="mb-25">Perbadanan Islam Johor</h6>
              <p class="card-text mb-25">T3.1, Bangunan PIJ,</p>
              <p class="card-text mb-25">No. 26C, Jalan Rebana, Kebun Teh,</p>
              <p class="card-text mb-25">80250, Johor Bahru.</p>
              <p class="card-text mb-0">pij@pij.gov.my</p>
            </div>
            <div class="col-xl-4 p-0 mt-xl-0 mt-2">
              <h6 class="mb-2">Payment Details:</h6>
              <table>
                <tbody>
                  <tr>
                    <td class="pe-1">Bank name:</td>
                    <td>CIMB Bank</td>
                  </tr>
                  <tr>
                    <td class="pe-1">Account Number:</td>
                    <td>718-986-6062</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- Address and Contact ends -->
        
        <!-- Invoice Description starts -->
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th class="py-1">#</th>
                <th class="py-1">Item</th>
                <th class="py-1">Price</th>
                <th class="py-1">Total</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="py-1">
                  <span>1</span>
                </td>
                <td class="py-1">
                  <p class="card-text mb-25">90% fees for menu registration</p>
                </td>
                <td class="py-1">
                  <span>RM900.00</span>
                </td>
                <td class="py-1">
                  <span>RM900.00</span>
                </td>
              </tr>
              <tr>
                <td class="py-1">
                  </td>
                <td class="py-1">
                  <span class="fw-bold">Subtotal</span>
                </td>
                <td class="py-1">
                  <span class="fw-bold"></span>
                </td>
                <td class="py-1">
                  <span class="fw-bold">RM900.00</span>
                </td>
              </tr>
              <tr>
                <td class="py-1">
                </td>
                <td class="py-1">
                  <span class="fw-bold">Total</span>
                </td>
                <td class="py-1">
                  <span class="fw-bold"></span>
                </td>
                <td class="py-1">
                  <span class="fw-bold">RM900.00</span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- / payment summary -->

    <!-- payment methods -->
    <div class="card">
      <div class="card-header border-bottom">
        <h4 class="card-title">Payment Methods</h4>
      </div>
      <div class="card-body my-1 py-25">
        <div class="row gx-4">
          <div class="col-lg-6">
            <form id="creditCardForm" class="row gx-2 gy-1 validate-form" onsubmit="return false">
              <div class="col-12">
                <div class="form-check form-check-inline mb-1">
                  <input name="collapsible-payment" class="form-check-input" type="radio" value=""
                    id="collapsible-payment-cc" checked="" />
                  <label class="form-check-label" for="collapsible-payment-cc">Credit/Debit/ATM Card</label>
                </div>
                <div class="form-check form-check-inline mb-1">
                  <input name="collapsible-payment" class="form-check-input" type="radio" value=""
                    id="collapsible-payment-cash" />
                  <label class="form-check-label" for="collapsible-payment-cash">PayPal account</label>
                </div>
              </div>
              <div class="col-12 mt-0">
                <label class="form-label" for="addCardNumber">Card Number</label>
                <div class="input-group input-group-merge">
                  <input id="addCardNumber" name="addCard" class="form-control add-credit-card-mask" type="text"
                    value="5637 8172 1290 7898" aria-describedby="addCard2"
                    data-msg="Please enter your credit card number" />
                  <span class="input-group-text cursor-pointer p-25" id="addCard2">
                    <span class="add-card-type"></span>
                  </span>
                </div>
              </div>

              <div class="col-md-6">
                <label class="form-label" for="addCardName">Name On Card</label>
                <input type="text" id="addCardName" class="form-control" value="John Doe" />
              </div>

              <div class="col-6 col-md-3">
                <label class="form-label" for="addCardExpiryDate">Exp. Date</label>
                <input type="text" id="addCardExpiryDate" class="form-control add-expiry-date-mask"
                  value="02/25" />
              </div>

              <div class="col-6 col-md-3">
                <label class="form-label" for="addCardCvv">CVV</label>
                <input type="text" id="addCardCvv" class="form-control add-cvv-code-mask" maxlength="3"
                  value="333" />
              </div>

              <div class="col-12">
                <div class="d-flex align-items-center">
                  <div class="form-check form-switch form-check-primary me-25">
                    <input type="checkbox" class="form-check-input" id="addSaveCard" checked />
                    <label class="form-check-label" for="addSaveCard">
                      <span class="switch-icon-left"><i data-feather="check"></i></span>
                      <span class="switch-icon-right"><i data-feather="x"></i></span>
                    </label>
                  </div>
                  <label class="form-check-label fw-bolder" for="addSaveCard"> Save Card for future billing? </label>
                </div>
              </div>
              <div class="d-flex justify-content-between mt-3">
                <a class="btn btn-outline-secondary" href="{{ route('product-restaurant') }}">
                  <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                  <span class="align-middle d-sm-inline-block d-none">Back</span>
                </a>
                <form class="chat-app-form" action="javascript:void(0);" onsubmit="confirmPaymentMenu();">
                  <button type="button" class="btn btn-primary me-1" onclick="confirmPaymentMenu();">Confirm Payment</button>
                </form>
              </div>
              <input type="hidden" />
            </form>
          </div>
          <div class="col-lg-6 mt-2 mt-lg-0">
            <h6 class="fw-bolder mb-2">My Cards</h6>
            <div class="added-cards">
              <div class="cardMaster rounded border p-2 mb-1">
                <div class="d-flex justify-content-between flex-sm-row flex-column">
                  <div class="card-information">
                    <img class="mb-1 img-fluid" src="{{asset('images/icons/payments/mastercard.png')}}"
                      alt="Master Card" />
                    <div class="d-flex align-items-center mb-50">
                      <h6 class="mb-0">Tom McBride</h6>
                      <span class="badge badge-light-primary ms-50">Primary</span>
                    </div>
                    <span class="card-number">∗∗∗∗ ∗∗∗∗ 9856</span>
                  </div>
                  <div class="d-flex flex-column text-start text-lg-end">
                    <div class="d-flex order-sm-0 order-1 mt-1 mt-sm-0">
                      <button class="btn btn-outline-primary me-75" data-bs-toggle="modal" data-bs-target="#editCard">
                        Edit
                      </button>
                      <button class="btn btn-outline-secondary">Delete</button>
                    </div>
                    <span class="mt-2">Card expires at 12/24</span>
                  </div>
                </div>
              </div>
              <div class="cardMaster border rounded p-2">
                <div class="d-flex justify-content-between flex-sm-row flex-column">
                  <div class="card-information">
                    <img class="mb-1 img-fluid" src="{{asset('images/icons/payments/visa.png')}}" alt="Visa Card" />
                    <h6>Mildred Wagner</h6>
                    <span class="card-number">∗∗∗∗ ∗∗∗∗ 5896</span>
                  </div>
                  <div class="d-flex flex-column text-start text-lg-end">
                    <div class="d-flex order-sm-0 order-1 mt-1 mt-sm-0">
                      <button class="btn btn-outline-primary me-75" data-bs-toggle="modal" data-bs-target="#editCard">
                        Edit
                      </button>
                      <button class="btn btn-outline-secondary">Delete</button>
                    </div>
                    <span class="mt-2">Card expires at 02/24</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / payment methods -->
  </div>
</div>

@include('content/_partials/_modals/modal-pricing')
@include('content/_partials/_modals/modal-edit-cc')
@endsection

@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/pages/alert-payment.js')) }}"></script>
@endsection

@section('vendor-script')
<!-- vendor files -->
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
@endsection