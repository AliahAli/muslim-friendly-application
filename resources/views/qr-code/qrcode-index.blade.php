@extends('layouts/contentLayoutMaster6')

@section('title', 'Restaurant')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
@endsection

@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
@endsection

@section('content')
<section class="invoice-list-wrapper">
  <div class="card">
    <div class="border-bottom">
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <h4 class="card-title">List of Restaurant</h4>
        </div>
      </div>
    </div>
    <div class="card-datatable table-responsive px-2 py-2">
      <table class="invoice-list-table table">
        <thead>
          <tr >
            <th>No</th>
            <th>Restaurant Name</th>
            <th>Restaurant Status</th>
            <th>Address</th>
            <th class="cell-fit">QR Code</th>
          </tr>
          <tr class="align-top">
            <td>1</td>
            <td>K Fry Urban Korean Holiday Villa Johor Bahru</td>
            <td>Bumiputra</td>
            <td>No. 260, Jalan Dato Sulaiman, Taman Abad Johor Bahru 80250, Johor, Malaysia.</td>
            <td class="text-center">
              <div class="text-center">
                 
                <a href="{{ route('qrcode-payment') }}">
                  <i data-feather="external-link" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
          <tr class="align-top">
            <td>2</td>
            <td>K Fry Urban Korean One Utama Shopping Center</td>
            <td>Bumiputra</td>
            <td>LG221A Bandar Utama City Centre, 1, Lebuh Bandar Utama, Bandar Utama, Petaling Jaya 47800, Selangor, Malaysia</td>
            <td class="text-center">
              <div class="text-center">
                 
                <a href="{{ route('qrcode-payment') }}">
                  <i data-feather="external-link" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
          <tr class="align-top">
            <td>3</td>
            <td>K Fry Urban Korean Old Street Batu Pahat</td>
            <td>Bumiputra</td>
            <td>7 Jalan Rotan Kong, Taman Sri Jaya, Batu Pahat, 83000, Johor, Malaysia.</td>
            <td class="text-center">
              <div class="text-center">
                 
                <a href="{{ route('qrcode-payment') }}">
                  <i data-feather="external-link" class="font-medium-3"></i>
                </a>
              </div>
            </td>
          </tr>
        </thead>
      </table>
    </div>
    <div>
      <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
        <div class="me-1">
          <a class="btn btn-outline-secondary" href="{{ route('dashboard-user3') }}">
            <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
            <span>Back</span>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/responsive.bootstrap5.js')}}"></script>
@endsection

{{-- @section('page-script')
<script src="{{asset('js/scripts/pages/app-invoice-list.js')}}"></script>
@endsection --}}