@extends('layouts/fullLayoutMaster')

@section('title', 'Menu')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
@endsection

@section('content')
<section>
  <div class="card">
    <!-- Topbar Start -->
      <div class="row align-items-center py-2 px-2">
        <div class="col-lg-2">
          <img src="{{asset('images/qrcode/logo-phv.png')}}" style="width:80%" />
        </div>
        <div class="col-lg-4">
          <a href="" class="text-decoration-none">
            <h2 class="m-0">PIJ HALAL VENTURES SDN BHD</h2>
          </a>
        </div>
        <div class="col-lg-3 text-right">
          <div class="d-inline-flex align-items-center">
            <i class="fa fa-2x fa-map-marker-alt text-primary mr-3"></i>
            <div class="text-left">
              <h6 class="font-weight-semi-bold mt-1">Address</h6>
              <small>
                  Aras 8, Bangunan PIJ Holdings, No. 8 Jalan Bukit Timbalan, 8000 Johor Bahru, Johor Darul Takzim, Malaysia</a>
              </small>
            </div>
          </div>
        </div>
        <div class="col-lg-1 text-right">
          <div class="d-inline-flex align-items-center">
            <i class="fa fa-2x fa-envelope text-primary mr-3"></i>
            <div class="text-left">
              <h6 class="font-weight-semi-bold mt-1">Email</h6>
              <small><a style="text-decoration: none; color: black;"
                  href="mailto: pij@pij.gov.my">pij@pij.gov.my</a></small>
            </div>
          </div>
        </div>
        <div class="col-lg-2 text-right">
          <div class="d-inline-flex align-items-center">
            <i class="fa fa-2x fa-phone text-primary mr-3"></i>
            <div class="text-left">
              <h6 class="font-weight-semi-bold mt-1">Phone Number</h6>
              <small><a style="text-decoration: none; color: black;" href="tel: 073369200">07 336 9200</a></small>
            </div>
          </div>
        </div>
      </div>
    <!-- Topbar End -->
    <div class="bg-primary">
      <ul class="list-unstyled px-2 py-5 text-white text-center">
        <li class="mb-2">
          <h1 class="text-white">K Fry Urban Korean Holiday Villa Johor Bahru</h1>
        </li>
        <li class="border-bottom mb-2 pb-2">
          <span>260, Jalan Dato Sulaiman, Taman Abad, 80250 Johor Bahru, Johor</span>
        </li>
        <li>
          <h4 class="text-white">This is the Muslim Friendly certified restaurant.</h4>
        </li>
      </ul>
    </div>
    
    {{-- Menu Details start --}}
    <div>
      <div class="text-center border-bottom px-2 py-2">
        <h2>List of Menu</h2>
      </div>
      <div class="card-body px-3 py-3">
        <div class="mb-4 row text-center">
          <span class="mb-1 fw-bolder">Fried Chicken Coating - Spicy</span>
          <span>This is your dish description. Include an overview of your ingredients, dietary notes, and other relevant
            info.</span>
        </div>
        <div class="mb-4 row text-center">
          <span class="mb-1 fw-bolder">Fried Chicken Premix - Black Pepper</span>
          <span>This is your dish description. Include an overview of your ingredients, dietary notes, and other relevant
            info.</span>
        </div>
        <div class="mb-4 row text-center">
          <span class="mb-1 fw-bolder">Fried Chicken Sambal</span>
          <span>This is your dish description. Include an overview of your ingredients, dietary notes, and other relevant
            info.</span>
        </div>
        <div class="mb-4 row text-center">
          <span class="mb-1 fw-bolder">Honey Mustard Cheesy Fried Chicken</span>
          <span>This is your dish description. Include an overview of your ingredients, dietary notes, and other relevant
            info.</span>
        </div>
        <div class="mb-4 row text-center">
          <span class="mb-1 fw-bolder">Honey Butter Wings</span>
          <span>This is your dish description. Include an overview of your ingredients, dietary notes, and other relevant
            info.</span>
        </div>
        <div class="mb-4 row text-center">
          <span class="mb-1 fw-bolder">Truffle Fries</span>
          <span>This is your dish description. Include an overview of your ingredients, dietary notes, and other relevant
            info.</span>
        </div>
      </div>
    </div>
    </div>
    {{-- Menu Details ends --}}
  </div>
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection

@section('page-script')
<!-- Page js files -->
<script src="{{ asset(mix('js/scripts/pages/alert-payment.js')) }}"></script>
@endsection