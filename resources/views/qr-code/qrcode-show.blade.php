@extends('layouts/fullLayoutMaster')

@section('title', 'QR Code')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
<link rel='stylesheet' href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice-list.css')}}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">
@endsection

@section('content')
<section onload="print()">
  <div class="bg-white">
    <ul class="list-unstyled text-white text-center border-bottom pb-1">
      <li class="mb-2">
        <img src="{{asset('images/qrcode/logo-phv.png')}}" style="width:40%" />
      </li>
      <li class="mb-2">
        <h2 class="m-0"><strong>PIJ HALAL VENTURES SDN BHD</strong></h2>
      </li>
      <li>
        <h6>ARAS 8, BANGUNAN PIJ HOLDINGS, NO. 8 JALAN BUKIT TIMBALAN, 8000 JOHOR BAHRU,
        JOHOR DARUL TAKZIM, MALAYSIA</h6>
      </li>
      <li class="mb-2">
        <div class="row align-items-center px-2">
          <div class="col-lg-12">
            <a href="" class="text-decoration-none">
              <h6 class="m-1">TEL: 07-336 9200 | FAKS: 07-336 9250</h6>
              <h6 class="m-0">EMEL: pij@pij.gov.my | LAMAN WEB: www.pij.gov.my</h6>
            </a>
          </div>
        </div>
      </li>
    </ul>
    <ul class="list-unstyled text-white text-center pt-1">
      <li class="mb-2">
        <h2><strong>WE ARE CERTIFIED MUSLIM FRIENDLY RESTAURANT</strong></h2>
      </li>
      <li class="mb-2">
        <h3><strong>K FRY URBAN KOREAN HOLIDAY VILLA JOHOR BAHRU</strong></h3>
      </li>
      <li class="mb-2">
        <h3>260, JALAN DATO SULAIMAN, TAMAN ABAD, 80250 JOHOR BAHRU, JOHOR</h3>
      </li>
    </ul>
    {{-- QR Code Details start --}}
    <div class="text-center">
      <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl={{ route('qrcode-menu') }}&choe=UTF-8" 
      style="width: 50%;" />
    </div>
    <div>
      <div class="text-center">
        <h4>PLEASE SCAN THE QR CODE</h4>
      </div>
    </div>
    {{-- QR Code Details ends --}}
  </div>
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/extensions/moment.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
<script>
  $(document).ready(function() {
    window.print();
    window.close();
    return true;
  });
</script>
@endsection