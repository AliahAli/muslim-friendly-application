<?php

use App\Http\Controllers\DocumentController;
use Illuminate\Support\Facades\Route;

/* Route Document Submission */
Route::group(['prefix' => 'document'], function () {
    Route::get('requirement', [DocumentController::class, 'documentRequirement'])->name('document-requirement');
    Route::get('payment', [DocumentController::class, 'documentPayment'])->name('document-payment');
    Route::get('submit', [DocumentController::class, 'documentSubmit'])->name('document-submit');
    Route::get('reviewed', [DocumentController::class, 'documentReviewed'])->name('document-reviewed');
});
/* Route Document Submission */

?>