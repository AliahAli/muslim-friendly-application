<?php

use App\Http\Controllers\MenuController;
use Illuminate\Support\Facades\Route;

/* Route Menu for Admin */
Route::group(['prefix' => 'menu'], function () {
    Route::get('index', [MenuController::class, 'index'])->name('menu-index');
    Route::get('index2', [MenuController::class, 'index2'])->name('menu-index2');
    Route::get('list', [MenuController::class, 'list'])->name('menu-list');
    Route::get('list2', [MenuController::class, 'list2'])->name('menu-list2');
    Route::get('review', [MenuController::class, 'review'])->name('menu-review');
    Route::get('review2', [MenuController::class, 'review2'])->name('menu-review2');
    Route::get('approve', [MenuController::class, 'approve'])->name('menu-approve');
    Route::get('reject', [MenuController::class, 'reject'])->name('menu-reject');
});
/* Route Menu for Admin */

?>