<?php

use App\Http\Controllers\TrainingController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'training'], function () {
    Route::get('restaurant', [TrainingController::class, 'trainingRestaurant'])->name('training-restaurant');
    Route::get('payment', [TrainingController::class, 'trainingPayment'])->name('training-payment');
    Route::get('video', [TrainingController::class, 'trainingVideo'])->name('training-video');
    Route::get('test1', [TrainingController::class, 'trainingTest1'])->name('training-test1');
    Route::get('test2', [TrainingController::class, 'trainingTest2'])->name('training-test2');
    Route::get('result', [TrainingController::class, 'trainingResult'])->name('training-result');

});
