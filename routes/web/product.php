<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/* Route Menu for User */
Route::group(['prefix' => 'product'], function () {
    Route::get('menu-restaurant', [ProductController::class, 'restaurant'])->name('product-restaurant');
    Route::get('payment', [ProductController::class, 'payment'])->name('product-payment');
    Route::get('index', [ProductController::class, 'index'])->name('product-index');
    Route::get('product-list', [ProductController::class, 'indexData'])->name('product-indexData');
    Route::get('create', [ProductController::class, 'create'])->name('product-create');
    Route::get('create-data', [ProductController::class, 'createData'])->name('product-createData');
    Route::get('show', [ProductController::class, 'show'])->name('product-show');
});
/* Route Menu for User */

?>