<?php

use App\Http\Controllers\IngredientController;
use Illuminate\Support\Facades\Route;

/* Route Ingredient */
Route::group(['prefix' => 'ingredient'], function () {
    Route::get('index', [IngredientController::class, 'index'])->name('ingredient-index');
    Route::get('create', [IngredientController::class, 'create'])->name('ingredient-create');
    Route::get('show', [IngredientController::class, 'show'])->name('ingredient-show');
    Route::get('document', [IngredientController::class, 'document'])->name('ingredient-document');
    Route::get('review', [IngredientController::class, 'review'])->name('ingredient-review');
});
/* Route Ingredient */

?>