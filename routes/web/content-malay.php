<?php

use App\Http\Controllers\ContentMalayController;
use Illuminate\Support\Facades\Route;

/* Route QR Code */
Route::group(['prefix' => 'content'], function () {
    Route::get('application-form', [ContentMalayController::class, 'applicationForm'])->name('content-application-form');
    Route::get('qrcode', [ContentMalayController::class, 'qrcode'])->name('content-qrcode');
});
/* Route QR Code */

?>