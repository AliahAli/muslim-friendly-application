<?php

use App\Http\Controllers\ApplicationController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'application'], function () {
    Route::get('app-form', [ApplicationController::class, 'applicationForm'])->name('application-app-form');
    Route::get('review', [ApplicationController::class, 'applicationReview'])->name('application-review');
    Route::get('dashboard', [ApplicationController::class, 'applicationDashboard'])->name('application-dashboard');
    Route::get('index', [ApplicationController::class, 'applicationIndex'])->name('application-index');
    Route::get('index-data', [ApplicationController::class, 'applicationIndexData'])->name('application-index-data');
    
});
