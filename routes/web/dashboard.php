<?php

use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;


/* Route Dashboards */
Route::group(['prefix' => 'dashboard'], function () {
    Route::get('analytics', [DashboardController::class, 'dashboardAnalytics'])->name('dashboard-analytics');
    Route::get('ecommerce', [DashboardController::class, 'dashboardEcommerce'])->name('dashboard-ecommerce');
    Route::get('admin2', [DashboardController::class, 'dashboardAdmin2'])->name('dashboard-admin2');
    Route::get('admin1', [DashboardController::class, 'dashboardAdmin1'])->name('dashboard-admin1');
    Route::get('user1', [DashboardController::class, 'dashboardUser1'])->name('dashboard-user1');
    Route::get('user2', [DashboardController::class, 'dashboardUser2'])->name('dashboard-user2');
    Route::get('user3', [DashboardController::class, 'dashboardUser3'])->name('dashboard-user3');
});
/* Route Dashboards */

?>