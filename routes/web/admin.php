<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin'], function () {
    Route::get('index', [AdminController::class, 'adminIndex'])->name('admin-index');
    Route::get('document-reviewed', [AdminController::class, 'adminDocumentReviewed'])->name('admin-document-reviewed');
    Route::get('review', [AdminController::class, 'adminReviewed'])->name('admin-review');
    Route::get('approved', [AdminController::class, 'adminApproved'])->name('admin-approved');
    Route::get('rejected', [AdminController::class, 'adminRejected'])->name('admin-rejected');
});
