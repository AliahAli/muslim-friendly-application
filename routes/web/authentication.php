<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'authentication'], function () {
    Route::get('login', [HomeController::class, 'authenticationLogin'])->name('authentication-login');
    Route::get('registeration', [HomeController::class, 'authenticatedRegisteration'])->name('authentication-registeration');
    Route::get('tac', [HomeController::class, 'authenticatedTac'])->name('authentication-tac');
    Route::get('login2', [HomeController::class, 'authenticationLogin2'])->name('authentication-login2');
    Route::get('email', [HomeController::class, 'authenticatedEmail'])->name('authentication-email');
    Route::get('login3', [HomeController::class, 'authenticationLogin3'])->name('authentication-login3');
    Route::get('login4', [HomeController::class, 'authenticationLogin4'])->name('authentication-login4');
    Route::get('login5', [HomeController::class, 'authenticationLogin5'])->name('authentication-login5');
});
