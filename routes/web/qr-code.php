<?php

use App\Http\Controllers\QRCodeController;
use Illuminate\Support\Facades\Route;

/* Route QR Code */
Route::group(['prefix' => 'qr-code'], function () {
    Route::get('index', [QRCodeController::class, 'index'])->name('qrcode-index');
    Route::get('payment', [QRCodeController::class, 'payment'])->name('qrcode-payment');
    Route::get('generate', [QRCodeController::class, 'generate'])->name('qrcode-generate');
    Route::get('menu', [QRCodeController::class, 'menu'])->name('qrcode-menu');
    Route::get('show', [QRCodeController::class, 'show'])->name('qrcode-show');

});
/* Route QR Code */

?>