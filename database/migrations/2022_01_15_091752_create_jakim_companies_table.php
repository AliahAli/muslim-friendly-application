<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJakimCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jakim_companies', function (Blueprint $table) {
            $table->id();
            $table->integer('num');
            $table->string('name', 800)->index();
            $table->string('address');
            $table->string('state')->default('');
            $table->string('phone_no')->default('');
            $table->string('fax_no')->default('');
            $table->string('email')->default('');
            $table->string('website')->default('');
            $table->string('reference_no')->default('');
            $table->string('officer')->default('');
            $table->string('company_url');
            $table->string('product_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jakim_companies');
    }
}
