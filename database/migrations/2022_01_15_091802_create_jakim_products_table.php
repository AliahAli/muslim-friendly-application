<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJakimProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jakim_products', function (Blueprint $table) {
            $table->id();
            $table->integer('jakim_company_id');
            $table->integer('num');
            $table->string('name', 800)->index();
            $table->string('brand', 800);
            $table->string('expired_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jakim_products');
    }
}
